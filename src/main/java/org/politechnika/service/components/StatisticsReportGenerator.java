package org.politechnika.service.components;

import org.politechnika.domain.GloveDataEntity;
import org.politechnika.domain.KinectDataEntity;
import org.politechnika.domain.PulsometerDataEntity;
import org.politechnika.domain.ResearchEntity;
import org.politechnika.service.report.functions.GloveFunctions;
import org.politechnika.service.report.glove_functions.CalculateGloveStatistics;
import org.politechnika.service.report.glove_functions.CalculateTimeIntervalStatistics;
import org.politechnika.service.report.glove_functions.PartitionDataByHand;
import org.politechnika.service.report.kinect_functions.*;
import org.politechnika.service.report.model.glove.Finger;
import org.politechnika.service.report.model.glove.HandStatistics;
import org.politechnika.service.report.model.glove.TimeIntervalHandStatistics;
import org.politechnika.service.report.model.kinect.*;
import org.politechnika.service.report.model.pulsometer.PulsometerStatistics;
import org.politechnika.service.report.pulsometer_functions.CalculatePulsometerStatistics;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;
import static org.politechnika.commons.Separators.NEWLINE;

@Component
public class StatisticsReportGenerator {

    public ReportBytesResource generate(ResearchEntity research) {
        StringBuilder sb = new StringBuilder();
        if (!research.getGloveData().isEmpty()) {
            sb.append("-------------- Statystyki dla rękawic 5DT (kciuk, wskazujący, środkowy, serdeczny, mały)--------------");
            final List<GloveDataEntity> gloveData = research.getGloveData();
            sb.append(NEWLINE).append(NEWLINE);
            Map<String, List<GloveDataEntity>> glovesPartitionedByHand = new PartitionDataByHand().apply(gloveData);

            Map<Finger, List<GloveDataEntity>> left = partitionHandByFinger(glovesPartitionedByHand, "left");
            HandStatistics leftStats = new CalculateGloveStatistics("left").apply(left);
            sb.append(leftStats.toReportString());
            sb.append(NEWLINE).append(NEWLINE);

            Map<Finger, List<GloveDataEntity>> right = partitionHandByFinger(glovesPartitionedByHand, "right");
            HandStatistics rightStats = new CalculateGloveStatistics("right").apply(right);
            sb.append(rightStats.toReportString());
            sb.append(NEWLINE).append(NEWLINE);

            Map<Long, List<GloveDataEntity>> leftTimely = partitionHandWithTimeSeries(glovesPartitionedByHand, "left");
            TimeIntervalHandStatistics leftTimelyStats = new CalculateTimeIntervalStatistics("left").apply(leftTimely);
            sb.append(leftTimelyStats.toReportString());
            sb.append(NEWLINE).append(NEWLINE);

            Map<Long, List<GloveDataEntity>> rightTimely = partitionHandWithTimeSeries(glovesPartitionedByHand, "right");
            TimeIntervalHandStatistics rightTimelyStats = new CalculateTimeIntervalStatistics("right").apply(rightTimely);
            sb.append(rightTimelyStats.toReportString());
            sb.append(NEWLINE).append(NEWLINE);
        }

        if (!research.getKinectData().isEmpty()) {
            sb.append("-------------- Statystyki dla Kinect - normy --------------");
            final List<KinectDataEntity> kinectData = research.getKinectData();
            Map<Long, List<KinectDataEntity>> kinectDataPartitionedByTimeInterval = kinectData.stream()
                .collect(groupingBy(datum -> datum.getTimestamp().toEpochMilli() / 1000));

            KinectStatistics kinectStats = new CalculateKinectStatistics().apply(kinectData);
            sb.append(kinectStats.toString());
            sb.append(NEWLINE).append(NEWLINE);

            TimeIntervalKinectStatistics kinectTimely = new CalculateTimeIntervalKinectStatistics().apply(kinectDataPartitionedByTimeInterval);
            sb.append(kinectTimely.toString());
            sb.append(NEWLINE).append(NEWLINE);

            List<PointDistance> pointDistances = new CalculatePointDistances().apply(kinectData);
            PointDistanceStatistics pointDistanceStats = new CalculatePointDistanceStatistics().apply(pointDistances);
            sb.append(pointDistanceStats.toReportString());
            sb.append(NEWLINE).append(NEWLINE);

            TimeIntervalPointDistanceStatistics pointDistanceTimelyStats = new CalculateTimeIntervalPointDistanceStatistics().apply(kinectDataPartitionedByTimeInterval);
            pointDistanceTimelyStats.getStatistics().forEach(e -> sb.append(e.toReportString()));
            sb.append(NEWLINE).append(NEWLINE);
        }

        if (!research.getPulsometerData().isEmpty()) {
            sb.append("-------------- Statystyki dla Pulsometru --------------");
            List<PulsometerDataEntity> pulsometerDataDtos = research.getPulsometerData();

            PulsometerStatistics pulsometerStatistics = new CalculatePulsometerStatistics().apply(pulsometerDataDtos);
            sb.append(pulsometerStatistics.toReportString());
            sb.append(NEWLINE).append(NEWLINE);
        }

        return new ReportBytesResource(sb.toString());
    }

    private Map<Long, List<GloveDataEntity>> partitionHandWithTimeSeries(Map<String, List<GloveDataEntity>> rawDataPartitionedByHand, String hand) {
        return rawDataPartitionedByHand.get(hand).stream()
            .collect(groupingBy(datum -> datum.getTimestamp().toEpochMilli() / 1000));
    }

    private Map<Finger, List<GloveDataEntity>> partitionHandByFinger(Map<String, List<GloveDataEntity>> rawDataPartitionedByHand, String hand) {
        return rawDataPartitionedByHand.get(hand).stream()
            .collect(groupingBy(GloveFunctions.sensorToFinger()));
    }
}
