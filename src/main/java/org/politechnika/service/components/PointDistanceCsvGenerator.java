package org.politechnika.service.components;

import org.politechnika.domain.GloveDataEntity;
import org.politechnika.domain.KinectDataEntity;
import org.politechnika.domain.PulsometerDataEntity;
import org.politechnika.domain.ResearchEntity;
import org.politechnika.service.data_parser.BeanToCsvParser;
import org.politechnika.service.data_parser.model.OneHandGloveRawData;
import org.politechnika.service.report.functions.GloveFunctions;
import org.politechnika.service.report.glove_functions.CalculateGloveStatistics;
import org.politechnika.service.report.glove_functions.CalculateTimeIntervalStatistics;
import org.politechnika.service.report.glove_functions.PartitionDataByHand;
import org.politechnika.service.report.kinect_functions.*;
import org.politechnika.service.report.model.glove.Finger;
import org.politechnika.service.report.model.glove.HandStatistics;
import org.politechnika.service.report.model.glove.TimeIntervalHandStatistics;
import org.politechnika.service.report.model.kinect.*;
import org.politechnika.service.report.model.pulsometer.PulsometerStatistics;
import org.politechnika.service.report.pulsometer_functions.CalculatePulsometerStatistics;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;
import static org.politechnika.commons.Separators.NEWLINE;

@Component
@SuppressWarnings("Duplicates")
public class PointDistanceCsvGenerator {

    public ReportBytesResource generate(ResearchEntity research) {

        if (!research.getKinectData().isEmpty()) {
            final List<KinectDataEntity> kinectData = research.getKinectData();
            List<PointDistance> pointDistances = new CalculatePointDistances().apply(kinectData);

            return tryWriteDataToCsv(pointDistances);
        }

        throw new IllegalStateException("Sum Ting Wong found in kinect point dist csv report");
    }


    private ReportBytesResource tryWriteDataToCsv(List<PointDistance> res) {
        String resultString = new BeanToCsvParser().parseToCsvString(res);
        return new ReportBytesResource(resultString);
    }

}
