package org.politechnika.service.components;

import com.google.common.base.Charsets;
import org.springframework.core.io.ByteArrayResource;

import java.nio.charset.Charset;

public class ReportBytesResource {

    private byte[] reportBytes;

    public ReportBytesResource(String reportString) {
        this.reportBytes = reportString.getBytes(Charsets.UTF_8);
    }

    public long getContentLength() {
        return reportBytes.length;
    }

    public ByteArrayResource toByteArrayResource() {
        return new ByteArrayResource(reportBytes);
    }
}
