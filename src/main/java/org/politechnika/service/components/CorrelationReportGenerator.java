package org.politechnika.service.components;

import org.politechnika.domain.GloveDataEntity;
import org.politechnika.domain.KinectDataEntity;
import org.politechnika.domain.PulsometerDataEntity;
import org.politechnika.domain.ResearchEntity;
import org.politechnika.service.report.functions.GloveFunctions;
import org.politechnika.service.report.glove_functions.CalculateAndCachePearsonCorrelations;
import org.politechnika.service.report.glove_functions.CalculateGloveStatistics;
import org.politechnika.service.report.glove_functions.CalculateTimeIntervalStatistics;
import org.politechnika.service.report.glove_functions.PartitionDataByHand;
import org.politechnika.service.report.kinect_functions.*;
import org.politechnika.service.report.model.glove.Finger;
import org.politechnika.service.report.model.glove.HandStatistics;
import org.politechnika.service.report.model.glove.TimeIntervalHandStatistics;
import org.politechnika.service.report.model.kinect.*;
import org.politechnika.service.report.model.pulsometer.PulsometerStatistics;
import org.politechnika.service.report.pulsometer_functions.CalculatePulsometerStatistics;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;
import static org.politechnika.commons.Separators.NEWLINE;

@SuppressWarnings("Duplicates")
@Component
public class CorrelationReportGenerator {

    public ReportBytesResource generate(ResearchEntity research) {
        StringBuilder sb = new StringBuilder();
        if (!research.getGloveData().isEmpty()) {
            final List<GloveDataEntity> gloveData = research.getGloveData();
            Map<String, List<GloveDataEntity>> glovesPartitionedByHand = new PartitionDataByHand().apply(gloveData);

            Map<Finger, List<GloveDataEntity>> left = partitionHandByFinger(glovesPartitionedByHand, "left");
            Map<Finger, List<GloveDataEntity>> right = partitionHandByFinger(glovesPartitionedByHand, "right");

            String leftCorr = new CalculateAndCachePearsonCorrelations("left").apply(left);
            String rightCorr = new CalculateAndCachePearsonCorrelations("right").apply(right);

            sb
                .append("-------------- Tabela korelacji dla palców lewej ręki 5DT (kciuk, wskazujący, środkowy, serdeczny, mały)--------------")
                .append(NEWLINE).append(NEWLINE)
                .append(leftCorr)
                .append(NEWLINE)
                .append("-------------- Tabela korelacji dla palców prawej ręki 5DT (kciuk, wskazujący, środkowy, serdeczny, mały)--------------")
                .append(NEWLINE).append(NEWLINE)
                .append(rightCorr)
                .append(NEWLINE);
        }

        if (!research.getKinectData().isEmpty()) {
            sb.append("-------------- Statystyki dla Kinect - normy --------------");
            final List<KinectDataEntity> kinectData = research.getKinectData();
            List<PointDistance> pointDistances = new CalculatePointDistances().apply(kinectData);

            new org.politechnika.service.report.kinect_functions.CalculateAndCachePearsonCorrelations().apply(pointDistances);

            sb
                .append("-------------- Tabela korelacji dla Kinect - normy --------------")
                .append(NEWLINE)
                .append("[ SPINE_BASE, SPINE_MID, NECK, HEAD, SHOULDER_LEFT, SHOULDER_RIGHT, ELBOW_LEFT, ELBOW_RIGHT, WRIST_LEFT, WRIST_RIGHT, HAND_LEFT, HAND_RIGHT, HIP_LEFT, HIP_RIGHT, KNEE_LEFT, KNEE_RIGHT, ANKLE_LEFT, ANKLE_RIGHT, FOOT_LEFT, FOOT_RIGHT, SPINE_SHOULDER, HAND_TIP_LEFT, HAND_TIP_RIGHT, THUMB_LEFT, THUMB_RIGHT ]")
                .append(NEWLINE).append(NEWLINE)
                .append(pointDistances)
                .append(NEWLINE);
        }

        return new ReportBytesResource(sb.toString());
    }

    private Map<Finger, List<GloveDataEntity>> partitionHandByFinger(Map<String, List<GloveDataEntity>> rawDataPartitionedByHand, String hand) {
        return rawDataPartitionedByHand.get(hand).stream()
            .collect(groupingBy(GloveFunctions.sensorToFinger()));
    }
}
