package org.politechnika.service.components;

import org.politechnika.domain.GloveDataEntity;
import org.politechnika.domain.ResearchEntity;
import org.politechnika.service.report.functions.GloveFunctions;
import org.politechnika.service.report.glove_functions.CreateOneHandRawDataCsv;
import org.politechnika.service.report.glove_functions.PartitionDataByHand;
import org.politechnika.service.report.model.glove.Finger;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;

@Component
@SuppressWarnings("Duplicates")
public class HandCsvGenerator {

    public ReportBytesResource generate(final ResearchEntity research, final String hand) {
        if (!research.getGloveData().isEmpty()) {
            final List<GloveDataEntity> gloveData = research.getGloveData();
            Map<String, List<GloveDataEntity>> glovesPartitionedByHand = new PartitionDataByHand().apply(gloveData);
            Map<Finger, List<GloveDataEntity>> fingerListMap = partitionHandByFinger(glovesPartitionedByHand, hand);

            return new CreateOneHandRawDataCsv(hand).apply(fingerListMap);
        }

        throw new IllegalStateException("wrong hand");
    }

    private Map<Finger, List<GloveDataEntity>> partitionHandByFinger(Map<String, List<GloveDataEntity>> rawDataPartitionedByHand, String hand) {
        return rawDataPartitionedByHand.get(hand).stream()
            .collect(groupingBy(GloveFunctions.sensorToFinger()));
    }
}
