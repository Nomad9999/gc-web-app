package org.politechnika.service;

import org.politechnika.domain.GloveDataEntity;
import org.politechnika.domain.KinectDataEntity;
import org.politechnika.domain.PulsometerDataEntity;
import org.politechnika.domain.ResearchEntity;
import org.politechnika.repository.ResearchDataRepository;
import org.politechnika.service.data_parser.CsvToBeanParser;
import org.politechnika.service.data_parser.model.GloveDataDto;
import org.politechnika.service.data_parser.model.KinectDataDto;
import org.politechnika.service.data_parser.model.PulsometerDataDto;
import org.politechnika.service.data_parser.model.TimestampPulsometerData;
import org.politechnika.service.data_parser.strategy.CsvPulsometerParsingStrategy;
import org.politechnika.service.data_parser.strategy.GloveParsingStrategy;
import org.politechnika.service.data_parser.strategy.KinectParsingStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

@Service
@Transactional
public class ResearchService {

    private CsvToBeanParser csvToBeanParser;
    private ResearchDataRepository researchDataRepository;

    @Autowired
    public ResearchService(CsvToBeanParser csvToBeanParser, ResearchDataRepository researchDataRepository) {
        this.csvToBeanParser = csvToBeanParser;
        this.researchDataRepository = researchDataRepository;
    }

    public void loadGlove(@NonNull MultipartFile file, @NonNull String researchName) throws IOException {
        List<GloveDataEntity> gloveData = csvToBeanParser
            .parseToBean(file, new GloveParsingStrategy())
            .stream()
            .map(GloveDataDto::toEntity)
            .collect(Collectors.toList());
        ResearchEntity researchEntity = findOrCreateResearch(researchName);

        researchEntity.getGloveData().clear();
        researchEntity.setGloveData(gloveData);
        gloveData.forEach(data -> data.setResearch(researchEntity));
        researchDataRepository.save(researchEntity);
    }

    public void loadKinect(@NonNull MultipartFile file, @NonNull String researchName) throws IOException {
        List<KinectDataEntity> kinectData = csvToBeanParser
            .parseToBean(file, new KinectParsingStrategy())
            .stream()
            .map(KinectDataDto::toEntity)
            .collect(Collectors.toList());
        ResearchEntity researchEntity = findOrCreateResearch(researchName);

        researchEntity.getKinectData().clear();
        researchEntity.setKinectData(kinectData);
        kinectData.forEach(data -> data.setResearch(researchEntity));
        researchDataRepository.save(researchEntity);
    }

    public void loadPulsometer(@NonNull MultipartFile file, @NonNull String researchName) throws IOException {
        List<PulsometerDataEntity> pulsometerData = csvToBeanParser
            .parseToBean(file, new CsvPulsometerParsingStrategy())
            .stream()
            .map(TimestampPulsometerData::toEntity)
            .collect(Collectors.toList());
        ResearchEntity researchEntity = findOrCreateResearch(researchName);

        researchEntity.getPulsometerData().clear();
        researchEntity.setPulsometerData(pulsometerData);
        pulsometerData.forEach(data -> data.setResearch(researchEntity));
        researchDataRepository.save(researchEntity);
    }

    private ResearchEntity findOrCreateResearch(@NonNull String researchName) {
        return researchDataRepository.findByResearchName(researchName).orElseGet(() -> {
            ResearchEntity re = new ResearchEntity();
            re.setResearchName(researchName);
            return re;
        });
    }

    public ResearchEntity getResearchByName(String researchName) {
        return researchDataRepository.findByResearchName(researchName)
            .orElseThrow(() -> new EntityNotFoundException("Research was not found for name: " + researchName));
    }

    public String getRandomName() {
        List<String> allResearchNames = researchDataRepository.getAllResearchNames();
        int i = ThreadLocalRandom.current().nextInt(0, allResearchNames.size() - 1);
        return allResearchNames.get(i);
    }
}
