package org.politechnika.service.data_parser;

import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;

@Component
public class BeanToCsvParser {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(BeanToCsvParser.class);

    public <T> void parseToCsv(List<T> beans, String path)  {
        try (Writer writer = new StringWriter()) {
            StatefulBeanToCsv<T> parser = new StatefulBeanToCsvBuilder<T>(writer)
                    .withSeparator(';').build();
            parser.write(beans);
        } catch (CsvDataTypeMismatchException e) {
            log.error("Data type mismatch: {}", e.getMessage());
            throw new IllegalStateException("Data type mismatch", e);
        } catch (CsvRequiredFieldEmptyException e) {
            log.error("Some field was required; {}", e.getMessage());
            throw new IllegalStateException("Required field was empty", e);
        } catch (IOException e) {
            log.error("Error while writing data: {}", e.getMessage());
            throw new IllegalStateException("Data type mismatch", e);
        }
    }

    public <T> String parseToCsvString(List<T> beans)  {
        try (Writer writer = new StringWriter()) {
            StatefulBeanToCsv<T> parser = new StatefulBeanToCsvBuilder<T>(writer)
                .withSeparator(';').build();
            parser.write(beans);
            return writer.toString();
        } catch (CsvDataTypeMismatchException e) {
            log.error("Data type mismatch: {}", e.getMessage());
            throw new IllegalStateException("Data type mismatch", e);
        } catch (CsvRequiredFieldEmptyException e) {
            log.error("Some field was required; {}", e.getMessage());
            throw new IllegalStateException("Required field was empty", e);
        } catch (IOException e) {
            log.error("Error while writing data: {}", e.getMessage());
            throw new IllegalStateException("Data type mismatch", e);
        }
    }
}
