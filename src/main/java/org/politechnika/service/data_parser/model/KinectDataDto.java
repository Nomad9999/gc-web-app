package org.politechnika.service.data_parser.model;

import com.opencsv.bean.CsvCustomBindByName;
import org.politechnika.domain.KinectDataEntity;
import org.politechnika.service.data_parser.converter.DoubleWithCommaConverter;
import org.politechnika.service.data_parser.converter.TimestampToInstantConverter;

import java.time.Instant;

public class KinectDataDto implements DataDto {

    // SpineBase_x|SpineBase_y|SpineBase_z|
    // SpineMid_x|SpineMid_y|SpineMid_z|
    // Neck_x|Neck_y|Neck_z|
    // Head_x|Head_y|Head_z| //
    // ShoulderLeft_x|ShoulderLeft_y|ShoulderLeft_z|
    // ElbowLeft_x|ElbowLeft_y|ElbowLeft_z|
    // WristLeft_x|WristLeft_y|WristLeft_z|
    // HandLeft_x|HandLeft_y|HandLeft_z|
    // ShoulderRight_x|ShoulderRight_y|ShoulderRight_z|
    // ElbowRight_x|ElbowRight_y|ElbowRight_z|
    // WristRight_x|WristRight_y|WristRight_z|
    // HandRight_x|HandRight_y|HandRight_z| //
    // HipLeft_x|HipLeft_y|HipLeft_z|
    // KneeLeft_x|KneeLeft_y|KneeLeft_z|
    // AnkleLeft_x|AnkleLeft_y|AnkleLeft_z|
    // FootLeft_x|FootLeft_y|FootLeft_z|
    // HipRight_x|HipRight_y|HipRight_z|
    // KneeRight_x|KneeRight_y|KneeRight_z|
    // AnkleRight_x|AnkleRight_y|AnkleRight_z|
    // FootRight_x|FootRight_y|FootRight_z| //
    // SpineShoulder_x|SpineShoulder_y|SpineShoulder_z|
    // HandTipLeft_x|HandTipLeft_y|HandTipLeft_z|
    // ThumbLeft_x|ThumbLeft_y|ThumbLeft_z|
    // HandTipRight_x|HandTipRight_y|HandTipRight_z|
    // ThumbRight_x|ThumbRight_y|ThumbRight_z|
    // TimeStamp

    @CsvCustomBindByName(column = "SpineBase_x", converter = DoubleWithCommaConverter.class)
    private double spineBase_x;

    @CsvCustomBindByName(column = "SpineBase_y", converter = DoubleWithCommaConverter.class)
    private double spineBase_y;

    @CsvCustomBindByName(column = "SpineBase_z", converter = DoubleWithCommaConverter.class)
    private double spineBase_z;

    @CsvCustomBindByName(column = "SpineMid_x", converter = DoubleWithCommaConverter.class)
    private double spineMid_x;

    @CsvCustomBindByName(column = "SpineMid_y", converter = DoubleWithCommaConverter.class)
    private double spineMid_y;

    @CsvCustomBindByName(column = "SpineMid_z", converter = DoubleWithCommaConverter.class)
    private double spineMid_z;

    @CsvCustomBindByName(column = "Neck_x", converter = DoubleWithCommaConverter.class)
    private double neck_x;

    @CsvCustomBindByName(column = "Neck_y", converter = DoubleWithCommaConverter.class)
    private double neck_y;

    @CsvCustomBindByName(column = "Neck_z", converter = DoubleWithCommaConverter.class)
    private double neck_z;

    @CsvCustomBindByName(column = "Head_x", converter = DoubleWithCommaConverter.class)
    private double head_x;

    @CsvCustomBindByName(column = "Head_y", converter = DoubleWithCommaConverter.class)
    private double head_y;

    @CsvCustomBindByName(column = "Head_z", converter = DoubleWithCommaConverter.class)
    private double head_z;

    @CsvCustomBindByName(column = "ShoulderLeft_x", converter = DoubleWithCommaConverter.class)
    private double shoulderLeft_x;

    @CsvCustomBindByName(column = "ShoulderLeft_y", converter = DoubleWithCommaConverter.class)
    private double shoulderLeft_y;

    @CsvCustomBindByName(column = "ShoulderLeft_z", converter = DoubleWithCommaConverter.class)
    private double shoulderLeft_z;

    @CsvCustomBindByName(column = "ElbowLeft_x", converter = DoubleWithCommaConverter.class)
    private double elbowLeft_x;

    @CsvCustomBindByName(column = "ElbowLeft_y", converter = DoubleWithCommaConverter.class)
    private double elbowLeft_y;

    @CsvCustomBindByName(column = "ElbowLeft_z", converter = DoubleWithCommaConverter.class)
    private double elbowLeft_z;

    @CsvCustomBindByName(column = "WristLeft_x", converter = DoubleWithCommaConverter.class)
    private double wristLeft_x;

    @CsvCustomBindByName(column = "WristLeft_y", converter = DoubleWithCommaConverter.class)
    private double wristLeft_y;

    @CsvCustomBindByName(column = "WristLeft_z", converter = DoubleWithCommaConverter.class)
    private double wristLeft_z;

    @CsvCustomBindByName(column = "HandLeft_x", converter = DoubleWithCommaConverter.class)
    private double handLeft_x;

    @CsvCustomBindByName(column = "HandLeft_y", converter = DoubleWithCommaConverter.class)
    private double handLeft_y;

    @CsvCustomBindByName(column = "HandLeft_z", converter = DoubleWithCommaConverter.class)
    private double handLeft_z;

    @CsvCustomBindByName(column = "ShoulderRight_x", converter = DoubleWithCommaConverter.class)
    private double shoulderRight_x;

    @CsvCustomBindByName(column = "ShoulderRight_y", converter = DoubleWithCommaConverter.class)
    private double shoulderRight_y;

    @CsvCustomBindByName(column = "ShoulderRight_z", converter = DoubleWithCommaConverter.class)
    private double shoulderRight_z;

    @CsvCustomBindByName(column = "ElbowRight_x", converter = DoubleWithCommaConverter.class)
    private double elbowRight_x;

    @CsvCustomBindByName(column = "ElbowRight_y", converter = DoubleWithCommaConverter.class)
    private double elbowRight_y;

    @CsvCustomBindByName(column = "ElbowRight_z", converter = DoubleWithCommaConverter.class)
    private double elbowRight_z;

    @CsvCustomBindByName(column = "WristRight_x", converter = DoubleWithCommaConverter.class)
    private double wristRight_x;

    @CsvCustomBindByName(column = "WristRight_y", converter = DoubleWithCommaConverter.class)
    private double wristRight_y;

    @CsvCustomBindByName(column = "WristRight_z", converter = DoubleWithCommaConverter.class)
    private double wristRight_z;

    @CsvCustomBindByName(column = "HandRight_x", converter = DoubleWithCommaConverter.class)
    private double handRight_x;

    @CsvCustomBindByName(column = "HandRight_y", converter = DoubleWithCommaConverter.class)
    private double handRight_y;

    @CsvCustomBindByName(column = "HandRight_z", converter = DoubleWithCommaConverter.class)
    private double handRight_z;

    @CsvCustomBindByName(column = "HipLeft_x", converter = DoubleWithCommaConverter.class)
    private double hipLeft_x;

    @CsvCustomBindByName(column = "HipLeft_y", converter = DoubleWithCommaConverter.class)
    private double hipLeft_y;

    @CsvCustomBindByName(column = "HipLeft_z", converter = DoubleWithCommaConverter.class)
    private double hipLeft_z;

    @CsvCustomBindByName(column = "KneeLeft_x", converter = DoubleWithCommaConverter.class)
    private double kneeLeft_x;

    @CsvCustomBindByName(column = "KneeLeft_y", converter = DoubleWithCommaConverter.class)
    private double kneeLeft_y;

    @CsvCustomBindByName(column = "KneeLeft_z", converter = DoubleWithCommaConverter.class)
    private double kneeLeft_z;

    @CsvCustomBindByName(column = "AnkleLeft_x", converter = DoubleWithCommaConverter.class)
    private double ankleLeft_x;

    @CsvCustomBindByName(column = "AnkleLeft_y", converter = DoubleWithCommaConverter.class)
    private double ankleLeft_y;

    @CsvCustomBindByName(column = "AnkleLeft_z", converter = DoubleWithCommaConverter.class)
    private double ankleLeft_z;

    @CsvCustomBindByName(column = "FootLeft_x", converter = DoubleWithCommaConverter.class)
    private double footLeft_x;

    @CsvCustomBindByName(column = "FootLeft_y", converter = DoubleWithCommaConverter.class)
    private double footLeft_y;

    @CsvCustomBindByName(column = "FootLeft_z", converter = DoubleWithCommaConverter.class)
    private double footLeft_z;

    @CsvCustomBindByName(column = "HipRight_x", converter = DoubleWithCommaConverter.class)
    private double hipRight_x;

    @CsvCustomBindByName(column = "HipRight_y", converter = DoubleWithCommaConverter.class)
    private double hipRight_y;

    @CsvCustomBindByName(column = "HipRight_z", converter = DoubleWithCommaConverter.class)
    private double hipRight_z;

    @CsvCustomBindByName(column = "KneeRight_x", converter = DoubleWithCommaConverter.class)
    private double kneeRight_x;

    @CsvCustomBindByName(column = "KneeRight_y", converter = DoubleWithCommaConverter.class)
    private double kneeRight_y;

    @CsvCustomBindByName(column = "KneeRight_z", converter = DoubleWithCommaConverter.class)
    private double kneeRight_z;

    @CsvCustomBindByName(column = "AnkleRight_x", converter = DoubleWithCommaConverter.class)
    private double ankleRight_x;

    @CsvCustomBindByName(column = "AnkleRight_y", converter = DoubleWithCommaConverter.class)
    private double ankleRight_y;

    @CsvCustomBindByName(column = "AnkleRight_z", converter = DoubleWithCommaConverter.class)
    private double ankleRight_z;

    @CsvCustomBindByName(column = "FootRight_x", converter = DoubleWithCommaConverter.class)
    private double footRight_x;

    @CsvCustomBindByName(column = "FootRight_y", converter = DoubleWithCommaConverter.class)
    private double footRight_y;

    @CsvCustomBindByName(column = "FootRight_z", converter = DoubleWithCommaConverter.class)
    private double footRight_z;

    @CsvCustomBindByName(column = "SpineShoulder_x", converter = DoubleWithCommaConverter.class)
    private double spineShoulder_x;

    @CsvCustomBindByName(column = "SpineShoulder_y", converter = DoubleWithCommaConverter.class)
    private double spineShoulder_y;

    @CsvCustomBindByName(column = "SpineShoulder_z", converter = DoubleWithCommaConverter.class)
    private double spineShoulder_z;

    @CsvCustomBindByName(column = "HandTipLeft_x", converter = DoubleWithCommaConverter.class)
    private double handTipLeft_x;

    @CsvCustomBindByName(column = "HandTipLeft_y", converter = DoubleWithCommaConverter.class)
    private double handTipLeft_y;

    @CsvCustomBindByName(column = "HandTipLeft_z", converter = DoubleWithCommaConverter.class)
    private double handTipLeft_z;

    @CsvCustomBindByName(column = "ThumbLeft_x", converter = DoubleWithCommaConverter.class)
    private double thumbLeft_x;

    @CsvCustomBindByName(column = "ThumbLeft_y", converter = DoubleWithCommaConverter.class)
    private double thumbLeft_y;

    @CsvCustomBindByName(column = "ThumbLeft_z", converter = DoubleWithCommaConverter.class)
    private double thumbLeft_z;

    @CsvCustomBindByName(column = "HandTipRight_x", converter = DoubleWithCommaConverter.class)
    private double handTipRight_x;

    @CsvCustomBindByName(column = "HandTipRight_y", converter = DoubleWithCommaConverter.class)
    private double handTipRight_y;

    @CsvCustomBindByName(column = "HandTipRight_z", converter = DoubleWithCommaConverter.class)
    private double handTipRight_z;

    @CsvCustomBindByName(column = "ThumbRight_x", converter = DoubleWithCommaConverter.class)
    private double thumbRight_x;

    @CsvCustomBindByName(column = "ThumbRight_y", converter = DoubleWithCommaConverter.class)
    private double thumbRight_y;

    @CsvCustomBindByName(column = "ThumbRight_z", converter = DoubleWithCommaConverter.class)
    private double thumbRight_z;

    @CsvCustomBindByName(column = "TimeStamp", converter = TimestampToInstantConverter.class)
    private Instant timestamp;

    public KinectDataEntity toEntity() {
        final KinectDataEntity kinectDataEntity = new KinectDataEntity();
        kinectDataEntity.setSpineBase_x(spineBase_x);
        kinectDataEntity.setSpineBase_y(spineBase_y);
        kinectDataEntity.setSpineBase_z(spineBase_z);
        kinectDataEntity.setSpineMid_x(spineMid_x);
        kinectDataEntity.setSpineMid_y(spineMid_y);
        kinectDataEntity.setSpineMid_z(spineMid_z);
        kinectDataEntity.setNeck_x(neck_x);
        kinectDataEntity.setNeck_y(neck_y);
        kinectDataEntity.setNeck_z(neck_z);
        kinectDataEntity.setHead_x(head_x);
        kinectDataEntity.setHead_y(head_y);
        kinectDataEntity.setHead_z(head_z);
        kinectDataEntity.setShoulderLeft_x(shoulderLeft_x);
        kinectDataEntity.setShoulderLeft_y(shoulderLeft_y);
        kinectDataEntity.setShoulderLeft_z(shoulderLeft_z);
        kinectDataEntity.setElbowLeft_x(elbowLeft_x);
        kinectDataEntity.setElbowLeft_y(elbowLeft_y);
        kinectDataEntity.setElbowLeft_z(elbowLeft_z);
        kinectDataEntity.setWristLeft_x(wristLeft_x);
        kinectDataEntity.setWristLeft_y(wristLeft_y);
        kinectDataEntity.setWristLeft_z(wristLeft_z);
        kinectDataEntity.setHandLeft_x(handLeft_x);
        kinectDataEntity.setHandLeft_y(handLeft_y);
        kinectDataEntity.setHandLeft_z(handLeft_z);
        kinectDataEntity.setShoulderRight_x(shoulderRight_x);
        kinectDataEntity.setShoulderRight_y(shoulderRight_y);
        kinectDataEntity.setShoulderRight_z(shoulderRight_z);
        kinectDataEntity.setElbowRight_x(elbowRight_x);
        kinectDataEntity.setElbowRight_y(elbowRight_y);
        kinectDataEntity.setElbowRight_z(elbowRight_z);
        kinectDataEntity.setWristRight_x(wristRight_x);
        kinectDataEntity.setWristRight_y(wristRight_y);
        kinectDataEntity.setWristRight_z(wristRight_z);
        kinectDataEntity.setHandRight_x(handRight_x);
        kinectDataEntity.setHandRight_y(handRight_y);
        kinectDataEntity.setHandRight_z(handRight_z);
        kinectDataEntity.setHipLeft_x(hipLeft_x);
        kinectDataEntity.setHipLeft_y(hipLeft_y);
        kinectDataEntity.setHipLeft_z(hipLeft_z);
        kinectDataEntity.setKneeLeft_x(kneeLeft_x);
        kinectDataEntity.setKneeLeft_y(kneeLeft_y);
        kinectDataEntity.setKneeLeft_z(kneeLeft_z);
        kinectDataEntity.setAnkleLeft_x(ankleLeft_x);
        kinectDataEntity.setAnkleLeft_y(ankleLeft_y);
        kinectDataEntity.setAnkleLeft_z(ankleLeft_z);
        kinectDataEntity.setFootLeft_x(footLeft_x);
        kinectDataEntity.setFootLeft_y(footLeft_y);
        kinectDataEntity.setFootLeft_z(footLeft_z);
        kinectDataEntity.setHipRight_x(hipRight_x);
        kinectDataEntity.setHipRight_y(hipRight_y);
        kinectDataEntity.setHipRight_z(hipRight_z);
        kinectDataEntity.setKneeRight_x(kneeRight_x);
        kinectDataEntity.setKneeRight_y(kneeRight_y);
        kinectDataEntity.setKneeRight_z(kneeRight_z);
        kinectDataEntity.setAnkleRight_x(ankleRight_x);
        kinectDataEntity.setAnkleRight_y(ankleRight_y);
        kinectDataEntity.setAnkleRight_z(ankleRight_z);
        kinectDataEntity.setFootRight_x(footRight_x);
        kinectDataEntity.setFootRight_y(footRight_y);
        kinectDataEntity.setFootRight_z(footRight_z);
        kinectDataEntity.setSpineShoulder_x(spineShoulder_x);
        kinectDataEntity.setSpineShoulder_y(spineShoulder_y);
        kinectDataEntity.setSpineShoulder_z(spineShoulder_z);
        kinectDataEntity.setHandTipLeft_x(handTipLeft_x);
        kinectDataEntity.setHandTipLeft_y(handTipLeft_y);
        kinectDataEntity.setHandTipLeft_z(handTipLeft_z);
        kinectDataEntity.setThumbLeft_x(thumbLeft_x);
        kinectDataEntity.setThumbLeft_y(thumbLeft_y);
        kinectDataEntity.setThumbLeft_z(thumbLeft_z);
        kinectDataEntity.setHandTipRight_x(handTipRight_x);
        kinectDataEntity.setHandTipRight_y(handTipRight_y);
        kinectDataEntity.setHandTipRight_z(handTipRight_z);
        kinectDataEntity.setTimestamp(timestamp);

        return kinectDataEntity;
    }

    public KinectDataDto(double spineBase_x, double spineBase_y, double spineBase_z, double spineMid_x, double spineMid_y, double spineMid_z, double neck_x, double neck_y, double neck_z, double head_x, double head_y, double head_z, double shoulderLeft_x, double shoulderLeft_y, double shoulderLeft_z, double elbowLeft_x, double elbowLeft_y, double elbowLeft_z, double wristLeft_x, double wristLeft_y, double wristLeft_z, double handLeft_x, double handLeft_y, double handLeft_z, double shoulderRight_x, double shoulderRight_y, double shoulderRight_z, double elbowRight_x, double elbowRight_y, double elbowRight_z, double wristRight_x, double wristRight_y, double wristRight_z, double handRight_x, double handRight_y, double handRight_z, double hipLeft_x, double hipLeft_y, double hipLeft_z, double kneeLeft_x, double kneeLeft_y, double kneeLeft_z, double ankleLeft_x, double ankleLeft_y, double ankleLeft_z, double footLeft_x, double footLeft_y, double footLeft_z, double hipRight_x, double hipRight_y, double hipRight_z, double kneeRight_x, double kneeRight_y, double kneeRight_z, double ankleRight_x, double ankleRight_y, double ankleRight_z, double footRight_x, double footRight_y, double footRight_z, double spineShoulder_x, double spineShoulder_y, double spineShoulder_z, double handTipLeft_x, double handTipLeft_y, double handTipLeft_z, double thumbLeft_x, double thumbLeft_y, double thumbLeft_z, double handTipRight_x, double handTipRight_y, double handTipRight_z, double thumbRight_x, double thumbRight_y, double thumbRight_z, Instant timestamp) {
        this.spineBase_x = spineBase_x;
        this.spineBase_y = spineBase_y;
        this.spineBase_z = spineBase_z;
        this.spineMid_x = spineMid_x;
        this.spineMid_y = spineMid_y;
        this.spineMid_z = spineMid_z;
        this.neck_x = neck_x;
        this.neck_y = neck_y;
        this.neck_z = neck_z;
        this.head_x = head_x;
        this.head_y = head_y;
        this.head_z = head_z;
        this.shoulderLeft_x = shoulderLeft_x;
        this.shoulderLeft_y = shoulderLeft_y;
        this.shoulderLeft_z = shoulderLeft_z;
        this.elbowLeft_x = elbowLeft_x;
        this.elbowLeft_y = elbowLeft_y;
        this.elbowLeft_z = elbowLeft_z;
        this.wristLeft_x = wristLeft_x;
        this.wristLeft_y = wristLeft_y;
        this.wristLeft_z = wristLeft_z;
        this.handLeft_x = handLeft_x;
        this.handLeft_y = handLeft_y;
        this.handLeft_z = handLeft_z;
        this.shoulderRight_x = shoulderRight_x;
        this.shoulderRight_y = shoulderRight_y;
        this.shoulderRight_z = shoulderRight_z;
        this.elbowRight_x = elbowRight_x;
        this.elbowRight_y = elbowRight_y;
        this.elbowRight_z = elbowRight_z;
        this.wristRight_x = wristRight_x;
        this.wristRight_y = wristRight_y;
        this.wristRight_z = wristRight_z;
        this.handRight_x = handRight_x;
        this.handRight_y = handRight_y;
        this.handRight_z = handRight_z;
        this.hipLeft_x = hipLeft_x;
        this.hipLeft_y = hipLeft_y;
        this.hipLeft_z = hipLeft_z;
        this.kneeLeft_x = kneeLeft_x;
        this.kneeLeft_y = kneeLeft_y;
        this.kneeLeft_z = kneeLeft_z;
        this.ankleLeft_x = ankleLeft_x;
        this.ankleLeft_y = ankleLeft_y;
        this.ankleLeft_z = ankleLeft_z;
        this.footLeft_x = footLeft_x;
        this.footLeft_y = footLeft_y;
        this.footLeft_z = footLeft_z;
        this.hipRight_x = hipRight_x;
        this.hipRight_y = hipRight_y;
        this.hipRight_z = hipRight_z;
        this.kneeRight_x = kneeRight_x;
        this.kneeRight_y = kneeRight_y;
        this.kneeRight_z = kneeRight_z;
        this.ankleRight_x = ankleRight_x;
        this.ankleRight_y = ankleRight_y;
        this.ankleRight_z = ankleRight_z;
        this.footRight_x = footRight_x;
        this.footRight_y = footRight_y;
        this.footRight_z = footRight_z;
        this.spineShoulder_x = spineShoulder_x;
        this.spineShoulder_y = spineShoulder_y;
        this.spineShoulder_z = spineShoulder_z;
        this.handTipLeft_x = handTipLeft_x;
        this.handTipLeft_y = handTipLeft_y;
        this.handTipLeft_z = handTipLeft_z;
        this.thumbLeft_x = thumbLeft_x;
        this.thumbLeft_y = thumbLeft_y;
        this.thumbLeft_z = thumbLeft_z;
        this.handTipRight_x = handTipRight_x;
        this.handTipRight_y = handTipRight_y;
        this.handTipRight_z = handTipRight_z;
        this.thumbRight_x = thumbRight_x;
        this.thumbRight_y = thumbRight_y;
        this.thumbRight_z = thumbRight_z;
        this.timestamp = timestamp;
    }

    public KinectDataDto() {
    }

    public static KinectDataDtoBuilder builder() {
        return new KinectDataDtoBuilder();
    }

    public double getSpineBase_x() {
        return this.spineBase_x;
    }

    public double getSpineBase_y() {
        return this.spineBase_y;
    }

    public double getSpineBase_z() {
        return this.spineBase_z;
    }

    public double getSpineMid_x() {
        return this.spineMid_x;
    }

    public double getSpineMid_y() {
        return this.spineMid_y;
    }

    public double getSpineMid_z() {
        return this.spineMid_z;
    }

    public double getNeck_x() {
        return this.neck_x;
    }

    public double getNeck_y() {
        return this.neck_y;
    }

    public double getNeck_z() {
        return this.neck_z;
    }

    public double getHead_x() {
        return this.head_x;
    }

    public double getHead_y() {
        return this.head_y;
    }

    public double getHead_z() {
        return this.head_z;
    }

    public double getShoulderLeft_x() {
        return this.shoulderLeft_x;
    }

    public double getShoulderLeft_y() {
        return this.shoulderLeft_y;
    }

    public double getShoulderLeft_z() {
        return this.shoulderLeft_z;
    }

    public double getElbowLeft_x() {
        return this.elbowLeft_x;
    }

    public double getElbowLeft_y() {
        return this.elbowLeft_y;
    }

    public double getElbowLeft_z() {
        return this.elbowLeft_z;
    }

    public double getWristLeft_x() {
        return this.wristLeft_x;
    }

    public double getWristLeft_y() {
        return this.wristLeft_y;
    }

    public double getWristLeft_z() {
        return this.wristLeft_z;
    }

    public double getHandLeft_x() {
        return this.handLeft_x;
    }

    public double getHandLeft_y() {
        return this.handLeft_y;
    }

    public double getHandLeft_z() {
        return this.handLeft_z;
    }

    public double getShoulderRight_x() {
        return this.shoulderRight_x;
    }

    public double getShoulderRight_y() {
        return this.shoulderRight_y;
    }

    public double getShoulderRight_z() {
        return this.shoulderRight_z;
    }

    public double getElbowRight_x() {
        return this.elbowRight_x;
    }

    public double getElbowRight_y() {
        return this.elbowRight_y;
    }

    public double getElbowRight_z() {
        return this.elbowRight_z;
    }

    public double getWristRight_x() {
        return this.wristRight_x;
    }

    public double getWristRight_y() {
        return this.wristRight_y;
    }

    public double getWristRight_z() {
        return this.wristRight_z;
    }

    public double getHandRight_x() {
        return this.handRight_x;
    }

    public double getHandRight_y() {
        return this.handRight_y;
    }

    public double getHandRight_z() {
        return this.handRight_z;
    }

    public double getHipLeft_x() {
        return this.hipLeft_x;
    }

    public double getHipLeft_y() {
        return this.hipLeft_y;
    }

    public double getHipLeft_z() {
        return this.hipLeft_z;
    }

    public double getKneeLeft_x() {
        return this.kneeLeft_x;
    }

    public double getKneeLeft_y() {
        return this.kneeLeft_y;
    }

    public double getKneeLeft_z() {
        return this.kneeLeft_z;
    }

    public double getAnkleLeft_x() {
        return this.ankleLeft_x;
    }

    public double getAnkleLeft_y() {
        return this.ankleLeft_y;
    }

    public double getAnkleLeft_z() {
        return this.ankleLeft_z;
    }

    public double getFootLeft_x() {
        return this.footLeft_x;
    }

    public double getFootLeft_y() {
        return this.footLeft_y;
    }

    public double getFootLeft_z() {
        return this.footLeft_z;
    }

    public double getHipRight_x() {
        return this.hipRight_x;
    }

    public double getHipRight_y() {
        return this.hipRight_y;
    }

    public double getHipRight_z() {
        return this.hipRight_z;
    }

    public double getKneeRight_x() {
        return this.kneeRight_x;
    }

    public double getKneeRight_y() {
        return this.kneeRight_y;
    }

    public double getKneeRight_z() {
        return this.kneeRight_z;
    }

    public double getAnkleRight_x() {
        return this.ankleRight_x;
    }

    public double getAnkleRight_y() {
        return this.ankleRight_y;
    }

    public double getAnkleRight_z() {
        return this.ankleRight_z;
    }

    public double getFootRight_x() {
        return this.footRight_x;
    }

    public double getFootRight_y() {
        return this.footRight_y;
    }

    public double getFootRight_z() {
        return this.footRight_z;
    }

    public double getSpineShoulder_x() {
        return this.spineShoulder_x;
    }

    public double getSpineShoulder_y() {
        return this.spineShoulder_y;
    }

    public double getSpineShoulder_z() {
        return this.spineShoulder_z;
    }

    public double getHandTipLeft_x() {
        return this.handTipLeft_x;
    }

    public double getHandTipLeft_y() {
        return this.handTipLeft_y;
    }

    public double getHandTipLeft_z() {
        return this.handTipLeft_z;
    }

    public double getThumbLeft_x() {
        return this.thumbLeft_x;
    }

    public double getThumbLeft_y() {
        return this.thumbLeft_y;
    }

    public double getThumbLeft_z() {
        return this.thumbLeft_z;
    }

    public double getHandTipRight_x() {
        return this.handTipRight_x;
    }

    public double getHandTipRight_y() {
        return this.handTipRight_y;
    }

    public double getHandTipRight_z() {
        return this.handTipRight_z;
    }

    public double getThumbRight_x() {
        return this.thumbRight_x;
    }

    public double getThumbRight_y() {
        return this.thumbRight_y;
    }

    public double getThumbRight_z() {
        return this.thumbRight_z;
    }

    public Instant getTimestamp() {
        return this.timestamp;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof KinectDataDto)) return false;
        final KinectDataDto other = (KinectDataDto) o;
        if (!other.canEqual((Object) this)) return false;
        if (Double.compare(this.getSpineBase_x(), other.getSpineBase_x()) != 0) return false;
        if (Double.compare(this.getSpineBase_y(), other.getSpineBase_y()) != 0) return false;
        if (Double.compare(this.getSpineBase_z(), other.getSpineBase_z()) != 0) return false;
        if (Double.compare(this.getSpineMid_x(), other.getSpineMid_x()) != 0) return false;
        if (Double.compare(this.getSpineMid_y(), other.getSpineMid_y()) != 0) return false;
        if (Double.compare(this.getSpineMid_z(), other.getSpineMid_z()) != 0) return false;
        if (Double.compare(this.getNeck_x(), other.getNeck_x()) != 0) return false;
        if (Double.compare(this.getNeck_y(), other.getNeck_y()) != 0) return false;
        if (Double.compare(this.getNeck_z(), other.getNeck_z()) != 0) return false;
        if (Double.compare(this.getHead_x(), other.getHead_x()) != 0) return false;
        if (Double.compare(this.getHead_y(), other.getHead_y()) != 0) return false;
        if (Double.compare(this.getHead_z(), other.getHead_z()) != 0) return false;
        if (Double.compare(this.getShoulderLeft_x(), other.getShoulderLeft_x()) != 0) return false;
        if (Double.compare(this.getShoulderLeft_y(), other.getShoulderLeft_y()) != 0) return false;
        if (Double.compare(this.getShoulderLeft_z(), other.getShoulderLeft_z()) != 0) return false;
        if (Double.compare(this.getElbowLeft_x(), other.getElbowLeft_x()) != 0) return false;
        if (Double.compare(this.getElbowLeft_y(), other.getElbowLeft_y()) != 0) return false;
        if (Double.compare(this.getElbowLeft_z(), other.getElbowLeft_z()) != 0) return false;
        if (Double.compare(this.getWristLeft_x(), other.getWristLeft_x()) != 0) return false;
        if (Double.compare(this.getWristLeft_y(), other.getWristLeft_y()) != 0) return false;
        if (Double.compare(this.getWristLeft_z(), other.getWristLeft_z()) != 0) return false;
        if (Double.compare(this.getHandLeft_x(), other.getHandLeft_x()) != 0) return false;
        if (Double.compare(this.getHandLeft_y(), other.getHandLeft_y()) != 0) return false;
        if (Double.compare(this.getHandLeft_z(), other.getHandLeft_z()) != 0) return false;
        if (Double.compare(this.getShoulderRight_x(), other.getShoulderRight_x()) != 0) return false;
        if (Double.compare(this.getShoulderRight_y(), other.getShoulderRight_y()) != 0) return false;
        if (Double.compare(this.getShoulderRight_z(), other.getShoulderRight_z()) != 0) return false;
        if (Double.compare(this.getElbowRight_x(), other.getElbowRight_x()) != 0) return false;
        if (Double.compare(this.getElbowRight_y(), other.getElbowRight_y()) != 0) return false;
        if (Double.compare(this.getElbowRight_z(), other.getElbowRight_z()) != 0) return false;
        if (Double.compare(this.getWristRight_x(), other.getWristRight_x()) != 0) return false;
        if (Double.compare(this.getWristRight_y(), other.getWristRight_y()) != 0) return false;
        if (Double.compare(this.getWristRight_z(), other.getWristRight_z()) != 0) return false;
        if (Double.compare(this.getHandRight_x(), other.getHandRight_x()) != 0) return false;
        if (Double.compare(this.getHandRight_y(), other.getHandRight_y()) != 0) return false;
        if (Double.compare(this.getHandRight_z(), other.getHandRight_z()) != 0) return false;
        if (Double.compare(this.getHipLeft_x(), other.getHipLeft_x()) != 0) return false;
        if (Double.compare(this.getHipLeft_y(), other.getHipLeft_y()) != 0) return false;
        if (Double.compare(this.getHipLeft_z(), other.getHipLeft_z()) != 0) return false;
        if (Double.compare(this.getKneeLeft_x(), other.getKneeLeft_x()) != 0) return false;
        if (Double.compare(this.getKneeLeft_y(), other.getKneeLeft_y()) != 0) return false;
        if (Double.compare(this.getKneeLeft_z(), other.getKneeLeft_z()) != 0) return false;
        if (Double.compare(this.getAnkleLeft_x(), other.getAnkleLeft_x()) != 0) return false;
        if (Double.compare(this.getAnkleLeft_y(), other.getAnkleLeft_y()) != 0) return false;
        if (Double.compare(this.getAnkleLeft_z(), other.getAnkleLeft_z()) != 0) return false;
        if (Double.compare(this.getFootLeft_x(), other.getFootLeft_x()) != 0) return false;
        if (Double.compare(this.getFootLeft_y(), other.getFootLeft_y()) != 0) return false;
        if (Double.compare(this.getFootLeft_z(), other.getFootLeft_z()) != 0) return false;
        if (Double.compare(this.getHipRight_x(), other.getHipRight_x()) != 0) return false;
        if (Double.compare(this.getHipRight_y(), other.getHipRight_y()) != 0) return false;
        if (Double.compare(this.getHipRight_z(), other.getHipRight_z()) != 0) return false;
        if (Double.compare(this.getKneeRight_x(), other.getKneeRight_x()) != 0) return false;
        if (Double.compare(this.getKneeRight_y(), other.getKneeRight_y()) != 0) return false;
        if (Double.compare(this.getKneeRight_z(), other.getKneeRight_z()) != 0) return false;
        if (Double.compare(this.getAnkleRight_x(), other.getAnkleRight_x()) != 0) return false;
        if (Double.compare(this.getAnkleRight_y(), other.getAnkleRight_y()) != 0) return false;
        if (Double.compare(this.getAnkleRight_z(), other.getAnkleRight_z()) != 0) return false;
        if (Double.compare(this.getFootRight_x(), other.getFootRight_x()) != 0) return false;
        if (Double.compare(this.getFootRight_y(), other.getFootRight_y()) != 0) return false;
        if (Double.compare(this.getFootRight_z(), other.getFootRight_z()) != 0) return false;
        if (Double.compare(this.getSpineShoulder_x(), other.getSpineShoulder_x()) != 0) return false;
        if (Double.compare(this.getSpineShoulder_y(), other.getSpineShoulder_y()) != 0) return false;
        if (Double.compare(this.getSpineShoulder_z(), other.getSpineShoulder_z()) != 0) return false;
        if (Double.compare(this.getHandTipLeft_x(), other.getHandTipLeft_x()) != 0) return false;
        if (Double.compare(this.getHandTipLeft_y(), other.getHandTipLeft_y()) != 0) return false;
        if (Double.compare(this.getHandTipLeft_z(), other.getHandTipLeft_z()) != 0) return false;
        if (Double.compare(this.getThumbLeft_x(), other.getThumbLeft_x()) != 0) return false;
        if (Double.compare(this.getThumbLeft_y(), other.getThumbLeft_y()) != 0) return false;
        if (Double.compare(this.getThumbLeft_z(), other.getThumbLeft_z()) != 0) return false;
        if (Double.compare(this.getHandTipRight_x(), other.getHandTipRight_x()) != 0) return false;
        if (Double.compare(this.getHandTipRight_y(), other.getHandTipRight_y()) != 0) return false;
        if (Double.compare(this.getHandTipRight_z(), other.getHandTipRight_z()) != 0) return false;
        if (Double.compare(this.getThumbRight_x(), other.getThumbRight_x()) != 0) return false;
        if (Double.compare(this.getThumbRight_y(), other.getThumbRight_y()) != 0) return false;
        if (Double.compare(this.getThumbRight_z(), other.getThumbRight_z()) != 0) return false;
        final Object this$timestamp = this.getTimestamp();
        final Object other$timestamp = other.getTimestamp();
        if (this$timestamp == null ? other$timestamp != null : !this$timestamp.equals(other$timestamp)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof KinectDataDto;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $spineBase_x = Double.doubleToLongBits(this.getSpineBase_x());
        result = result * PRIME + (int) ($spineBase_x >>> 32 ^ $spineBase_x);
        final long $spineBase_y = Double.doubleToLongBits(this.getSpineBase_y());
        result = result * PRIME + (int) ($spineBase_y >>> 32 ^ $spineBase_y);
        final long $spineBase_z = Double.doubleToLongBits(this.getSpineBase_z());
        result = result * PRIME + (int) ($spineBase_z >>> 32 ^ $spineBase_z);
        final long $spineMid_x = Double.doubleToLongBits(this.getSpineMid_x());
        result = result * PRIME + (int) ($spineMid_x >>> 32 ^ $spineMid_x);
        final long $spineMid_y = Double.doubleToLongBits(this.getSpineMid_y());
        result = result * PRIME + (int) ($spineMid_y >>> 32 ^ $spineMid_y);
        final long $spineMid_z = Double.doubleToLongBits(this.getSpineMid_z());
        result = result * PRIME + (int) ($spineMid_z >>> 32 ^ $spineMid_z);
        final long $neck_x = Double.doubleToLongBits(this.getNeck_x());
        result = result * PRIME + (int) ($neck_x >>> 32 ^ $neck_x);
        final long $neck_y = Double.doubleToLongBits(this.getNeck_y());
        result = result * PRIME + (int) ($neck_y >>> 32 ^ $neck_y);
        final long $neck_z = Double.doubleToLongBits(this.getNeck_z());
        result = result * PRIME + (int) ($neck_z >>> 32 ^ $neck_z);
        final long $head_x = Double.doubleToLongBits(this.getHead_x());
        result = result * PRIME + (int) ($head_x >>> 32 ^ $head_x);
        final long $head_y = Double.doubleToLongBits(this.getHead_y());
        result = result * PRIME + (int) ($head_y >>> 32 ^ $head_y);
        final long $head_z = Double.doubleToLongBits(this.getHead_z());
        result = result * PRIME + (int) ($head_z >>> 32 ^ $head_z);
        final long $shoulderLeft_x = Double.doubleToLongBits(this.getShoulderLeft_x());
        result = result * PRIME + (int) ($shoulderLeft_x >>> 32 ^ $shoulderLeft_x);
        final long $shoulderLeft_y = Double.doubleToLongBits(this.getShoulderLeft_y());
        result = result * PRIME + (int) ($shoulderLeft_y >>> 32 ^ $shoulderLeft_y);
        final long $shoulderLeft_z = Double.doubleToLongBits(this.getShoulderLeft_z());
        result = result * PRIME + (int) ($shoulderLeft_z >>> 32 ^ $shoulderLeft_z);
        final long $elbowLeft_x = Double.doubleToLongBits(this.getElbowLeft_x());
        result = result * PRIME + (int) ($elbowLeft_x >>> 32 ^ $elbowLeft_x);
        final long $elbowLeft_y = Double.doubleToLongBits(this.getElbowLeft_y());
        result = result * PRIME + (int) ($elbowLeft_y >>> 32 ^ $elbowLeft_y);
        final long $elbowLeft_z = Double.doubleToLongBits(this.getElbowLeft_z());
        result = result * PRIME + (int) ($elbowLeft_z >>> 32 ^ $elbowLeft_z);
        final long $wristLeft_x = Double.doubleToLongBits(this.getWristLeft_x());
        result = result * PRIME + (int) ($wristLeft_x >>> 32 ^ $wristLeft_x);
        final long $wristLeft_y = Double.doubleToLongBits(this.getWristLeft_y());
        result = result * PRIME + (int) ($wristLeft_y >>> 32 ^ $wristLeft_y);
        final long $wristLeft_z = Double.doubleToLongBits(this.getWristLeft_z());
        result = result * PRIME + (int) ($wristLeft_z >>> 32 ^ $wristLeft_z);
        final long $handLeft_x = Double.doubleToLongBits(this.getHandLeft_x());
        result = result * PRIME + (int) ($handLeft_x >>> 32 ^ $handLeft_x);
        final long $handLeft_y = Double.doubleToLongBits(this.getHandLeft_y());
        result = result * PRIME + (int) ($handLeft_y >>> 32 ^ $handLeft_y);
        final long $handLeft_z = Double.doubleToLongBits(this.getHandLeft_z());
        result = result * PRIME + (int) ($handLeft_z >>> 32 ^ $handLeft_z);
        final long $shoulderRight_x = Double.doubleToLongBits(this.getShoulderRight_x());
        result = result * PRIME + (int) ($shoulderRight_x >>> 32 ^ $shoulderRight_x);
        final long $shoulderRight_y = Double.doubleToLongBits(this.getShoulderRight_y());
        result = result * PRIME + (int) ($shoulderRight_y >>> 32 ^ $shoulderRight_y);
        final long $shoulderRight_z = Double.doubleToLongBits(this.getShoulderRight_z());
        result = result * PRIME + (int) ($shoulderRight_z >>> 32 ^ $shoulderRight_z);
        final long $elbowRight_x = Double.doubleToLongBits(this.getElbowRight_x());
        result = result * PRIME + (int) ($elbowRight_x >>> 32 ^ $elbowRight_x);
        final long $elbowRight_y = Double.doubleToLongBits(this.getElbowRight_y());
        result = result * PRIME + (int) ($elbowRight_y >>> 32 ^ $elbowRight_y);
        final long $elbowRight_z = Double.doubleToLongBits(this.getElbowRight_z());
        result = result * PRIME + (int) ($elbowRight_z >>> 32 ^ $elbowRight_z);
        final long $wristRight_x = Double.doubleToLongBits(this.getWristRight_x());
        result = result * PRIME + (int) ($wristRight_x >>> 32 ^ $wristRight_x);
        final long $wristRight_y = Double.doubleToLongBits(this.getWristRight_y());
        result = result * PRIME + (int) ($wristRight_y >>> 32 ^ $wristRight_y);
        final long $wristRight_z = Double.doubleToLongBits(this.getWristRight_z());
        result = result * PRIME + (int) ($wristRight_z >>> 32 ^ $wristRight_z);
        final long $handRight_x = Double.doubleToLongBits(this.getHandRight_x());
        result = result * PRIME + (int) ($handRight_x >>> 32 ^ $handRight_x);
        final long $handRight_y = Double.doubleToLongBits(this.getHandRight_y());
        result = result * PRIME + (int) ($handRight_y >>> 32 ^ $handRight_y);
        final long $handRight_z = Double.doubleToLongBits(this.getHandRight_z());
        result = result * PRIME + (int) ($handRight_z >>> 32 ^ $handRight_z);
        final long $hipLeft_x = Double.doubleToLongBits(this.getHipLeft_x());
        result = result * PRIME + (int) ($hipLeft_x >>> 32 ^ $hipLeft_x);
        final long $hipLeft_y = Double.doubleToLongBits(this.getHipLeft_y());
        result = result * PRIME + (int) ($hipLeft_y >>> 32 ^ $hipLeft_y);
        final long $hipLeft_z = Double.doubleToLongBits(this.getHipLeft_z());
        result = result * PRIME + (int) ($hipLeft_z >>> 32 ^ $hipLeft_z);
        final long $kneeLeft_x = Double.doubleToLongBits(this.getKneeLeft_x());
        result = result * PRIME + (int) ($kneeLeft_x >>> 32 ^ $kneeLeft_x);
        final long $kneeLeft_y = Double.doubleToLongBits(this.getKneeLeft_y());
        result = result * PRIME + (int) ($kneeLeft_y >>> 32 ^ $kneeLeft_y);
        final long $kneeLeft_z = Double.doubleToLongBits(this.getKneeLeft_z());
        result = result * PRIME + (int) ($kneeLeft_z >>> 32 ^ $kneeLeft_z);
        final long $ankleLeft_x = Double.doubleToLongBits(this.getAnkleLeft_x());
        result = result * PRIME + (int) ($ankleLeft_x >>> 32 ^ $ankleLeft_x);
        final long $ankleLeft_y = Double.doubleToLongBits(this.getAnkleLeft_y());
        result = result * PRIME + (int) ($ankleLeft_y >>> 32 ^ $ankleLeft_y);
        final long $ankleLeft_z = Double.doubleToLongBits(this.getAnkleLeft_z());
        result = result * PRIME + (int) ($ankleLeft_z >>> 32 ^ $ankleLeft_z);
        final long $footLeft_x = Double.doubleToLongBits(this.getFootLeft_x());
        result = result * PRIME + (int) ($footLeft_x >>> 32 ^ $footLeft_x);
        final long $footLeft_y = Double.doubleToLongBits(this.getFootLeft_y());
        result = result * PRIME + (int) ($footLeft_y >>> 32 ^ $footLeft_y);
        final long $footLeft_z = Double.doubleToLongBits(this.getFootLeft_z());
        result = result * PRIME + (int) ($footLeft_z >>> 32 ^ $footLeft_z);
        final long $hipRight_x = Double.doubleToLongBits(this.getHipRight_x());
        result = result * PRIME + (int) ($hipRight_x >>> 32 ^ $hipRight_x);
        final long $hipRight_y = Double.doubleToLongBits(this.getHipRight_y());
        result = result * PRIME + (int) ($hipRight_y >>> 32 ^ $hipRight_y);
        final long $hipRight_z = Double.doubleToLongBits(this.getHipRight_z());
        result = result * PRIME + (int) ($hipRight_z >>> 32 ^ $hipRight_z);
        final long $kneeRight_x = Double.doubleToLongBits(this.getKneeRight_x());
        result = result * PRIME + (int) ($kneeRight_x >>> 32 ^ $kneeRight_x);
        final long $kneeRight_y = Double.doubleToLongBits(this.getKneeRight_y());
        result = result * PRIME + (int) ($kneeRight_y >>> 32 ^ $kneeRight_y);
        final long $kneeRight_z = Double.doubleToLongBits(this.getKneeRight_z());
        result = result * PRIME + (int) ($kneeRight_z >>> 32 ^ $kneeRight_z);
        final long $ankleRight_x = Double.doubleToLongBits(this.getAnkleRight_x());
        result = result * PRIME + (int) ($ankleRight_x >>> 32 ^ $ankleRight_x);
        final long $ankleRight_y = Double.doubleToLongBits(this.getAnkleRight_y());
        result = result * PRIME + (int) ($ankleRight_y >>> 32 ^ $ankleRight_y);
        final long $ankleRight_z = Double.doubleToLongBits(this.getAnkleRight_z());
        result = result * PRIME + (int) ($ankleRight_z >>> 32 ^ $ankleRight_z);
        final long $footRight_x = Double.doubleToLongBits(this.getFootRight_x());
        result = result * PRIME + (int) ($footRight_x >>> 32 ^ $footRight_x);
        final long $footRight_y = Double.doubleToLongBits(this.getFootRight_y());
        result = result * PRIME + (int) ($footRight_y >>> 32 ^ $footRight_y);
        final long $footRight_z = Double.doubleToLongBits(this.getFootRight_z());
        result = result * PRIME + (int) ($footRight_z >>> 32 ^ $footRight_z);
        final long $spineShoulder_x = Double.doubleToLongBits(this.getSpineShoulder_x());
        result = result * PRIME + (int) ($spineShoulder_x >>> 32 ^ $spineShoulder_x);
        final long $spineShoulder_y = Double.doubleToLongBits(this.getSpineShoulder_y());
        result = result * PRIME + (int) ($spineShoulder_y >>> 32 ^ $spineShoulder_y);
        final long $spineShoulder_z = Double.doubleToLongBits(this.getSpineShoulder_z());
        result = result * PRIME + (int) ($spineShoulder_z >>> 32 ^ $spineShoulder_z);
        final long $handTipLeft_x = Double.doubleToLongBits(this.getHandTipLeft_x());
        result = result * PRIME + (int) ($handTipLeft_x >>> 32 ^ $handTipLeft_x);
        final long $handTipLeft_y = Double.doubleToLongBits(this.getHandTipLeft_y());
        result = result * PRIME + (int) ($handTipLeft_y >>> 32 ^ $handTipLeft_y);
        final long $handTipLeft_z = Double.doubleToLongBits(this.getHandTipLeft_z());
        result = result * PRIME + (int) ($handTipLeft_z >>> 32 ^ $handTipLeft_z);
        final long $thumbLeft_x = Double.doubleToLongBits(this.getThumbLeft_x());
        result = result * PRIME + (int) ($thumbLeft_x >>> 32 ^ $thumbLeft_x);
        final long $thumbLeft_y = Double.doubleToLongBits(this.getThumbLeft_y());
        result = result * PRIME + (int) ($thumbLeft_y >>> 32 ^ $thumbLeft_y);
        final long $thumbLeft_z = Double.doubleToLongBits(this.getThumbLeft_z());
        result = result * PRIME + (int) ($thumbLeft_z >>> 32 ^ $thumbLeft_z);
        final long $handTipRight_x = Double.doubleToLongBits(this.getHandTipRight_x());
        result = result * PRIME + (int) ($handTipRight_x >>> 32 ^ $handTipRight_x);
        final long $handTipRight_y = Double.doubleToLongBits(this.getHandTipRight_y());
        result = result * PRIME + (int) ($handTipRight_y >>> 32 ^ $handTipRight_y);
        final long $handTipRight_z = Double.doubleToLongBits(this.getHandTipRight_z());
        result = result * PRIME + (int) ($handTipRight_z >>> 32 ^ $handTipRight_z);
        final long $thumbRight_x = Double.doubleToLongBits(this.getThumbRight_x());
        result = result * PRIME + (int) ($thumbRight_x >>> 32 ^ $thumbRight_x);
        final long $thumbRight_y = Double.doubleToLongBits(this.getThumbRight_y());
        result = result * PRIME + (int) ($thumbRight_y >>> 32 ^ $thumbRight_y);
        final long $thumbRight_z = Double.doubleToLongBits(this.getThumbRight_z());
        result = result * PRIME + (int) ($thumbRight_z >>> 32 ^ $thumbRight_z);
        final Object $timestamp = this.getTimestamp();
        result = result * PRIME + ($timestamp == null ? 43 : $timestamp.hashCode());
        return result;
    }

    public static class KinectDataDtoBuilder {
        private double spineBase_x;
        private double spineBase_y;
        private double spineBase_z;
        private double spineMid_x;
        private double spineMid_y;
        private double spineMid_z;
        private double neck_x;
        private double neck_y;
        private double neck_z;
        private double head_x;
        private double head_y;
        private double head_z;
        private double shoulderLeft_x;
        private double shoulderLeft_y;
        private double shoulderLeft_z;
        private double elbowLeft_x;
        private double elbowLeft_y;
        private double elbowLeft_z;
        private double wristLeft_x;
        private double wristLeft_y;
        private double wristLeft_z;
        private double handLeft_x;
        private double handLeft_y;
        private double handLeft_z;
        private double shoulderRight_x;
        private double shoulderRight_y;
        private double shoulderRight_z;
        private double elbowRight_x;
        private double elbowRight_y;
        private double elbowRight_z;
        private double wristRight_x;
        private double wristRight_y;
        private double wristRight_z;
        private double handRight_x;
        private double handRight_y;
        private double handRight_z;
        private double hipLeft_x;
        private double hipLeft_y;
        private double hipLeft_z;
        private double kneeLeft_x;
        private double kneeLeft_y;
        private double kneeLeft_z;
        private double ankleLeft_x;
        private double ankleLeft_y;
        private double ankleLeft_z;
        private double footLeft_x;
        private double footLeft_y;
        private double footLeft_z;
        private double hipRight_x;
        private double hipRight_y;
        private double hipRight_z;
        private double kneeRight_x;
        private double kneeRight_y;
        private double kneeRight_z;
        private double ankleRight_x;
        private double ankleRight_y;
        private double ankleRight_z;
        private double footRight_x;
        private double footRight_y;
        private double footRight_z;
        private double spineShoulder_x;
        private double spineShoulder_y;
        private double spineShoulder_z;
        private double handTipLeft_x;
        private double handTipLeft_y;
        private double handTipLeft_z;
        private double thumbLeft_x;
        private double thumbLeft_y;
        private double thumbLeft_z;
        private double handTipRight_x;
        private double handTipRight_y;
        private double handTipRight_z;
        private double thumbRight_x;
        private double thumbRight_y;
        private double thumbRight_z;
        private Instant timestamp;

        KinectDataDtoBuilder() {
        }

        public KinectDataDto.KinectDataDtoBuilder spineBase_x(double spineBase_x) {
            this.spineBase_x = spineBase_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder spineBase_y(double spineBase_y) {
            this.spineBase_y = spineBase_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder spineBase_z(double spineBase_z) {
            this.spineBase_z = spineBase_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder spineMid_x(double spineMid_x) {
            this.spineMid_x = spineMid_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder spineMid_y(double spineMid_y) {
            this.spineMid_y = spineMid_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder spineMid_z(double spineMid_z) {
            this.spineMid_z = spineMid_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder neck_x(double neck_x) {
            this.neck_x = neck_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder neck_y(double neck_y) {
            this.neck_y = neck_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder neck_z(double neck_z) {
            this.neck_z = neck_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder head_x(double head_x) {
            this.head_x = head_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder head_y(double head_y) {
            this.head_y = head_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder head_z(double head_z) {
            this.head_z = head_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder shoulderLeft_x(double shoulderLeft_x) {
            this.shoulderLeft_x = shoulderLeft_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder shoulderLeft_y(double shoulderLeft_y) {
            this.shoulderLeft_y = shoulderLeft_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder shoulderLeft_z(double shoulderLeft_z) {
            this.shoulderLeft_z = shoulderLeft_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder elbowLeft_x(double elbowLeft_x) {
            this.elbowLeft_x = elbowLeft_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder elbowLeft_y(double elbowLeft_y) {
            this.elbowLeft_y = elbowLeft_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder elbowLeft_z(double elbowLeft_z) {
            this.elbowLeft_z = elbowLeft_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder wristLeft_x(double wristLeft_x) {
            this.wristLeft_x = wristLeft_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder wristLeft_y(double wristLeft_y) {
            this.wristLeft_y = wristLeft_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder wristLeft_z(double wristLeft_z) {
            this.wristLeft_z = wristLeft_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder handLeft_x(double handLeft_x) {
            this.handLeft_x = handLeft_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder handLeft_y(double handLeft_y) {
            this.handLeft_y = handLeft_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder handLeft_z(double handLeft_z) {
            this.handLeft_z = handLeft_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder shoulderRight_x(double shoulderRight_x) {
            this.shoulderRight_x = shoulderRight_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder shoulderRight_y(double shoulderRight_y) {
            this.shoulderRight_y = shoulderRight_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder shoulderRight_z(double shoulderRight_z) {
            this.shoulderRight_z = shoulderRight_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder elbowRight_x(double elbowRight_x) {
            this.elbowRight_x = elbowRight_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder elbowRight_y(double elbowRight_y) {
            this.elbowRight_y = elbowRight_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder elbowRight_z(double elbowRight_z) {
            this.elbowRight_z = elbowRight_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder wristRight_x(double wristRight_x) {
            this.wristRight_x = wristRight_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder wristRight_y(double wristRight_y) {
            this.wristRight_y = wristRight_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder wristRight_z(double wristRight_z) {
            this.wristRight_z = wristRight_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder handRight_x(double handRight_x) {
            this.handRight_x = handRight_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder handRight_y(double handRight_y) {
            this.handRight_y = handRight_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder handRight_z(double handRight_z) {
            this.handRight_z = handRight_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder hipLeft_x(double hipLeft_x) {
            this.hipLeft_x = hipLeft_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder hipLeft_y(double hipLeft_y) {
            this.hipLeft_y = hipLeft_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder hipLeft_z(double hipLeft_z) {
            this.hipLeft_z = hipLeft_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder kneeLeft_x(double kneeLeft_x) {
            this.kneeLeft_x = kneeLeft_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder kneeLeft_y(double kneeLeft_y) {
            this.kneeLeft_y = kneeLeft_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder kneeLeft_z(double kneeLeft_z) {
            this.kneeLeft_z = kneeLeft_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder ankleLeft_x(double ankleLeft_x) {
            this.ankleLeft_x = ankleLeft_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder ankleLeft_y(double ankleLeft_y) {
            this.ankleLeft_y = ankleLeft_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder ankleLeft_z(double ankleLeft_z) {
            this.ankleLeft_z = ankleLeft_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder footLeft_x(double footLeft_x) {
            this.footLeft_x = footLeft_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder footLeft_y(double footLeft_y) {
            this.footLeft_y = footLeft_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder footLeft_z(double footLeft_z) {
            this.footLeft_z = footLeft_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder hipRight_x(double hipRight_x) {
            this.hipRight_x = hipRight_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder hipRight_y(double hipRight_y) {
            this.hipRight_y = hipRight_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder hipRight_z(double hipRight_z) {
            this.hipRight_z = hipRight_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder kneeRight_x(double kneeRight_x) {
            this.kneeRight_x = kneeRight_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder kneeRight_y(double kneeRight_y) {
            this.kneeRight_y = kneeRight_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder kneeRight_z(double kneeRight_z) {
            this.kneeRight_z = kneeRight_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder ankleRight_x(double ankleRight_x) {
            this.ankleRight_x = ankleRight_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder ankleRight_y(double ankleRight_y) {
            this.ankleRight_y = ankleRight_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder ankleRight_z(double ankleRight_z) {
            this.ankleRight_z = ankleRight_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder footRight_x(double footRight_x) {
            this.footRight_x = footRight_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder footRight_y(double footRight_y) {
            this.footRight_y = footRight_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder footRight_z(double footRight_z) {
            this.footRight_z = footRight_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder spineShoulder_x(double spineShoulder_x) {
            this.spineShoulder_x = spineShoulder_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder spineShoulder_y(double spineShoulder_y) {
            this.spineShoulder_y = spineShoulder_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder spineShoulder_z(double spineShoulder_z) {
            this.spineShoulder_z = spineShoulder_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder handTipLeft_x(double handTipLeft_x) {
            this.handTipLeft_x = handTipLeft_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder handTipLeft_y(double handTipLeft_y) {
            this.handTipLeft_y = handTipLeft_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder handTipLeft_z(double handTipLeft_z) {
            this.handTipLeft_z = handTipLeft_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder thumbLeft_x(double thumbLeft_x) {
            this.thumbLeft_x = thumbLeft_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder thumbLeft_y(double thumbLeft_y) {
            this.thumbLeft_y = thumbLeft_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder thumbLeft_z(double thumbLeft_z) {
            this.thumbLeft_z = thumbLeft_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder handTipRight_x(double handTipRight_x) {
            this.handTipRight_x = handTipRight_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder handTipRight_y(double handTipRight_y) {
            this.handTipRight_y = handTipRight_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder handTipRight_z(double handTipRight_z) {
            this.handTipRight_z = handTipRight_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder thumbRight_x(double thumbRight_x) {
            this.thumbRight_x = thumbRight_x;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder thumbRight_y(double thumbRight_y) {
            this.thumbRight_y = thumbRight_y;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder thumbRight_z(double thumbRight_z) {
            this.thumbRight_z = thumbRight_z;
            return this;
        }

        public KinectDataDto.KinectDataDtoBuilder timestamp(Instant timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public KinectDataDto build() {
            return new KinectDataDto(spineBase_x, spineBase_y, spineBase_z, spineMid_x, spineMid_y, spineMid_z, neck_x, neck_y, neck_z, head_x, head_y, head_z, shoulderLeft_x, shoulderLeft_y, shoulderLeft_z, elbowLeft_x, elbowLeft_y, elbowLeft_z, wristLeft_x, wristLeft_y, wristLeft_z, handLeft_x, handLeft_y, handLeft_z, shoulderRight_x, shoulderRight_y, shoulderRight_z, elbowRight_x, elbowRight_y, elbowRight_z, wristRight_x, wristRight_y, wristRight_z, handRight_x, handRight_y, handRight_z, hipLeft_x, hipLeft_y, hipLeft_z, kneeLeft_x, kneeLeft_y, kneeLeft_z, ankleLeft_x, ankleLeft_y, ankleLeft_z, footLeft_x, footLeft_y, footLeft_z, hipRight_x, hipRight_y, hipRight_z, kneeRight_x, kneeRight_y, kneeRight_z, ankleRight_x, ankleRight_y, ankleRight_z, footRight_x, footRight_y, footRight_z, spineShoulder_x, spineShoulder_y, spineShoulder_z, handTipLeft_x, handTipLeft_y, handTipLeft_z, thumbLeft_x, thumbLeft_y, thumbLeft_z, handTipRight_x, handTipRight_y, handTipRight_z, thumbRight_x, thumbRight_y, thumbRight_z, timestamp);
        }

        public String toString() {
            return "KinectDataDto.KinectDataDtoBuilder(spineBase_x=" + this.spineBase_x + ", spineBase_y=" + this.spineBase_y + ", spineBase_z=" + this.spineBase_z + ", spineMid_x=" + this.spineMid_x + ", spineMid_y=" + this.spineMid_y + ", spineMid_z=" + this.spineMid_z + ", neck_x=" + this.neck_x + ", neck_y=" + this.neck_y + ", neck_z=" + this.neck_z + ", head_x=" + this.head_x + ", head_y=" + this.head_y + ", head_z=" + this.head_z + ", shoulderLeft_x=" + this.shoulderLeft_x + ", shoulderLeft_y=" + this.shoulderLeft_y + ", shoulderLeft_z=" + this.shoulderLeft_z + ", elbowLeft_x=" + this.elbowLeft_x + ", elbowLeft_y=" + this.elbowLeft_y + ", elbowLeft_z=" + this.elbowLeft_z + ", wristLeft_x=" + this.wristLeft_x + ", wristLeft_y=" + this.wristLeft_y + ", wristLeft_z=" + this.wristLeft_z + ", handLeft_x=" + this.handLeft_x + ", handLeft_y=" + this.handLeft_y + ", handLeft_z=" + this.handLeft_z + ", shoulderRight_x=" + this.shoulderRight_x + ", shoulderRight_y=" + this.shoulderRight_y + ", shoulderRight_z=" + this.shoulderRight_z + ", elbowRight_x=" + this.elbowRight_x + ", elbowRight_y=" + this.elbowRight_y + ", elbowRight_z=" + this.elbowRight_z + ", wristRight_x=" + this.wristRight_x + ", wristRight_y=" + this.wristRight_y + ", wristRight_z=" + this.wristRight_z + ", handRight_x=" + this.handRight_x + ", handRight_y=" + this.handRight_y + ", handRight_z=" + this.handRight_z + ", hipLeft_x=" + this.hipLeft_x + ", hipLeft_y=" + this.hipLeft_y + ", hipLeft_z=" + this.hipLeft_z + ", kneeLeft_x=" + this.kneeLeft_x + ", kneeLeft_y=" + this.kneeLeft_y + ", kneeLeft_z=" + this.kneeLeft_z + ", ankleLeft_x=" + this.ankleLeft_x + ", ankleLeft_y=" + this.ankleLeft_y + ", ankleLeft_z=" + this.ankleLeft_z + ", footLeft_x=" + this.footLeft_x + ", footLeft_y=" + this.footLeft_y + ", footLeft_z=" + this.footLeft_z + ", hipRight_x=" + this.hipRight_x + ", hipRight_y=" + this.hipRight_y + ", hipRight_z=" + this.hipRight_z + ", kneeRight_x=" + this.kneeRight_x + ", kneeRight_y=" + this.kneeRight_y + ", kneeRight_z=" + this.kneeRight_z + ", ankleRight_x=" + this.ankleRight_x + ", ankleRight_y=" + this.ankleRight_y + ", ankleRight_z=" + this.ankleRight_z + ", footRight_x=" + this.footRight_x + ", footRight_y=" + this.footRight_y + ", footRight_z=" + this.footRight_z + ", spineShoulder_x=" + this.spineShoulder_x + ", spineShoulder_y=" + this.spineShoulder_y + ", spineShoulder_z=" + this.spineShoulder_z + ", handTipLeft_x=" + this.handTipLeft_x + ", handTipLeft_y=" + this.handTipLeft_y + ", handTipLeft_z=" + this.handTipLeft_z + ", thumbLeft_x=" + this.thumbLeft_x + ", thumbLeft_y=" + this.thumbLeft_y + ", thumbLeft_z=" + this.thumbLeft_z + ", handTipRight_x=" + this.handTipRight_x + ", handTipRight_y=" + this.handTipRight_y + ", handTipRight_z=" + this.handTipRight_z + ", thumbRight_x=" + this.thumbRight_x + ", thumbRight_y=" + this.thumbRight_y + ", thumbRight_z=" + this.thumbRight_z + ", timestamp=" + this.timestamp + ")";
        }
    }
}
