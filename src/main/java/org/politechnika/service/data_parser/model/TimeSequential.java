package org.politechnika.service.data_parser.model;

import java.time.Instant;

public interface TimeSequential {

    Instant getTime();
}
