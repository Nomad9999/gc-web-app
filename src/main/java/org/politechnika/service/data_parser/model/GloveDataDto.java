package org.politechnika.service.data_parser.model;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import org.politechnika.domain.GloveDataEntity;
import org.politechnika.service.data_parser.converter.DoubleWithCommaConverter;
import org.politechnika.service.data_parser.converter.TimestampToInstantConverter;

import java.time.Instant;

public class GloveDataDto implements DataDto {

    //Hand|SensorNumber|Scale|Raw|Low|Up|TimeStamp
    //left|0|0,4166667|2847|2832|2868|2018-06-10-11-48-34-7705

    @CsvBindByName(column = "Hand")
    private String hand;

    @CsvBindByName(column = "SensorNumber")
    private int sensorNumber;

    @CsvCustomBindByName(column = "Scale", converter = DoubleWithCommaConverter.class)
    private double scale; //scale = (raw-low)/(up-low)

    @CsvBindByName(column = "Raw")
    private int raw;

    @CsvBindByName(column = "Low")
    private int low;

    @CsvBindByName(column = "Up")
    private int up;

    @CsvCustomBindByName(column = "TimeStamp", converter = TimestampToInstantConverter.class)
    private Instant timestamp;

    public GloveDataEntity toEntity() {
        final GloveDataEntity gloveDataEntity = new GloveDataEntity();
        gloveDataEntity.setHand(hand);
        gloveDataEntity.setSensorNumber(sensorNumber);
        gloveDataEntity.setScale(scale);
        gloveDataEntity.setRaw(raw);
        gloveDataEntity.setLow(low);
        gloveDataEntity.setUp(up);
        gloveDataEntity.setTimestamp(timestamp);
        return gloveDataEntity;
    }

    public GloveDataDto(String hand, int sensorNumber, double scale, int raw, int low, int up, Instant timestamp) {
        this.hand = hand;
        this.sensorNumber = sensorNumber;
        this.scale = scale;
        this.raw = raw;
        this.low = low;
        this.up = up;
        this.timestamp = timestamp;
    }

    public GloveDataDto() {
    }

    public static GloveDataDtoBuilder builder() {
        return new GloveDataDtoBuilder();
    }

    public String getHand() {
        return this.hand;
    }

    public int getSensorNumber() {
        return this.sensorNumber;
    }

    public double getScale() {
        return this.scale;
    }

    public int getRaw() {
        return this.raw;
    }

    public int getLow() {
        return this.low;
    }

    public int getUp() {
        return this.up;
    }

    public Instant getTimestamp() {
        return this.timestamp;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof GloveDataDto)) return false;
        final GloveDataDto other = (GloveDataDto) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$hand = this.getHand();
        final Object other$hand = other.getHand();
        if (this$hand == null ? other$hand != null : !this$hand.equals(other$hand)) return false;
        if (this.getSensorNumber() != other.getSensorNumber()) return false;
        if (Double.compare(this.getScale(), other.getScale()) != 0) return false;
        if (this.getRaw() != other.getRaw()) return false;
        if (this.getLow() != other.getLow()) return false;
        if (this.getUp() != other.getUp()) return false;
        final Object this$timestamp = this.getTimestamp();
        final Object other$timestamp = other.getTimestamp();
        if (this$timestamp == null ? other$timestamp != null : !this$timestamp.equals(other$timestamp)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GloveDataDto;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $hand = this.getHand();
        result = result * PRIME + ($hand == null ? 43 : $hand.hashCode());
        result = result * PRIME + this.getSensorNumber();
        final long $scale = Double.doubleToLongBits(this.getScale());
        result = result * PRIME + (int) ($scale >>> 32 ^ $scale);
        result = result * PRIME + this.getRaw();
        result = result * PRIME + this.getLow();
        result = result * PRIME + this.getUp();
        final Object $timestamp = this.getTimestamp();
        result = result * PRIME + ($timestamp == null ? 43 : $timestamp.hashCode());
        return result;
    }

    public static class GloveDataDtoBuilder {
        private String hand;
        private int sensorNumber;
        private double scale;
        private int raw;
        private int low;
        private int up;
        private Instant timestamp;

        GloveDataDtoBuilder() {
        }

        public GloveDataDto.GloveDataDtoBuilder hand(String hand) {
            this.hand = hand;
            return this;
        }

        public GloveDataDto.GloveDataDtoBuilder sensorNumber(int sensorNumber) {
            this.sensorNumber = sensorNumber;
            return this;
        }

        public GloveDataDto.GloveDataDtoBuilder scale(double scale) {
            this.scale = scale;
            return this;
        }

        public GloveDataDto.GloveDataDtoBuilder raw(int raw) {
            this.raw = raw;
            return this;
        }

        public GloveDataDto.GloveDataDtoBuilder low(int low) {
            this.low = low;
            return this;
        }

        public GloveDataDto.GloveDataDtoBuilder up(int up) {
            this.up = up;
            return this;
        }

        public GloveDataDto.GloveDataDtoBuilder timestamp(Instant timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public GloveDataDto build() {
            return new GloveDataDto(hand, sensorNumber, scale, raw, low, up, timestamp);
        }

        public String toString() {
            return "GloveDataDto.GloveDataDtoBuilder(hand=" + this.hand + ", sensorNumber=" + this.sensorNumber + ", scale=" + this.scale + ", raw=" + this.raw + ", low=" + this.low + ", up=" + this.up + ", timestamp=" + this.timestamp + ")";
        }
    }
}
