package org.politechnika.service.data_parser.model;

import com.opencsv.bean.CsvBindByName;

public final class OneHandGloveRawData implements DataDto {

    @CsvBindByName(column = "kciuk")
    private final double thumb;

    @CsvBindByName(column = "wskazujacy")
    private final double index;

    @CsvBindByName(column = "srodkowy")
    private final double middle;

    @CsvBindByName(column = "serdeczny")
    private final double ring;

    @CsvBindByName(column = "maly")
    private final double little;

    public OneHandGloveRawData(double thumb, double index, double middle, double ring, double little) {
        this.thumb = thumb;
        this.index = index;
        this.middle = middle;
        this.ring = ring;
        this.little = little;
    }

    public double getThumb() {
        return this.thumb;
    }

    public double getIndex() {
        return this.index;
    }

    public double getMiddle() {
        return this.middle;
    }

    public double getRing() {
        return this.ring;
    }

    public double getLittle() {
        return this.little;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof OneHandGloveRawData)) return false;
        final OneHandGloveRawData other = (OneHandGloveRawData) o;
        if (Double.compare(this.getThumb(), other.getThumb()) != 0) return false;
        if (Double.compare(this.getIndex(), other.getIndex()) != 0) return false;
        if (Double.compare(this.getMiddle(), other.getMiddle()) != 0) return false;
        if (Double.compare(this.getRing(), other.getRing()) != 0) return false;
        if (Double.compare(this.getLittle(), other.getLittle()) != 0) return false;
        return true;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $thumb = Double.doubleToLongBits(this.getThumb());
        result = result * PRIME + (int) ($thumb >>> 32 ^ $thumb);
        final long $index = Double.doubleToLongBits(this.getIndex());
        result = result * PRIME + (int) ($index >>> 32 ^ $index);
        final long $middle = Double.doubleToLongBits(this.getMiddle());
        result = result * PRIME + (int) ($middle >>> 32 ^ $middle);
        final long $ring = Double.doubleToLongBits(this.getRing());
        result = result * PRIME + (int) ($ring >>> 32 ^ $ring);
        final long $little = Double.doubleToLongBits(this.getLittle());
        result = result * PRIME + (int) ($little >>> 32 ^ $little);
        return result;
    }

    public String toString() {
        return "OneHandGloveRawData(thumb=" + this.getThumb() + ", index=" + this.getIndex() + ", middle=" + this.getMiddle() + ", ring=" + this.getRing() + ", little=" + this.getLittle() + ")";
    }
}
