package org.politechnika.service.data_parser.model;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import org.politechnika.domain.PulsometerDataEntity;
import org.politechnika.service.data_parser.converter.TimestampToInstantConverter;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class TimestampPulsometerData implements DataDto {

    @CsvBindByName(column = "HeartRate")
    private Integer value;

    @CsvCustomBindByName(column = "TimeStamp", converter = TimestampToInstantConverter.class)
    private Instant timestamp;

    public TimestampPulsometerData(Integer value, Instant timestamp) {
        this.value = value;
        this.timestamp = timestamp;
    }

    public TimestampPulsometerData() {
    }

    public PulsometerDataDto toPulsometerDto() {
        LocalDateTime ltd = LocalDateTime.ofInstant(timestamp, ZoneOffset.UTC);
        return new PulsometerDataDto(ltd.toLocalDate(), ltd.toLocalTime(), value);
    }

    public PulsometerDataEntity toEntity() {
        final PulsometerDataEntity pulsometerDataEntity = new PulsometerDataEntity();
        if (value != null)
            pulsometerDataEntity.setValue(value);
        else
            pulsometerDataEntity.setValue(0);

        if (timestamp != null)
            pulsometerDataEntity.setTimestamp(timestamp);
        else
            pulsometerDataEntity.setTimestamp(Instant.EPOCH);

        return pulsometerDataEntity;
    }

    public Integer getValue() {
        return this.value;
    }

    public Instant getTimestamp() {
        return this.timestamp;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof TimestampPulsometerData)) return false;
        final TimestampPulsometerData other = (TimestampPulsometerData) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$value = this.getValue();
        final Object other$value = other.getValue();
        if (this$value == null ? other$value != null : !this$value.equals(other$value)) return false;
        final Object this$timestamp = this.getTimestamp();
        final Object other$timestamp = other.getTimestamp();
        if (this$timestamp == null ? other$timestamp != null : !this$timestamp.equals(other$timestamp)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof TimestampPulsometerData;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $value = this.getValue();
        result = result * PRIME + ($value == null ? 43 : $value.hashCode());
        final Object $timestamp = this.getTimestamp();
        result = result * PRIME + ($timestamp == null ? 43 : $timestamp.hashCode());
        return result;
    }
}
