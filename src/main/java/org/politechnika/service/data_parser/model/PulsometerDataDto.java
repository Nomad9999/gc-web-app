package org.politechnika.service.data_parser.model;

import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvCustomBindByPosition;
import org.politechnika.domain.PulsometerDataEntity;
import org.politechnika.service.data_parser.converter.PulsometerDateConverter;
import org.politechnika.service.data_parser.converter.PulsometerTimeConverter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;

public class PulsometerDataDto implements DataDto {

    @CsvCustomBindByPosition(position = 0, converter = PulsometerDateConverter.class)
    private LocalDate date;

    @CsvCustomBindByPosition(position = 1, converter = PulsometerTimeConverter.class)
    private LocalTime time;

    @CsvBindByPosition(position = 3)
    private Integer value;

    public PulsometerDataDto(LocalDate date, LocalTime time, Integer value) {
        this.date = date;
        this.time = time;
        this.value = value;
    }

    public PulsometerDataDto() {
    }

    public PulsometerDataEntity toEntity() {
        final PulsometerDataEntity pulsometerDataEntity = new PulsometerDataEntity();
        pulsometerDataEntity.setValue(value);
        pulsometerDataEntity.setTimestamp(LocalDateTime.of(date, time).toInstant(ZoneOffset.UTC));

        return pulsometerDataEntity;
    }

    public LocalDate getDate() {
        return this.date;
    }

    public LocalTime getTime() {
        return this.time;
    }

    public Integer getValue() {
        return this.value;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PulsometerDataDto)) return false;
        final PulsometerDataDto other = (PulsometerDataDto) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$date = this.getDate();
        final Object other$date = other.getDate();
        if (this$date == null ? other$date != null : !this$date.equals(other$date)) return false;
        final Object this$time = this.getTime();
        final Object other$time = other.getTime();
        if (this$time == null ? other$time != null : !this$time.equals(other$time)) return false;
        final Object this$value = this.getValue();
        final Object other$value = other.getValue();
        if (this$value == null ? other$value != null : !this$value.equals(other$value)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PulsometerDataDto;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $date = this.getDate();
        result = result * PRIME + ($date == null ? 43 : $date.hashCode());
        final Object $time = this.getTime();
        result = result * PRIME + ($time == null ? 43 : $time.hashCode());
        final Object $value = this.getValue();
        result = result * PRIME + ($value == null ? 43 : $value.hashCode());
        return result;
    }
}
