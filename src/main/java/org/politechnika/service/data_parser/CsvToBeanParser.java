package org.politechnika.service.data_parser;

import com.opencsv.bean.CsvToBeanBuilder;
import org.politechnika.service.data_parser.strategy.ParsingStrategy;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

@Component
public class CsvToBeanParser {

    public <T> List<T> parseToBean(MultipartFile file, ParsingStrategy<T> parsingStrategy)
        throws IOException {
        CsvToBeanBuilder<T> builder = new CsvToBeanBuilder<>(new BufferedReader(new InputStreamReader(file.getInputStream())));
        builder.withSeparator(parsingStrategy.getReadSeparator());
        builder.withType(parsingStrategy.getBeanClass());

        return builder.build().parse();
    }
}
