package org.politechnika.service.data_parser.strategy;

import org.politechnika.service.data_parser.model.PulsometerDataDto;

public class TxtPulsometerParsingStrategy implements ParsingStrategy<PulsometerDataDto> {

    @Override
    public Class<PulsometerDataDto> getBeanClass() {
        return PulsometerDataDto.class;
    }

    @Override
    public char getReadSeparator() {
        return ' ';
    }

    @Override
    public char getWriteSeparator() {
        return ';';
    }

    @Override
    public boolean supportsFileExtension(String ext) {
        return "txt".equals(ext);
    }
}
