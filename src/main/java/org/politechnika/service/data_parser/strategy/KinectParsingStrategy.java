package org.politechnika.service.data_parser.strategy;

import org.politechnika.service.data_parser.model.KinectDataDto;

public class KinectParsingStrategy implements ParsingStrategy<KinectDataDto> {

    @Override
    public Class<KinectDataDto> getBeanClass() {
        return KinectDataDto.class;
    }

    @Override
    public char getReadSeparator() {
        return '|';
    }

    @Override
    public char getWriteSeparator() {
        return ';';
    }

    @Override
    public boolean supportsFileExtension(String ext) {
        return "csv".equals(ext);
    }
}
