package org.politechnika.service.data_parser.strategy;

import org.politechnika.service.data_parser.model.TimestampPulsometerData;

public class CsvPulsometerParsingStrategy implements ParsingStrategy<TimestampPulsometerData> {

    @Override
    public Class<TimestampPulsometerData> getBeanClass() {
        return TimestampPulsometerData.class;
    }

    @Override
    public char getReadSeparator() {
        return '|';
    }

    @Override
    public char getWriteSeparator() {
        return ';';
    }

    @Override
    public boolean supportsFileExtension(String ext) {
        return "csv".equals(ext);
    }
}
