package org.politechnika.service.data_parser.strategy;

import org.politechnika.service.data_parser.model.GloveDataDto;

public class GloveParsingStrategy implements ParsingStrategy<GloveDataDto> {

    @Override
    public Class<GloveDataDto> getBeanClass() {
        return GloveDataDto.class;
    }

    @Override
    public char getReadSeparator() {
        return '|';
    }

    @Override
    public char getWriteSeparator() {
        return ';';
    }

    @Override
    public boolean supportsFileExtension(String ext) {
        return "csv".equals(ext);
    }
}
