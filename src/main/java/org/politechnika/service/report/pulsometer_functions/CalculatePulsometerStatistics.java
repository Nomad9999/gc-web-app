package org.politechnika.service.report.pulsometer_functions;

import org.politechnika.domain.PulsometerDataEntity;
import org.politechnika.service.analysis.StandardStatisticsAnalyzerImpl;
import org.politechnika.service.report.model.pulsometer.PulsometerStatistics;
import org.slf4j.Logger;

import java.util.List;
import java.util.function.Function;

public class CalculatePulsometerStatistics implements Function<List<PulsometerDataEntity>, PulsometerStatistics> {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(CalculatePulsometerStatistics.class);

    @Override
    public PulsometerStatistics apply(List<PulsometerDataEntity> pulsometerDataDtos) {
        log.debug("Calculating pulsometer statistics");

        StandardStatisticsAnalyzerImpl analyzer = new StandardStatisticsAnalyzerImpl();
        double average = analyzer.getAverage(pulsometerDataDtos, PulsometerDataEntity::getValue);
        double kurtosis = analyzer.getKurtosis(pulsometerDataDtos, PulsometerDataEntity::getValue);
        double skewness = analyzer.getSkewness(pulsometerDataDtos, PulsometerDataEntity::getValue);
        double deviation = analyzer.getStandardDeviation(pulsometerDataDtos, PulsometerDataEntity::getValue);
        double variance = analyzer.getVariance(pulsometerDataDtos, PulsometerDataEntity::getValue);

        return new PulsometerStatistics(average, deviation, variance, skewness, kurtosis);
    }
}
