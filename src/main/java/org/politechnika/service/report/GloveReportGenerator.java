package org.politechnika.service.report;


import org.politechnika.service.report.glove_functions.*;
import org.slf4j.Logger;

import static org.politechnika.commons.Constants.LEFT_HAND;
import static org.politechnika.commons.Constants.RIGHT_HAND;

public class GloveReportGenerator  {

//    private static final Logger log = org.slf4j.LoggerFactory.getLogger(GloveReportGenerator.class);
//
//    @Override
//    public void generate(AbstractDataFile dataFile) {
//        GenerateGloveReport generator = GenerateGloveReport.builder()
//                .fromFile(dataFile)
//                .parseData(new ParseToBeans())
//                .partitionRawData(new PartitionDataByHand())
//                .doOnLeftHand(new CalculateGloveStatistics(LEFT_HAND)
//                        .compose(new CreateTimeSegmentedLeftHandRawDataChart())
//                        .compose(new CreateOneHandRawDataCsv(LEFT_HAND))
//                        .compose(new StoreGloveValues(EntryType.LEFT_HAND_VALUES))
//                        .compose(new CalculateAndCachePearsonCorrelations(LEFT_HAND))
//                        .andThen(new CacheStatistics()))
//                .doOnRightHand(new CalculateGloveStatistics(RIGHT_HAND)
//                        .compose(new CreateTimeSegmentedRightHandRawDataChart())
//                        .compose(new CreateOneHandRawDataCsv(RIGHT_HAND))
//                        .compose(new StoreGloveValues(EntryType.RIGHT_HAND_VALUES))
//                        .compose(new CalculateAndCachePearsonCorrelations(RIGHT_HAND))
//                        .andThen(new CacheStatistics()))
//                .doOnLeftHandWithTimeInterval(new CalculateTimeIntervalStatistics(LEFT_HAND)
//                        .andThen(new CreateTimeSegmentedAverageChart())
//                        .andThen(new CreateTimeSegmentedVarianceChart())
//                        .andThen(new CreateTimeSegmentedStandardDeviationChart())
//                        .andThen(new CreateTimeSegmentedSkewnessChart())
//                        .andThen(new CreateTimeSegmentedKurtosisChart())
//                        .andThen(new CreateAverageAndVarianceChart()))
//                .doOnRightHandWithTimeInterval(new CalculateTimeIntervalStatistics(RIGHT_HAND)
//                        .andThen(new CreateTimeSegmentedAverageChart())
//                        .andThen(new CreateTimeSegmentedVarianceChart())
//                        .andThen(new CreateTimeSegmentedStandardDeviationChart())
//                        .andThen(new CreateTimeSegmentedSkewnessChart())
//                        .andThen(new CreateTimeSegmentedKurtosisChart())
//                        .andThen(new CreateAverageAndVarianceChart()))
//                .build();
//
//        log.debug("Generating glove report");
//        generator.generate();
//        log.debug("Glove report was generated");
//    }
//
//    @Override
//    public boolean supports(String fileType) {
//        return Constants.GLOVE.equals(fileType);
//    }
}
