package org.politechnika.service.report.glove_functions;

import org.politechnika.commons.Constants;
import org.politechnika.domain.GloveDataEntity;
import org.politechnika.service.components.ReportBytesResource;
import org.politechnika.service.data_parser.BeanToCsvParser;
import org.politechnika.service.data_parser.model.DataDto;
import org.politechnika.service.data_parser.model.OneHandGloveRawData;
import org.politechnika.service.report.model.glove.Finger;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.UnaryOperator;

public class CreateOneHandRawDataCsv implements Function<Map<Finger, List<GloveDataEntity>>, ReportBytesResource> {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(CreateOneHandRawDataCsv.class);
    private String handName;

    public CreateOneHandRawDataCsv(String handName) {
        this.handName = handName;
    }

    @Override
    public ReportBytesResource apply(Map<Finger, List<GloveDataEntity>> rawLeftHandDataByFinger) {
        log.debug(String.format("Creating %s hand raw data csv", handName));
        DoubleArrayTimeSeries arrayTimeSeries = new AlignGloveSeries().apply(rawLeftHandDataByFinger);

        double[] thumbFingerRawData = arrayTimeSeries.getSeries(Constants.THUMB);
        double[] indexFingerRawData = arrayTimeSeries.getSeries(Constants.INDEX);
        double[] middleFingerRawData = arrayTimeSeries.getSeries(Constants.MIDDLE);
        double[] ringFingerRawData = arrayTimeSeries.getSeries(Constants.RING);
        double[] littleFingerRawData = arrayTimeSeries.getSeries(Constants.LITTLE);

        ArrayList<OneHandGloveRawData> res = new ArrayList<>();
        for (int i = 0; i < thumbFingerRawData.length; i++) {
            res.add(new OneHandGloveRawData(
                    thumbFingerRawData[i],
                    indexFingerRawData[i],
                    middleFingerRawData[i],
                    ringFingerRawData[i],
                    littleFingerRawData[i]
            ));
        }

        return tryWriteDataToCsv(res);
    }

    private ReportBytesResource tryWriteDataToCsv(ArrayList<OneHandGloveRawData> res) {
        String resultString = new BeanToCsvParser().parseToCsvString(res);
        return new ReportBytesResource(resultString);
    }

}
