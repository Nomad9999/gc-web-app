package org.politechnika.service.report.glove_functions;

import org.politechnika.commons.Constants;
import org.politechnika.domain.GloveDataEntity;
import org.politechnika.service.report.model.glove.Finger;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.Collections.emptyList;
import static org.politechnika.service.report.model.glove.Finger.*;

public class AlignGloveSeries implements Function<Map<Finger, List<GloveDataEntity>>, DoubleArrayTimeSeries> {

    @Override
    public DoubleArrayTimeSeries apply(Map<Finger, List<GloveDataEntity>> fingerListMap) {
        double[] thumbFingerRawData = fingerListMap.getOrDefault(THUMB, emptyList()).stream().mapToDouble(GloveDataEntity::getRaw).toArray();
        double[] indexFingerRawData = fingerListMap.getOrDefault(INDEX, emptyList()).stream().mapToDouble(GloveDataEntity::getRaw).toArray();
        double[] middleFingerRawData = fingerListMap.getOrDefault(MIDDLE, emptyList()).stream().mapToDouble(GloveDataEntity::getRaw).toArray();
        double[] ringFingerRawData = fingerListMap.getOrDefault(RING, emptyList()).stream().mapToDouble(GloveDataEntity::getRaw).toArray();
        double[] littleFingerRawData = fingerListMap.getOrDefault(LITTLE, emptyList()).stream().mapToDouble(GloveDataEntity::getRaw).toArray();

        DoubleArrayTimeSeries arrayTimeSeries = new DoubleArrayTimeSeries();
        arrayTimeSeries.addSeries(Constants.THUMB, thumbFingerRawData);
        arrayTimeSeries.addSeries(Constants.INDEX, indexFingerRawData);
        arrayTimeSeries.addSeries(Constants.MIDDLE, middleFingerRawData);
        arrayTimeSeries.addSeries(Constants.RING, ringFingerRawData);
        arrayTimeSeries.addSeries(Constants.LITTLE, littleFingerRawData);
        arrayTimeSeries.alignArrays(DoubleArrayTimeSeries.AligningMode.LAST_VALUE);

        return arrayTimeSeries;
    }
}
