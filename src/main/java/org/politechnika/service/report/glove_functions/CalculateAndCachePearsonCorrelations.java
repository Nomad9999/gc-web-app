package org.politechnika.service.report.glove_functions;

import org.politechnika.commons.Separators;
import org.politechnika.domain.GloveDataEntity;
import org.politechnika.service.analysis.CorrelationAnalyzer;
import org.politechnika.service.analysis.StandardCorrelationAnalyzer;
import org.politechnika.service.report.model.glove.Finger;
import org.slf4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.UnaryOperator;

public class CalculateAndCachePearsonCorrelations implements Function<Map<Finger, List<GloveDataEntity>>, String> {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(CalculateAndCachePearsonCorrelations.class);
    private String handName;

    public CalculateAndCachePearsonCorrelations(String handName) {
        this.handName = handName;
    }

    @Override
    public String apply(Map<Finger, List<GloveDataEntity>> rawHandDataByFinger) {
        log.debug(String.format("Calculating %s hand Pearson's correlation matrix", handName));
        DoubleArrayTimeSeries arrayTimeSeries = new AlignGloveSeries().apply(rawHandDataByFinger);
        double[][] pearsonTable = calculateCorrTable(arrayTimeSeries);
        StringBuilder sb = createStringRepresentation(pearsonTable);

//        if ("left".equals(handName))
//            LoadingStringCache.put(EntryType.LEFT_HAND_CORRELATIONS, sb.toString());
//        else
//            LoadingStringCache.put(EntryType.RIGHT_HAND_CORRELATIONS, sb.toString());

        return sb.toString();
    }

    private StringBuilder createStringRepresentation(double[][] pearsonTable) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < pearsonTable.length; i++) {
            for (int j = 0; j < pearsonTable[i].length; j++) {
                sb.append(pearsonTable[i][j]).append(Separators.TAB).append(Separators.TAB);
            }
            sb.append(Separators.NEWLINE);
        }
        return sb;
    }

    private double[][] calculateCorrTable(DoubleArrayTimeSeries arrayTimeSeries) {
        String[] fingers = arrayTimeSeries.getInsertOrderKeys();
        CorrelationAnalyzer corrAnalyzer = new StandardCorrelationAnalyzer();
        double[][] pearsonTable = new double[5][5];
        for (int i = 0; i < pearsonTable.length; i++) {
            for (int j = 0; j < pearsonTable[i].length; j++) {
                pearsonTable[i][j] = corrAnalyzer.getPearsonCorrelation(
                        arrayTimeSeries.getSeries(fingers[i]),
                        arrayTimeSeries.getSeries(fingers[j])
                );
            }
        }
        return pearsonTable;
    }


}
