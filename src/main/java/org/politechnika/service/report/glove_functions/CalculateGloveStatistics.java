package org.politechnika.service.report.glove_functions;

import org.politechnika.domain.GloveDataEntity;
import org.politechnika.service.analysis.StandardStatisticsAnalyzerImpl;
import org.politechnika.service.report.model.glove.Finger;
import org.politechnika.service.report.model.glove.HandStatistics;
import org.slf4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.lang.String.format;

public class CalculateGloveStatistics implements Function<Map<Finger, List<GloveDataEntity>>, HandStatistics> {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(CalculateGloveStatistics.class);
    private String handName;
    private final StandardStatisticsAnalyzerImpl statisticsAnalyzer;

    public CalculateGloveStatistics(String handName) {
        this.statisticsAnalyzer = new StandardStatisticsAnalyzerImpl();
        this.handName = handName;
    }

    @Override
    public HandStatistics apply(Map<Finger, List<GloveDataEntity>> dataByFingersOfOneHand) {
        log.debug(format("Calculating statistics for %s hand", handName));
        HandStatistics handStatistics = new HandStatistics(handName);
        for (Map.Entry<Finger, List<GloveDataEntity>> dataByFinger : dataByFingersOfOneHand.entrySet()) {
            handStatistics.setAverageFor(dataByFinger.getKey(), statisticsAnalyzer.getAverage(dataByFinger.getValue(), GloveDataEntity::getRaw));
            handStatistics.setVarianceFor(dataByFinger.getKey(), statisticsAnalyzer.getVariance(dataByFinger.getValue(), GloveDataEntity::getRaw));
            handStatistics.setStandardDeviationFor(dataByFinger.getKey(), statisticsAnalyzer.getStandardDeviation(dataByFinger.getValue(), GloveDataEntity::getRaw));
            handStatistics.setSkewnessCoefficientFor(dataByFinger.getKey(), statisticsAnalyzer.getSkewness(dataByFinger.getValue(), GloveDataEntity::getRaw));
            handStatistics.setKurtosisFor(dataByFinger.getKey(), statisticsAnalyzer.getKurtosis(dataByFinger.getValue(), GloveDataEntity::getRaw));
        }
        log.debug("Calculation finished");
        return handStatistics;
    }

}
