package org.politechnika.service.report.glove_functions;

import org.politechnika.domain.GloveDataEntity;
import org.politechnika.service.report.model.glove.Finger;
import org.politechnika.service.report.model.glove.HandStatistics;
import org.politechnika.service.report.model.glove.TimeIntervalHandStatistics;
import org.slf4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.stream.Collectors.groupingBy;
import static org.politechnika.service.report.functions.GloveFunctions.sensorToFinger;

public class CalculateTimeIntervalStatistics implements Function<Map<Long, List<GloveDataEntity>>, TimeIntervalHandStatistics> {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(CalculateTimeIntervalStatistics.class);
    private String handName;

    public CalculateTimeIntervalStatistics(String handName) {
        this.handName = handName;
    }

    @Override
    public TimeIntervalHandStatistics apply(Map<Long, List<GloveDataEntity>> handDataPartitionedByTime) {
        log.debug(String.format("Creating time interval statistics for %s hand", handName));
        TimeIntervalHandStatistics statsByTimeInterval = new TimeIntervalHandStatistics();
        calculateStatsForEveryTimeInterval(handDataPartitionedByTime, statsByTimeInterval);
        statsByTimeInterval.setHandName(handName);
        log.debug("Calculation finished");

        return statsByTimeInterval;
    }

    private void calculateStatsForEveryTimeInterval(Map<Long, List<GloveDataEntity>> dataByFingersOfOneHand,
                                                    TimeIntervalHandStatistics statsByTimeInterval) {
        for (List<GloveDataEntity> dataSection : dataByFingersOfOneHand.values()) {
            Map<Finger, List<GloveDataEntity>> dataSectionByFingers = partitionDataSectionByFinger(dataSection);
            HandStatistics statistics = calculateStatistics(dataSectionByFingers);
            statsByTimeInterval.addStatstics(statistics);
        }
    }

    private Map<Finger, List<GloveDataEntity>> partitionDataSectionByFinger(List<GloveDataEntity> dataSection) {
        return dataSection.stream()
                .collect(groupingBy(sensorToFinger()));
    }

    private HandStatistics calculateStatistics(Map<Finger, List<GloveDataEntity>> dataSectionByFingers) {
        CalculateGloveStatistics gloveStatistics = new CalculateGloveStatistics(handName);
        return gloveStatistics.apply(dataSectionByFingers);
    }
}
