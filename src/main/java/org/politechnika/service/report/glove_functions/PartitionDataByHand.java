package org.politechnika.service.report.glove_functions;

import org.politechnika.domain.GloveDataEntity;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.stream.Collectors.groupingBy;

public class PartitionDataByHand implements Function<List<GloveDataEntity>, Map<String, List<GloveDataEntity>>> {

    @Override
    public Map<String, List<GloveDataEntity>> apply(List<GloveDataEntity> gloveDataEntities) {
        return gloveDataEntities.stream().collect(groupingBy(GloveDataEntity::getHand));
    }
}
