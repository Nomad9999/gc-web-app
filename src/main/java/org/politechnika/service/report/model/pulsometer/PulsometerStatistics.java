package org.politechnika.service.report.model.pulsometer;

public final class PulsometerStatistics {
    public static final String TAB = "\t";
    public static final String NEWLINE = "\n";
    private final double average;
    private final double stdDeviation;
    private final double variance;
    private final double skewness;
    private final double kurtosis;

    public PulsometerStatistics(double average, double stdDeviation, double variance, double skewness, double kurtosis) {
        this.average = average;
        this.stdDeviation = stdDeviation;
        this.variance = variance;
        this.skewness = skewness;
        this.kurtosis = kurtosis;
    }

    public String toReportString() {
        return new StringBuilder("Srednia arytmetyczna: ").append(TAB).append(average).append(NEWLINE)
                .append("Odchylenie standardowe: ").append(TAB).append(stdDeviation).append(NEWLINE)
                .append("Wariancja: ").append(TAB).append(variance).append(NEWLINE)
                .append("Wsp. Skośności: ").append(TAB).append(skewness).append(NEWLINE)
                .append("Kurtoza: ").append(TAB).append(kurtosis).append(NEWLINE)
                .toString();
    }

    public double getAverage() {
        return this.average;
    }

    public double getStdDeviation() {
        return this.stdDeviation;
    }

    public double getVariance() {
        return this.variance;
    }

    public double getSkewness() {
        return this.skewness;
    }

    public double getKurtosis() {
        return this.kurtosis;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PulsometerStatistics)) return false;
        final PulsometerStatistics other = (PulsometerStatistics) o;
        if (Double.compare(this.getAverage(), other.getAverage()) != 0) return false;
        if (Double.compare(this.getStdDeviation(), other.getStdDeviation()) != 0) return false;
        if (Double.compare(this.getVariance(), other.getVariance()) != 0) return false;
        if (Double.compare(this.getSkewness(), other.getSkewness()) != 0) return false;
        if (Double.compare(this.getKurtosis(), other.getKurtosis()) != 0) return false;
        return true;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $average = Double.doubleToLongBits(this.getAverage());
        result = result * PRIME + (int) ($average >>> 32 ^ $average);
        final long $stdDeviation = Double.doubleToLongBits(this.getStdDeviation());
        result = result * PRIME + (int) ($stdDeviation >>> 32 ^ $stdDeviation);
        final long $variance = Double.doubleToLongBits(this.getVariance());
        result = result * PRIME + (int) ($variance >>> 32 ^ $variance);
        final long $skewness = Double.doubleToLongBits(this.getSkewness());
        result = result * PRIME + (int) ($skewness >>> 32 ^ $skewness);
        final long $kurtosis = Double.doubleToLongBits(this.getKurtosis());
        result = result * PRIME + (int) ($kurtosis >>> 32 ^ $kurtosis);
        return result;
    }

    public String toString() {
        return "PulsometerStatistics(average=" + this.getAverage() + ", stdDeviation=" + this.getStdDeviation() + ", variance=" + this.getVariance() + ", skewness=" + this.getSkewness() + ", kurtosis=" + this.getKurtosis() + ")";
    }
}
