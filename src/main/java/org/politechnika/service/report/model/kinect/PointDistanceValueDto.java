package org.politechnika.service.report.model.kinect;

import org.politechnika.service.data_parser.model.TimeSequential;

import java.time.Instant;

public final class PointDistanceValueDto implements TimeSequential {
    private final double spineBase;
    private final double spineMid;
    private final double neck;
    private final double head;
    private final double shoulderLeft;
    private final double elbowLeft;
    private final double wristLeft;
    private final double handLeft;
    private final double shoulderRight;
    private final double elbowRight;
    private final double wristRight;
    private final double handRight;
    private final double hipLeft;
    private final double kneeLeft;
    private final double ankleLeft;
    private final double footLeft;
    private final double hipRight;
    private final double kneeRight;
    private final double ankleRight;
    private final double footRight;
    private final double spineShoulder;
    private final double handTipLeft;
    private final double thumbLeft;
    private final double handTipRight;
    private final double thumbRight;
    private final Instant time;

    public PointDistanceValueDto(double spineBase, double spineMid, double neck, double head, double shoulderLeft, double elbowLeft, double wristLeft, double handLeft, double shoulderRight, double elbowRight, double wristRight, double handRight, double hipLeft, double kneeLeft, double ankleLeft, double footLeft, double hipRight, double kneeRight, double ankleRight, double footRight, double spineShoulder, double handTipLeft, double thumbLeft, double handTipRight, double thumbRight, Instant time) {
        this.spineBase = spineBase;
        this.spineMid = spineMid;
        this.neck = neck;
        this.head = head;
        this.shoulderLeft = shoulderLeft;
        this.elbowLeft = elbowLeft;
        this.wristLeft = wristLeft;
        this.handLeft = handLeft;
        this.shoulderRight = shoulderRight;
        this.elbowRight = elbowRight;
        this.wristRight = wristRight;
        this.handRight = handRight;
        this.hipLeft = hipLeft;
        this.kneeLeft = kneeLeft;
        this.ankleLeft = ankleLeft;
        this.footLeft = footLeft;
        this.hipRight = hipRight;
        this.kneeRight = kneeRight;
        this.ankleRight = ankleRight;
        this.footRight = footRight;
        this.spineShoulder = spineShoulder;
        this.handTipLeft = handTipLeft;
        this.thumbLeft = thumbLeft;
        this.handTipRight = handTipRight;
        this.thumbRight = thumbRight;
        this.time = time;
    }

    public PointDistanceValueDto moveInTime(Instant newTime) {
        return new PointDistanceValueDto(
                spineBase, spineMid, neck, head, shoulderLeft, elbowLeft,
                wristLeft, handLeft, shoulderRight, elbowRight, wristRight, handRight, hipLeft,
                kneeLeft, ankleLeft, footLeft, hipRight, kneeRight, ankleRight, footRight,
                spineShoulder, handTipLeft, thumbLeft, handTipRight, thumbRight, newTime);
    }

    public double getSpineBase() {
        return this.spineBase;
    }

    public double getSpineMid() {
        return this.spineMid;
    }

    public double getNeck() {
        return this.neck;
    }

    public double getHead() {
        return this.head;
    }

    public double getShoulderLeft() {
        return this.shoulderLeft;
    }

    public double getElbowLeft() {
        return this.elbowLeft;
    }

    public double getWristLeft() {
        return this.wristLeft;
    }

    public double getHandLeft() {
        return this.handLeft;
    }

    public double getShoulderRight() {
        return this.shoulderRight;
    }

    public double getElbowRight() {
        return this.elbowRight;
    }

    public double getWristRight() {
        return this.wristRight;
    }

    public double getHandRight() {
        return this.handRight;
    }

    public double getHipLeft() {
        return this.hipLeft;
    }

    public double getKneeLeft() {
        return this.kneeLeft;
    }

    public double getAnkleLeft() {
        return this.ankleLeft;
    }

    public double getFootLeft() {
        return this.footLeft;
    }

    public double getHipRight() {
        return this.hipRight;
    }

    public double getKneeRight() {
        return this.kneeRight;
    }

    public double getAnkleRight() {
        return this.ankleRight;
    }

    public double getFootRight() {
        return this.footRight;
    }

    public double getSpineShoulder() {
        return this.spineShoulder;
    }

    public double getHandTipLeft() {
        return this.handTipLeft;
    }

    public double getThumbLeft() {
        return this.thumbLeft;
    }

    public double getHandTipRight() {
        return this.handTipRight;
    }

    public double getThumbRight() {
        return this.thumbRight;
    }

    public Instant getTime() {
        return this.time;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PointDistanceValueDto)) return false;
        final PointDistanceValueDto other = (PointDistanceValueDto) o;
        if (Double.compare(this.getSpineBase(), other.getSpineBase()) != 0) return false;
        if (Double.compare(this.getSpineMid(), other.getSpineMid()) != 0) return false;
        if (Double.compare(this.getNeck(), other.getNeck()) != 0) return false;
        if (Double.compare(this.getHead(), other.getHead()) != 0) return false;
        if (Double.compare(this.getShoulderLeft(), other.getShoulderLeft()) != 0) return false;
        if (Double.compare(this.getElbowLeft(), other.getElbowLeft()) != 0) return false;
        if (Double.compare(this.getWristLeft(), other.getWristLeft()) != 0) return false;
        if (Double.compare(this.getHandLeft(), other.getHandLeft()) != 0) return false;
        if (Double.compare(this.getShoulderRight(), other.getShoulderRight()) != 0) return false;
        if (Double.compare(this.getElbowRight(), other.getElbowRight()) != 0) return false;
        if (Double.compare(this.getWristRight(), other.getWristRight()) != 0) return false;
        if (Double.compare(this.getHandRight(), other.getHandRight()) != 0) return false;
        if (Double.compare(this.getHipLeft(), other.getHipLeft()) != 0) return false;
        if (Double.compare(this.getKneeLeft(), other.getKneeLeft()) != 0) return false;
        if (Double.compare(this.getAnkleLeft(), other.getAnkleLeft()) != 0) return false;
        if (Double.compare(this.getFootLeft(), other.getFootLeft()) != 0) return false;
        if (Double.compare(this.getHipRight(), other.getHipRight()) != 0) return false;
        if (Double.compare(this.getKneeRight(), other.getKneeRight()) != 0) return false;
        if (Double.compare(this.getAnkleRight(), other.getAnkleRight()) != 0) return false;
        if (Double.compare(this.getFootRight(), other.getFootRight()) != 0) return false;
        if (Double.compare(this.getSpineShoulder(), other.getSpineShoulder()) != 0) return false;
        if (Double.compare(this.getHandTipLeft(), other.getHandTipLeft()) != 0) return false;
        if (Double.compare(this.getThumbLeft(), other.getThumbLeft()) != 0) return false;
        if (Double.compare(this.getHandTipRight(), other.getHandTipRight()) != 0) return false;
        if (Double.compare(this.getThumbRight(), other.getThumbRight()) != 0) return false;
        final Object this$time = this.getTime();
        final Object other$time = other.getTime();
        if (this$time == null ? other$time != null : !this$time.equals(other$time)) return false;
        return true;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $spineBase = Double.doubleToLongBits(this.getSpineBase());
        result = result * PRIME + (int) ($spineBase >>> 32 ^ $spineBase);
        final long $spineMid = Double.doubleToLongBits(this.getSpineMid());
        result = result * PRIME + (int) ($spineMid >>> 32 ^ $spineMid);
        final long $neck = Double.doubleToLongBits(this.getNeck());
        result = result * PRIME + (int) ($neck >>> 32 ^ $neck);
        final long $head = Double.doubleToLongBits(this.getHead());
        result = result * PRIME + (int) ($head >>> 32 ^ $head);
        final long $shoulderLeft = Double.doubleToLongBits(this.getShoulderLeft());
        result = result * PRIME + (int) ($shoulderLeft >>> 32 ^ $shoulderLeft);
        final long $elbowLeft = Double.doubleToLongBits(this.getElbowLeft());
        result = result * PRIME + (int) ($elbowLeft >>> 32 ^ $elbowLeft);
        final long $wristLeft = Double.doubleToLongBits(this.getWristLeft());
        result = result * PRIME + (int) ($wristLeft >>> 32 ^ $wristLeft);
        final long $handLeft = Double.doubleToLongBits(this.getHandLeft());
        result = result * PRIME + (int) ($handLeft >>> 32 ^ $handLeft);
        final long $shoulderRight = Double.doubleToLongBits(this.getShoulderRight());
        result = result * PRIME + (int) ($shoulderRight >>> 32 ^ $shoulderRight);
        final long $elbowRight = Double.doubleToLongBits(this.getElbowRight());
        result = result * PRIME + (int) ($elbowRight >>> 32 ^ $elbowRight);
        final long $wristRight = Double.doubleToLongBits(this.getWristRight());
        result = result * PRIME + (int) ($wristRight >>> 32 ^ $wristRight);
        final long $handRight = Double.doubleToLongBits(this.getHandRight());
        result = result * PRIME + (int) ($handRight >>> 32 ^ $handRight);
        final long $hipLeft = Double.doubleToLongBits(this.getHipLeft());
        result = result * PRIME + (int) ($hipLeft >>> 32 ^ $hipLeft);
        final long $kneeLeft = Double.doubleToLongBits(this.getKneeLeft());
        result = result * PRIME + (int) ($kneeLeft >>> 32 ^ $kneeLeft);
        final long $ankleLeft = Double.doubleToLongBits(this.getAnkleLeft());
        result = result * PRIME + (int) ($ankleLeft >>> 32 ^ $ankleLeft);
        final long $footLeft = Double.doubleToLongBits(this.getFootLeft());
        result = result * PRIME + (int) ($footLeft >>> 32 ^ $footLeft);
        final long $hipRight = Double.doubleToLongBits(this.getHipRight());
        result = result * PRIME + (int) ($hipRight >>> 32 ^ $hipRight);
        final long $kneeRight = Double.doubleToLongBits(this.getKneeRight());
        result = result * PRIME + (int) ($kneeRight >>> 32 ^ $kneeRight);
        final long $ankleRight = Double.doubleToLongBits(this.getAnkleRight());
        result = result * PRIME + (int) ($ankleRight >>> 32 ^ $ankleRight);
        final long $footRight = Double.doubleToLongBits(this.getFootRight());
        result = result * PRIME + (int) ($footRight >>> 32 ^ $footRight);
        final long $spineShoulder = Double.doubleToLongBits(this.getSpineShoulder());
        result = result * PRIME + (int) ($spineShoulder >>> 32 ^ $spineShoulder);
        final long $handTipLeft = Double.doubleToLongBits(this.getHandTipLeft());
        result = result * PRIME + (int) ($handTipLeft >>> 32 ^ $handTipLeft);
        final long $thumbLeft = Double.doubleToLongBits(this.getThumbLeft());
        result = result * PRIME + (int) ($thumbLeft >>> 32 ^ $thumbLeft);
        final long $handTipRight = Double.doubleToLongBits(this.getHandTipRight());
        result = result * PRIME + (int) ($handTipRight >>> 32 ^ $handTipRight);
        final long $thumbRight = Double.doubleToLongBits(this.getThumbRight());
        result = result * PRIME + (int) ($thumbRight >>> 32 ^ $thumbRight);
        final Object $time = this.getTime();
        result = result * PRIME + ($time == null ? 43 : $time.hashCode());
        return result;
    }

    public String toString() {
        return "PointDistanceValueDto(spineBase=" + this.getSpineBase() + ", spineMid=" + this.getSpineMid() + ", neck=" + this.getNeck() + ", head=" + this.getHead() + ", shoulderLeft=" + this.getShoulderLeft() + ", elbowLeft=" + this.getElbowLeft() + ", wristLeft=" + this.getWristLeft() + ", handLeft=" + this.getHandLeft() + ", shoulderRight=" + this.getShoulderRight() + ", elbowRight=" + this.getElbowRight() + ", wristRight=" + this.getWristRight() + ", handRight=" + this.getHandRight() + ", hipLeft=" + this.getHipLeft() + ", kneeLeft=" + this.getKneeLeft() + ", ankleLeft=" + this.getAnkleLeft() + ", footLeft=" + this.getFootLeft() + ", hipRight=" + this.getHipRight() + ", kneeRight=" + this.getKneeRight() + ", ankleRight=" + this.getAnkleRight() + ", footRight=" + this.getFootRight() + ", spineShoulder=" + this.getSpineShoulder() + ", handTipLeft=" + this.getHandTipLeft() + ", thumbLeft=" + this.getThumbLeft() + ", handTipRight=" + this.getHandTipRight() + ", thumbRight=" + this.getThumbRight() + ", time=" + this.getTime() + ")";
    }
}
