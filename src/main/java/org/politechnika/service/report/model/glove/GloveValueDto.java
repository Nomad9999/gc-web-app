package org.politechnika.service.report.model.glove;

import org.politechnika.service.data_parser.model.TimeSequential;

import java.time.Instant;

public final class GloveValueDto implements TimeSequential {
    private final double thumb;
    private final double index;
    private final double middle;
    private final double ring;
    private final double little;
    private final Instant time;

    public GloveValueDto(double thumb, double index, double middle, double ring, double little, Instant time) {
        this.thumb = thumb;
        this.index = index;
        this.middle = middle;
        this.ring = ring;
        this.little = little;
        this.time = time;
    }

    public GloveValueDto moveInTime(Instant newTime) {
        return new GloveValueDto(thumb, index, middle, ring, little, newTime);
    }

    public double getThumb() {
        return this.thumb;
    }

    public double getIndex() {
        return this.index;
    }

    public double getMiddle() {
        return this.middle;
    }

    public double getRing() {
        return this.ring;
    }

    public double getLittle() {
        return this.little;
    }

    public Instant getTime() {
        return this.time;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof GloveValueDto)) return false;
        final GloveValueDto other = (GloveValueDto) o;
        if (Double.compare(this.getThumb(), other.getThumb()) != 0) return false;
        if (Double.compare(this.getIndex(), other.getIndex()) != 0) return false;
        if (Double.compare(this.getMiddle(), other.getMiddle()) != 0) return false;
        if (Double.compare(this.getRing(), other.getRing()) != 0) return false;
        if (Double.compare(this.getLittle(), other.getLittle()) != 0) return false;
        final Object this$time = this.getTime();
        final Object other$time = other.getTime();
        if (this$time == null ? other$time != null : !this$time.equals(other$time)) return false;
        return true;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $thumb = Double.doubleToLongBits(this.getThumb());
        result = result * PRIME + (int) ($thumb >>> 32 ^ $thumb);
        final long $index = Double.doubleToLongBits(this.getIndex());
        result = result * PRIME + (int) ($index >>> 32 ^ $index);
        final long $middle = Double.doubleToLongBits(this.getMiddle());
        result = result * PRIME + (int) ($middle >>> 32 ^ $middle);
        final long $ring = Double.doubleToLongBits(this.getRing());
        result = result * PRIME + (int) ($ring >>> 32 ^ $ring);
        final long $little = Double.doubleToLongBits(this.getLittle());
        result = result * PRIME + (int) ($little >>> 32 ^ $little);
        final Object $time = this.getTime();
        result = result * PRIME + ($time == null ? 43 : $time.hashCode());
        return result;
    }

    public String toString() {
        return "GloveValueDto(thumb=" + this.getThumb() + ", index=" + this.getIndex() + ", middle=" + this.getMiddle() + ", ring=" + this.getRing() + ", little=" + this.getLittle() + ", time=" + this.getTime() + ")";
    }
}
