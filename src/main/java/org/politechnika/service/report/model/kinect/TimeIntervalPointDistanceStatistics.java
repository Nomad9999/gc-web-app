package org.politechnika.service.report.model.kinect;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.stream.IntStream;

public class TimeIntervalPointDistanceStatistics {

    private List<PointDistanceStatistics> statistics;

    public List<PointDistanceStatistics> getStatistics() {
        return statistics;
    }

    public TimeIntervalPointDistanceStatistics() {
        this.statistics = Lists.newArrayList();
    }

    public void addStatstics(PointDistanceStatistics kinectStatistics) {
        statistics.add(kinectStatistics);
    }

    public double[] getTimeDimension() {
        return IntStream.range(0, statistics.size())
                .mapToDouble(value ->  ((double) value))
                .toArray();
    }

    public double[] getAverageValue(Sensor sensor) {
        return statistics.stream().mapToDouble(stats -> stats.getAverageForSensorView(sensor)).toArray();
    }

    public double[] getKurtosisValue(Sensor sensor) {
        return statistics.stream().mapToDouble(stats -> stats.getKurtosisForSensorView(sensor)).toArray();
    }

    public double[] getSkewnessValue(Sensor sensor) {
        return statistics.stream().mapToDouble(stats -> stats.getSkewnessForSensorView(sensor)).toArray();
    }

    public double[] getStandardDeviationValue(Sensor sensor) {
        return statistics.stream().mapToDouble(stats -> stats.getStandardDeviationForSensorView(sensor)).toArray();
    }

    public double[] getVarianceValue(Sensor sensor) {
        return statistics.stream().mapToDouble(stats -> stats.getVarianceForSensorView(sensor)).toArray();
    }
}
