package org.politechnika.service.report.model.pulsometer;

import org.politechnika.service.data_parser.model.TimeSequential;

import java.time.Instant;

public final class PulsometerValueDto implements  TimeSequential {
    private final Instant time;
    private final double value;

    public PulsometerValueDto(Instant time, double value) {
        this.time = time;
        this.value = value;
    }

    public PulsometerValueDto moveInTime(Instant newTime) {
        return new PulsometerValueDto(newTime, value);
    }

    public Instant getTime() {
        return this.time;
    }

    public double getValue() {
        return this.value;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PulsometerValueDto)) return false;
        final PulsometerValueDto other = (PulsometerValueDto) o;
        final Object this$time = this.getTime();
        final Object other$time = other.getTime();
        if (this$time == null ? other$time != null : !this$time.equals(other$time)) return false;
        if (Double.compare(this.getValue(), other.getValue()) != 0) return false;
        return true;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $time = this.getTime();
        result = result * PRIME + ($time == null ? 43 : $time.hashCode());
        final long $value = Double.doubleToLongBits(this.getValue());
        result = result * PRIME + (int) ($value >>> 32 ^ $value);
        return result;
    }

    public String toString() {
        return "PulsometerValueDto(time=" + this.getTime() + ", value=" + this.getValue() + ")";
    }
}
