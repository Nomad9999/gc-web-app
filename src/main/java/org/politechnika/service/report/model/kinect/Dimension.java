package org.politechnika.service.report.model.kinect;

public enum Dimension {

    X, Y, Z
}
