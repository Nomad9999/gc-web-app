package org.politechnika.service.report.model.kinect;

import com.opencsv.bean.CsvBindByName;

import java.time.Instant;

public class PointDistance  {

    private final String description;

    @CsvBindByName(column = "odcinek_ledzwiowy_kregoslupa")
    private double spineBase;

    @CsvBindByName(column = "odcinek_srodkowy_kregoslupa")
    private double spineMid;

    @CsvBindByName(column = "szyja")
    private double neck;

    @CsvBindByName(column = "glowa")
    private double head;

    @CsvBindByName(column = "ramie_lewe")
    private double shoulderLeft;

    @CsvBindByName(column = "lewy_lokiec")
    private double elbowLeft;

    @CsvBindByName(column = "lewy_nadgarstek")
    private double wristLeft;

    @CsvBindByName(column = "lewa_reka")
    private double handLeft;

    @CsvBindByName(column = "ramie_prawe")
    private double shoulderRight;

    @CsvBindByName(column = "prawy_lokiec")
    private double elbowRight;

    @CsvBindByName(column = "prawy_nadgarstek")
    private double wristRight;

    @CsvBindByName(column = "prawa_reka")
    private double handRight;

    @CsvBindByName(column = "lewe_biodro")
    private double hipLeft;

    @CsvBindByName(column = "lewe_kolano")
    private double kneeLeft;

    @CsvBindByName(column = "lewa_kostka")
    private double ankleLeft;

    @CsvBindByName(column = "lewa_stopa")
    private double footLeft;

    @CsvBindByName(column = "prawe_biodro")
    private double hipRight;

    @CsvBindByName(column = "prawe_kolano")
    private double kneeRight;

    @CsvBindByName(column = "prawa_kostka")
    private double ankleRight;

    @CsvBindByName(column = "prawa_stopa")
    private double footRight;

    @CsvBindByName(column = "odcinek_szyjny_kregoslupa")
    private double spineShoulder;

    @CsvBindByName(column = "palec_wskazujacy_lewy")
    private double handTipLeft;

    @CsvBindByName(column = "kciuk_lewy")
    private double thumbLeft;

    @CsvBindByName(column = "palec_wskazujacy_prawy")
    private double handTipRight;

    @CsvBindByName(column = "kciuk_prawy")
    private double thumbRight;

    private Instant time;

    public PointDistance(String description) {
        this.description = description;
    }

    public void setValueFor(Sensor sensor, double value) {
        switch (sensor) {
            case SPINE_BASE: spineBase = value; return;
            case SPINE_MID:  spineMid = value; return;
            case NECK:  neck  = value; return;
            case HEAD:  head  = value; return;
            case SHOULDER_LEFT:  shoulderLeft  = value; return;
            case SHOULDER_RIGHT:  shoulderRight  = value; return;
            case ELBOW_LEFT:  elbowLeft  = value; return;
            case ELBOW_RIGHT:  elbowRight  = value; return;
            case WRIST_LEFT:  wristLeft  = value; return;
            case WRIST_RIGHT:  wristRight  = value; return;
            case HAND_LEFT:  handLeft  = value; return;
            case HAND_RIGHT:  handRight  = value; return;
            case HIP_LEFT:  hipLeft  = value; return;
            case HIP_RIGHT:  hipRight  = value; return;
            case KNEE_LEFT:  kneeLeft  = value; return;
            case KNEE_RIGHT:  kneeRight  = value; return;
            case ANKLE_LEFT:  ankleLeft  = value; return;
            case ANKLE_RIGHT:  ankleRight  = value; return;
            case FOOT_LEFT:  footLeft  = value; return;
            case FOOT_RIGHT:  footRight  = value; return;
            case SPINE_SHOULDER:  spineShoulder  = value; return;
            case HAND_TIP_LEFT:  handTipLeft  = value; return;
            case HAND_TIP_RIGHT:  handTipRight  = value; return;
            case THUMB_LEFT:  thumbLeft  = value; return;
            case THUMB_RIGHT:  thumbRight  = value; return;
        }
    }

     double getValueFor(Sensor sensor) {
        switch (sensor) {
            case SPINE_BASE: return spineBase;
            case SPINE_MID: return spineMid;
            case NECK: return neck;
            case HEAD: return head;
            case SHOULDER_LEFT: return shoulderLeft;
            case SHOULDER_RIGHT: return shoulderRight;
            case ELBOW_LEFT: return elbowLeft;
            case ELBOW_RIGHT: return elbowRight;
            case WRIST_LEFT: return wristLeft;
            case WRIST_RIGHT: return wristRight;
            case HAND_LEFT: return handLeft;
            case HAND_RIGHT: return handRight;
            case HIP_LEFT: return hipLeft;
            case HIP_RIGHT: return hipRight;
            case KNEE_LEFT: return kneeLeft;
            case KNEE_RIGHT: return kneeRight;
            case ANKLE_LEFT: return ankleLeft;
            case ANKLE_RIGHT: return ankleRight;
            case FOOT_LEFT: return footLeft;
            case FOOT_RIGHT: return footRight;
            case SPINE_SHOULDER: return spineShoulder;
            case HAND_TIP_LEFT: return handTipLeft;
            case HAND_TIP_RIGHT: return handTipRight;
            case THUMB_LEFT: return thumbLeft;
            case THUMB_RIGHT: return thumbRight;
        }
        throw new IllegalArgumentException("Could not find value for sensor " + sensor);
    }

    public String getDescription() {
        return this.description;
    }

    public double getSpineBase() {
        return this.spineBase;
    }

    public double getSpineMid() {
        return this.spineMid;
    }

    public double getNeck() {
        return this.neck;
    }

    public double getHead() {
        return this.head;
    }

    public double getShoulderLeft() {
        return this.shoulderLeft;
    }

    public double getElbowLeft() {
        return this.elbowLeft;
    }

    public double getWristLeft() {
        return this.wristLeft;
    }

    public double getHandLeft() {
        return this.handLeft;
    }

    public double getShoulderRight() {
        return this.shoulderRight;
    }

    public double getElbowRight() {
        return this.elbowRight;
    }

    public double getWristRight() {
        return this.wristRight;
    }

    public double getHandRight() {
        return this.handRight;
    }

    public double getHipLeft() {
        return this.hipLeft;
    }

    public double getKneeLeft() {
        return this.kneeLeft;
    }

    public double getAnkleLeft() {
        return this.ankleLeft;
    }

    public double getFootLeft() {
        return this.footLeft;
    }

    public double getHipRight() {
        return this.hipRight;
    }

    public double getKneeRight() {
        return this.kneeRight;
    }

    public double getAnkleRight() {
        return this.ankleRight;
    }

    public double getFootRight() {
        return this.footRight;
    }

    public double getSpineShoulder() {
        return this.spineShoulder;
    }

    public double getHandTipLeft() {
        return this.handTipLeft;
    }

    public double getThumbLeft() {
        return this.thumbLeft;
    }

    public double getHandTipRight() {
        return this.handTipRight;
    }

    public double getThumbRight() {
        return this.thumbRight;
    }

    public Instant getTime() {
        return this.time;
    }

    public void setSpineBase(double spineBase) {
        this.spineBase = spineBase;
    }

    public void setSpineMid(double spineMid) {
        this.spineMid = spineMid;
    }

    public void setNeck(double neck) {
        this.neck = neck;
    }

    public void setHead(double head) {
        this.head = head;
    }

    public void setShoulderLeft(double shoulderLeft) {
        this.shoulderLeft = shoulderLeft;
    }

    public void setElbowLeft(double elbowLeft) {
        this.elbowLeft = elbowLeft;
    }

    public void setWristLeft(double wristLeft) {
        this.wristLeft = wristLeft;
    }

    public void setHandLeft(double handLeft) {
        this.handLeft = handLeft;
    }

    public void setShoulderRight(double shoulderRight) {
        this.shoulderRight = shoulderRight;
    }

    public void setElbowRight(double elbowRight) {
        this.elbowRight = elbowRight;
    }

    public void setWristRight(double wristRight) {
        this.wristRight = wristRight;
    }

    public void setHandRight(double handRight) {
        this.handRight = handRight;
    }

    public void setHipLeft(double hipLeft) {
        this.hipLeft = hipLeft;
    }

    public void setKneeLeft(double kneeLeft) {
        this.kneeLeft = kneeLeft;
    }

    public void setAnkleLeft(double ankleLeft) {
        this.ankleLeft = ankleLeft;
    }

    public void setFootLeft(double footLeft) {
        this.footLeft = footLeft;
    }

    public void setHipRight(double hipRight) {
        this.hipRight = hipRight;
    }

    public void setKneeRight(double kneeRight) {
        this.kneeRight = kneeRight;
    }

    public void setAnkleRight(double ankleRight) {
        this.ankleRight = ankleRight;
    }

    public void setFootRight(double footRight) {
        this.footRight = footRight;
    }

    public void setSpineShoulder(double spineShoulder) {
        this.spineShoulder = spineShoulder;
    }

    public void setHandTipLeft(double handTipLeft) {
        this.handTipLeft = handTipLeft;
    }

    public void setThumbLeft(double thumbLeft) {
        this.thumbLeft = thumbLeft;
    }

    public void setHandTipRight(double handTipRight) {
        this.handTipRight = handTipRight;
    }

    public void setThumbRight(double thumbRight) {
        this.thumbRight = thumbRight;
    }

    public void setTime(Instant time) {
        this.time = time;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PointDistance)) return false;
        final PointDistance other = (PointDistance) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$description = this.getDescription();
        final Object other$description = other.getDescription();
        if (this$description == null ? other$description != null : !this$description.equals(other$description))
            return false;
        if (Double.compare(this.getSpineBase(), other.getSpineBase()) != 0) return false;
        if (Double.compare(this.getSpineMid(), other.getSpineMid()) != 0) return false;
        if (Double.compare(this.getNeck(), other.getNeck()) != 0) return false;
        if (Double.compare(this.getHead(), other.getHead()) != 0) return false;
        if (Double.compare(this.getShoulderLeft(), other.getShoulderLeft()) != 0) return false;
        if (Double.compare(this.getElbowLeft(), other.getElbowLeft()) != 0) return false;
        if (Double.compare(this.getWristLeft(), other.getWristLeft()) != 0) return false;
        if (Double.compare(this.getHandLeft(), other.getHandLeft()) != 0) return false;
        if (Double.compare(this.getShoulderRight(), other.getShoulderRight()) != 0) return false;
        if (Double.compare(this.getElbowRight(), other.getElbowRight()) != 0) return false;
        if (Double.compare(this.getWristRight(), other.getWristRight()) != 0) return false;
        if (Double.compare(this.getHandRight(), other.getHandRight()) != 0) return false;
        if (Double.compare(this.getHipLeft(), other.getHipLeft()) != 0) return false;
        if (Double.compare(this.getKneeLeft(), other.getKneeLeft()) != 0) return false;
        if (Double.compare(this.getAnkleLeft(), other.getAnkleLeft()) != 0) return false;
        if (Double.compare(this.getFootLeft(), other.getFootLeft()) != 0) return false;
        if (Double.compare(this.getHipRight(), other.getHipRight()) != 0) return false;
        if (Double.compare(this.getKneeRight(), other.getKneeRight()) != 0) return false;
        if (Double.compare(this.getAnkleRight(), other.getAnkleRight()) != 0) return false;
        if (Double.compare(this.getFootRight(), other.getFootRight()) != 0) return false;
        if (Double.compare(this.getSpineShoulder(), other.getSpineShoulder()) != 0) return false;
        if (Double.compare(this.getHandTipLeft(), other.getHandTipLeft()) != 0) return false;
        if (Double.compare(this.getThumbLeft(), other.getThumbLeft()) != 0) return false;
        if (Double.compare(this.getHandTipRight(), other.getHandTipRight()) != 0) return false;
        if (Double.compare(this.getThumbRight(), other.getThumbRight()) != 0) return false;
        final Object this$time = this.getTime();
        final Object other$time = other.getTime();
        if (this$time == null ? other$time != null : !this$time.equals(other$time)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PointDistance;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $description = this.getDescription();
        result = result * PRIME + ($description == null ? 43 : $description.hashCode());
        final long $spineBase = Double.doubleToLongBits(this.getSpineBase());
        result = result * PRIME + (int) ($spineBase >>> 32 ^ $spineBase);
        final long $spineMid = Double.doubleToLongBits(this.getSpineMid());
        result = result * PRIME + (int) ($spineMid >>> 32 ^ $spineMid);
        final long $neck = Double.doubleToLongBits(this.getNeck());
        result = result * PRIME + (int) ($neck >>> 32 ^ $neck);
        final long $head = Double.doubleToLongBits(this.getHead());
        result = result * PRIME + (int) ($head >>> 32 ^ $head);
        final long $shoulderLeft = Double.doubleToLongBits(this.getShoulderLeft());
        result = result * PRIME + (int) ($shoulderLeft >>> 32 ^ $shoulderLeft);
        final long $elbowLeft = Double.doubleToLongBits(this.getElbowLeft());
        result = result * PRIME + (int) ($elbowLeft >>> 32 ^ $elbowLeft);
        final long $wristLeft = Double.doubleToLongBits(this.getWristLeft());
        result = result * PRIME + (int) ($wristLeft >>> 32 ^ $wristLeft);
        final long $handLeft = Double.doubleToLongBits(this.getHandLeft());
        result = result * PRIME + (int) ($handLeft >>> 32 ^ $handLeft);
        final long $shoulderRight = Double.doubleToLongBits(this.getShoulderRight());
        result = result * PRIME + (int) ($shoulderRight >>> 32 ^ $shoulderRight);
        final long $elbowRight = Double.doubleToLongBits(this.getElbowRight());
        result = result * PRIME + (int) ($elbowRight >>> 32 ^ $elbowRight);
        final long $wristRight = Double.doubleToLongBits(this.getWristRight());
        result = result * PRIME + (int) ($wristRight >>> 32 ^ $wristRight);
        final long $handRight = Double.doubleToLongBits(this.getHandRight());
        result = result * PRIME + (int) ($handRight >>> 32 ^ $handRight);
        final long $hipLeft = Double.doubleToLongBits(this.getHipLeft());
        result = result * PRIME + (int) ($hipLeft >>> 32 ^ $hipLeft);
        final long $kneeLeft = Double.doubleToLongBits(this.getKneeLeft());
        result = result * PRIME + (int) ($kneeLeft >>> 32 ^ $kneeLeft);
        final long $ankleLeft = Double.doubleToLongBits(this.getAnkleLeft());
        result = result * PRIME + (int) ($ankleLeft >>> 32 ^ $ankleLeft);
        final long $footLeft = Double.doubleToLongBits(this.getFootLeft());
        result = result * PRIME + (int) ($footLeft >>> 32 ^ $footLeft);
        final long $hipRight = Double.doubleToLongBits(this.getHipRight());
        result = result * PRIME + (int) ($hipRight >>> 32 ^ $hipRight);
        final long $kneeRight = Double.doubleToLongBits(this.getKneeRight());
        result = result * PRIME + (int) ($kneeRight >>> 32 ^ $kneeRight);
        final long $ankleRight = Double.doubleToLongBits(this.getAnkleRight());
        result = result * PRIME + (int) ($ankleRight >>> 32 ^ $ankleRight);
        final long $footRight = Double.doubleToLongBits(this.getFootRight());
        result = result * PRIME + (int) ($footRight >>> 32 ^ $footRight);
        final long $spineShoulder = Double.doubleToLongBits(this.getSpineShoulder());
        result = result * PRIME + (int) ($spineShoulder >>> 32 ^ $spineShoulder);
        final long $handTipLeft = Double.doubleToLongBits(this.getHandTipLeft());
        result = result * PRIME + (int) ($handTipLeft >>> 32 ^ $handTipLeft);
        final long $thumbLeft = Double.doubleToLongBits(this.getThumbLeft());
        result = result * PRIME + (int) ($thumbLeft >>> 32 ^ $thumbLeft);
        final long $handTipRight = Double.doubleToLongBits(this.getHandTipRight());
        result = result * PRIME + (int) ($handTipRight >>> 32 ^ $handTipRight);
        final long $thumbRight = Double.doubleToLongBits(this.getThumbRight());
        result = result * PRIME + (int) ($thumbRight >>> 32 ^ $thumbRight);
        final Object $time = this.getTime();
        result = result * PRIME + ($time == null ? 43 : $time.hashCode());
        return result;
    }

    public String toString() {
        return "PointDistance(description=" + this.getDescription() + ", spineBase=" + this.getSpineBase() + ", spineMid=" + this.getSpineMid() + ", neck=" + this.getNeck() + ", head=" + this.getHead() + ", shoulderLeft=" + this.getShoulderLeft() + ", elbowLeft=" + this.getElbowLeft() + ", wristLeft=" + this.getWristLeft() + ", handLeft=" + this.getHandLeft() + ", shoulderRight=" + this.getShoulderRight() + ", elbowRight=" + this.getElbowRight() + ", wristRight=" + this.getWristRight() + ", handRight=" + this.getHandRight() + ", hipLeft=" + this.getHipLeft() + ", kneeLeft=" + this.getKneeLeft() + ", ankleLeft=" + this.getAnkleLeft() + ", footLeft=" + this.getFootLeft() + ", hipRight=" + this.getHipRight() + ", kneeRight=" + this.getKneeRight() + ", ankleRight=" + this.getAnkleRight() + ", footRight=" + this.getFootRight() + ", spineShoulder=" + this.getSpineShoulder() + ", handTipLeft=" + this.getHandTipLeft() + ", thumbLeft=" + this.getThumbLeft() + ", handTipRight=" + this.getHandTipRight() + ", thumbRight=" + this.getThumbRight() + ", time=" + this.getTime() + ")";
    }
}
