package org.politechnika.service.report.model.glove;

import com.google.common.collect.Lists;
import org.politechnika.commons.Constants;

import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.joining;
import static org.politechnika.commons.Separators.TAB;
import static org.politechnika.service.report.model.glove.Finger.*;

public class TimeIntervalHandStatistics {

    private String handName = "Hand";
    private List<HandStatistics> statistics;

    public TimeIntervalHandStatistics() {
        this.statistics = Lists.newArrayList();
    }

    public String getPolishHandName() {
        return "right".equals(handName) ? "prawa" : "lewa";
    }

    public void addStatstics(HandStatistics handStatistics) {
        statistics.add(handStatistics);
    }

    public double[] getTimeDimension() {
        return IntStream.range(0, statistics.size())
                .mapToDouble(value ->  ((double) value * 1000))
                .toArray();
    }

    public double[] getAverageValueDimensionForFinger(Finger finger) {
        return statistics.stream().mapToDouble(stats -> stats.getAverageForFinger(finger)).toArray();
    }

    public double[] getKurtosisValueDimensionForFinger(Finger finger) {
        return statistics.stream().mapToDouble(stats -> stats.getKurtosisForFinger(finger)).toArray();
    }

    public double[] getSkewnessValueDimensionForFinger(Finger finger) {
        return statistics.stream().mapToDouble(stats -> stats.getSkewnessForFinger(finger)).toArray();
    }

    public double[] getStandardDeviationValueDimensionForFinger(Finger finger) {
        return statistics.stream().mapToDouble(stats -> stats.getStandardDeviationForFinger(finger)).toArray();
    }

    public double[] getVarianceValueDimensionForFinger(Finger finger) {
        return statistics.stream().mapToDouble(stats -> stats.getVarianceForFinger(finger)).toArray();
    }

    public String toReportString() {
        StringBuilder sb = new StringBuilder();
        sb.append("------------- dla okresu ----------");
        sb.append("Srednia");

        sb.append("Kciuk: " + statistics.stream().map(stats -> stats.getAverageForFingerView(THUMB)).collect(joining(TAB)));
        sb.append("Wskazujacy: " + statistics.stream().map(stats -> stats.getAverageForFingerView(INDEX)).collect(joining(TAB)));
        sb.append("Srodkowy: " + statistics.stream().map(stats -> stats.getAverageForFingerView(MIDDLE)).collect(joining(TAB)));
        sb.append("Serdeczny: " + statistics.stream().map(stats -> stats.getAverageForFingerView(RING)).collect(joining(TAB)));
        sb.append("Mały: " + statistics.stream().map(stats -> stats.getAverageForFingerView(LITTLE)).collect(joining(TAB)));

        sb.append("-----------------------");
        sb.append("Wariancja");

        sb.append("Kciuk: " + statistics.stream().map(stats -> stats.getVarianceForFingerView(THUMB)).collect(joining(TAB)));
        sb.append("Wskazujacy: " + statistics.stream().map(stats -> stats.getVarianceForFingerView(INDEX)).collect(joining(TAB)));
        sb.append("Srodkowy: " + statistics.stream().map(stats -> stats.getVarianceForFingerView(MIDDLE)).collect(joining(TAB)));
        sb.append("Serdeczny: " + statistics.stream().map(stats -> stats.getVarianceForFingerView(RING)).collect(joining(TAB)));
        sb.append("Mały: " + statistics.stream().map(stats -> stats.getVarianceForFingerView(LITTLE)).collect(joining(TAB)));

        sb.append("-----------------------");
        sb.append("Odchylenie standardowe");

        sb.append("Kciuk: " + statistics.stream().map(stats -> stats.getStandardDeviationForFingerView(THUMB)).collect(joining(TAB)));
        sb.append("Wskazujacy: " + statistics.stream().map(stats -> stats.getStandardDeviationForFingerView(INDEX)).collect(joining(TAB)));
        sb.append("Srodkowy: " + statistics.stream().map(stats -> stats.getStandardDeviationForFingerView(MIDDLE)).collect(joining(TAB)));
        sb.append("Serdeczny: " + statistics.stream().map(stats -> stats.getStandardDeviationForFingerView(RING)).collect(joining(TAB)));
        sb.append("Mały: " + statistics.stream().map(stats -> stats.getStandardDeviationForFingerView(LITTLE)).collect(joining(TAB)));

        sb.append("-----------------------");
        sb.append("Wsp Skośnosci");

        sb.append("Kciuk: " + statistics.stream().map(stats -> stats.getSkewnessForFingerView(THUMB)).collect(joining(TAB)));
        sb.append("Wskazujacy: " + statistics.stream().map(stats -> stats.getSkewnessForFingerView(INDEX)).collect(joining(TAB)));
        sb.append("Srodkowy: " + statistics.stream().map(stats -> stats.getSkewnessForFingerView(MIDDLE)).collect(joining(TAB)));
        sb.append("Serdeczny: " + statistics.stream().map(stats -> stats.getSkewnessForFingerView(RING)).collect(joining(TAB)));
        sb.append("Mały: " + statistics.stream().map(stats -> stats.getSkewnessForFingerView(LITTLE)).collect(joining(TAB)));

        sb.append("-----------------------");
        sb.append("Kurtoza");

        sb.append("Kciuk: " + statistics.stream().map(stats -> stats.getKurtosisForFingerView(THUMB)).collect(joining(TAB)));
        sb.append("Wskazujacy: " + statistics.stream().map(stats -> stats.getKurtosisForFingerView(INDEX)).collect(joining(TAB)));
        sb.append("Srodkowy: " + statistics.stream().map(stats -> stats.getKurtosisForFingerView(MIDDLE)).collect(joining(TAB)));
        sb.append("Serdeczny: " + statistics.stream().map(stats -> stats.getKurtosisForFingerView(RING)).collect(joining(TAB)));
        sb.append("Mały: " + statistics.stream().map(stats -> stats.getKurtosisForFingerView(LITTLE)).collect(joining(TAB)));

        return sb.toString();
    }

    public String getHandName() {
        return this.handName;
    }

    public void setHandName(String handName) {
        this.handName = handName;
    }
}
