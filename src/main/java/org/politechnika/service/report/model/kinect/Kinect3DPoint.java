package org.politechnika.service.report.model.kinect;

class Kinect3DPoint {

    private double x;

    private double y;

    private double z;

    public Kinect3DPoint() {
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof Kinect3DPoint)) return false;
        final Kinect3DPoint other = (Kinect3DPoint) o;
        if (!other.canEqual((Object) this)) return false;
        if (Double.compare(this.getX(), other.getX()) != 0) return false;
        if (Double.compare(this.getY(), other.getY()) != 0) return false;
        if (Double.compare(this.getZ(), other.getZ()) != 0) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof Kinect3DPoint;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $x = Double.doubleToLongBits(this.getX());
        result = result * PRIME + (int) ($x >>> 32 ^ $x);
        final long $y = Double.doubleToLongBits(this.getY());
        result = result * PRIME + (int) ($y >>> 32 ^ $y);
        final long $z = Double.doubleToLongBits(this.getZ());
        result = result * PRIME + (int) ($z >>> 32 ^ $z);
        return result;
    }

    public String toString() {
        return "Kinect3DPoint(x=" + this.getX() + ", y=" + this.getY() + ", z=" + this.getZ() + ")";
    }
}
