package org.politechnika.service.report.kinect_functions;

import org.politechnika.service.report.model.kinect.PointDistance;
import org.slf4j.Logger;

import java.util.List;
import java.util.function.UnaryOperator;

public class GenerateKinectPointDistanceReportCsv implements UnaryOperator<List<PointDistance>> {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(GenerateKinectPointDistanceReportCsv.class);

    public GenerateKinectPointDistanceReportCsv() {

    }

    @Override
    public List<PointDistance> apply(List<PointDistance> points) {
        tryWriteToCsv(points);
        return points;
    }

    private void tryWriteToCsv(List<PointDistance> points) {
//        try {
//            fileWriter.writeToCsvFile(points, "/kinect_points_distance.csv");
//        } catch (CsvParsingException e) {
//            log.error("Could not create a kinect point distance csv: {}", e.getMessage());
//        }
    }

}
