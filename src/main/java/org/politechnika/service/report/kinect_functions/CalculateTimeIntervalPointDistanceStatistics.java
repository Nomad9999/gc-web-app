package org.politechnika.service.report.kinect_functions;

import org.politechnika.domain.KinectDataEntity;
import org.politechnika.service.report.model.kinect.PointDistanceStatistics;
import org.politechnika.service.report.model.kinect.TimeIntervalPointDistanceStatistics;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class CalculateTimeIntervalPointDistanceStatistics implements Function<Map<Long, List<KinectDataEntity>>, TimeIntervalPointDistanceStatistics> {

    @Override
    public TimeIntervalPointDistanceStatistics apply(Map<Long, List<KinectDataEntity>> kinectDataPartitionedByTimeInterval) {

        TimeIntervalPointDistanceStatistics statsByTimeInterval = new TimeIntervalPointDistanceStatistics();
        calculateStatsForEveryTimeInterval(kinectDataPartitionedByTimeInterval, statsByTimeInterval);

        return statsByTimeInterval;
    }

    private void calculateStatsForEveryTimeInterval(Map<Long, List<KinectDataEntity>> kinectDataPartitionedByTimeInterval, TimeIntervalPointDistanceStatistics statsByTimeInterval) {
        for (List<KinectDataEntity> dataSection : kinectDataPartitionedByTimeInterval.values()) {
            PointDistanceStatistics statistics = new CalculatePointDistances()
                    .andThen(new CalculatePointDistanceStatistics())
                    .apply(dataSection);
            statsByTimeInterval.addStatstics(statistics);
        }
    }
}
