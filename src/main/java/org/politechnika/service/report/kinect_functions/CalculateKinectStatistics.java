package org.politechnika.service.report.kinect_functions;

import org.politechnika.domain.KinectDataEntity;
import org.politechnika.service.report.model.kinect.KinectStatistics;

import java.util.List;
import java.util.function.Function;

public class CalculateKinectStatistics implements Function<List<KinectDataEntity>, KinectStatistics> {

    private TerrifyingKinectSetter setter;

    public CalculateKinectStatistics() {
        this.setter = new TerrifyingKinectSetter();
    }

    @Override
    public KinectStatistics apply(List<KinectDataEntity> kinectData) {
        KinectStatistics kinectStatistics = new KinectStatistics();
        setter.setAllAverages(kinectStatistics, kinectData);
        setter.setAllVariances(kinectStatistics, kinectData);
        setter.setAllStandardDeviations(kinectStatistics, kinectData);
        setter.setSkewnessCoefficients(kinectStatistics, kinectData);
        setter.setAllKurtosis(kinectStatistics, kinectData);

        return kinectStatistics;
    }


}
