package org.politechnika.service.report.kinect_functions;

import org.politechnika.domain.KinectDataEntity;
import org.politechnika.service.report.model.kinect.KinectStatistics;
import org.politechnika.service.report.model.kinect.TimeIntervalKinectStatistics;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class CalculateTimeIntervalKinectStatistics implements Function<Map<Long, List<KinectDataEntity>>, TimeIntervalKinectStatistics> {

    @Override
    public TimeIntervalKinectStatistics apply(Map<Long, List<KinectDataEntity>> kinectDataPartitionedByTimeInterval) {
        TimeIntervalKinectStatistics statsByTimeInterval = new TimeIntervalKinectStatistics();
        calculateStatsForEveryTimeInterval(kinectDataPartitionedByTimeInterval, statsByTimeInterval);

        return statsByTimeInterval;
    }

    private void calculateStatsForEveryTimeInterval(Map<Long, List<KinectDataEntity>> kinectDataPartitionedByTimeInterval, TimeIntervalKinectStatistics statsByTimeInterval) {
        for (List<KinectDataEntity> dataSection : kinectDataPartitionedByTimeInterval.values()) {
            KinectStatistics statistics = new CalculateKinectStatistics().apply(dataSection);
            statsByTimeInterval.addStatstics(statistics);
        }
    }
}
