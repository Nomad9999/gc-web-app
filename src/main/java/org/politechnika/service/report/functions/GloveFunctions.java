package org.politechnika.service.report.functions;

import org.politechnika.domain.GloveDataEntity;
import org.politechnika.service.report.model.glove.Finger;

import java.util.function.Function;

public final class GloveFunctions  {

    private GloveFunctions() {
    }

    public static Function<GloveDataEntity, Finger> sensorToFinger() {
        return dto -> Finger.getFingerBySensorNumber(dto.getSensorNumber());
    }
}
