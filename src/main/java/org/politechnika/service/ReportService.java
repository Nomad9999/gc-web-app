package org.politechnika.service;

import org.politechnika.domain.ResearchEntity;
import org.politechnika.repository.ResearchDataRepository;
import org.politechnika.service.components.*;
import org.politechnika.util.MixedRandomList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.ThreadLocalRandom;

@Service
@Transactional
public class ReportService {

    private final ResearchService researchService;
    private final StatisticsReportGenerator statisticsReportGenerator;
    private final HandCsvGenerator handCsvGenerator;
    private final PointDistanceCsvGenerator pointDistanceCsvGenerator;
    private final CorrelationReportGenerator correlationReportGenerator;

    private final ResearchDataRepository researchDataRepository;

    @Autowired
    public ReportService(
        ResearchService researchService, StatisticsReportGenerator statisticsReportGenerator,
        HandCsvGenerator handCsvGenerator, PointDistanceCsvGenerator pointDistanceCsvGenerator, CorrelationReportGenerator correlationReportGenerator,
        final ResearchDataRepository researchDataRepository) {
        this.researchService = researchService;
        this.statisticsReportGenerator = statisticsReportGenerator;
        this.handCsvGenerator = handCsvGenerator;
        this.pointDistanceCsvGenerator = pointDistanceCsvGenerator;
        this.correlationReportGenerator = correlationReportGenerator;
        this.researchDataRepository = researchDataRepository;
    }

    public ReportBytesResource createStatisticsReport(final String researchName) {
        final ResearchEntity research = researchService.getResearchByName(researchName);
        return statisticsReportGenerator.generate(research);
    }

    public ReportBytesResource createHandCsv(final String researchName, final String hand) {
        final ResearchEntity research = researchService.getResearchByName(researchName);
        return handCsvGenerator.generate(research, hand);
    }

    public ReportBytesResource createPointDistanceCsv(final String researchName) {
        final ResearchEntity research = researchService.getResearchByName(researchName);
        return pointDistanceCsvGenerator.generate(research);
    }

    public ReportBytesResource createCorrelationReport(final String researchName) {
        final ResearchEntity research = researchService.getResearchByName(researchName);
        return correlationReportGenerator.generate(research);
    }

    public String getRandomName() {
        return researchService.getRandomName();
    }

    public ReportBytesResource getRandomReport(String researchName) {
        int random = ThreadLocalRandom.current().nextInt(1, 4);

        new MixedRandomList.GarbageProducer(50, MixedRandomList.numLive).run();

        switch (random) {
            case 1: return createStatisticsReport(researchName);
            case 2: return createHandCsv(researchName, "left");
            case 3: return createPointDistanceCsv(researchName);
            case 4: return createCorrelationReport(researchName);
        }

        throw new IllegalStateException("omg the random was: " + random);
    }

    public void clear() {
        researchDataRepository.deleteAll();
    }

//    public ReportBytesResource createStatisticsReport() {
//        throw new NotImplementedException();
//    }
//
//    public ReportBytesResource createStatisticsReport() {
//        throw new NotImplementedException();
//    }
//
//    public ReportBytesResource createStatisticsReport() {
//        throw new NotImplementedException();
//    }
}
