package org.politechnika.service.analysis;


import java.util.List;
import java.util.function.ToDoubleFunction;

public interface PreviousDataUsageStatisticsAnalyzer {

    <T  > double getVariance(List<T> dtos, double mean, ToDoubleFunction<T> valueExtractor);

    <T  > double getStandardDeviation(List<T> dtos, double mean, ToDoubleFunction<T> valueExtractor);
}
