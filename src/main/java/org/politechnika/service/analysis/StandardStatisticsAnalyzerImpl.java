package org.politechnika.service.analysis;

import org.apache.commons.math3.stat.descriptive.moment.Kurtosis;
import org.apache.commons.math3.stat.descriptive.moment.Skewness;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.apache.commons.math3.stat.descriptive.moment.Variance;

import java.util.List;
import java.util.function.ToDoubleFunction;


public class StandardStatisticsAnalyzerImpl  {

    public <T> double getAverage(List<T> dtos, ToDoubleFunction<T> valueExtractor) {
        return dtos.stream()
                .mapToDouble(valueExtractor)
                .average()
                .orElse(0);
    }

    public <T> double getVariance(List<T> dtos, ToDoubleFunction<T> valueExtractor) {
        return new Variance().evaluate(
                dtos.stream().mapToDouble(valueExtractor).toArray(),
                getAverage(dtos, valueExtractor));
    }

    public <T> double getVariance(List<T> dtos, double mean, ToDoubleFunction<T> valueExtractor) {
        return new Variance().evaluate(
                dtos.stream().mapToDouble(valueExtractor).toArray(),
                mean);
    }

    public <T> double getStandardDeviation(List<T> dtos, ToDoubleFunction<T> valueExtractor) {
        return new StandardDeviation().evaluate(dtos.stream().mapToDouble(valueExtractor).toArray(), getAverage(dtos, valueExtractor));
    }

    public <T> double getStandardDeviation(List<T> dtos, double mean, ToDoubleFunction<T> valueExtractor) {
        return new StandardDeviation().evaluate(dtos.stream().mapToDouble(valueExtractor).toArray(), mean);
    }

    public <T> double getSkewness(List<T> dtos, ToDoubleFunction<T> valueExtractor) {
        double val = new Skewness().evaluate(dtos.stream().mapToDouble(valueExtractor).toArray());
        return Double.isNaN(val) ? 0 : val;
    }

    public <T> double getKurtosis(List<T> dtos, ToDoubleFunction<T> valueExtractor) {
        double val = new Kurtosis().evaluate(dtos.stream().mapToDouble(valueExtractor).toArray());
        return Double.isNaN(val) ? 0 : val;
    }
}
