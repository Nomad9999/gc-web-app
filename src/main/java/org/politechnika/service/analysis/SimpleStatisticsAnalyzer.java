package org.politechnika.service.analysis;

import java.util.List;
import java.util.function.ToDoubleFunction;

public interface SimpleStatisticsAnalyzer{

    <T> double getAverage(List<T> dtos, ToDoubleFunction<T> valueExtractor);

    <T> double getVariance(List<T> dtos, ToDoubleFunction<T> valueExtractor);

    <T> double getStandardDeviation(List<T> dtos, ToDoubleFunction<T> valueExtractor);

    <T> double getSkewness(List<T> dtos, ToDoubleFunction<T> valueExtractor);

    <T> double getKurtosis(List<T> dtos, ToDoubleFunction<T> valueExtractor);

}
