package org.politechnika.service.analysis;

public interface CorrelationAnalyzer {

    double getPearsonCorrelation(double[] x, double[] y);
}
