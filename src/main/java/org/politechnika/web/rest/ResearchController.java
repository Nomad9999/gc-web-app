package org.politechnika.web.rest;


import org.politechnika.service.ResearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


@RestController
@RequestMapping("/api/research")
public class ResearchController {

    private final Logger log = LoggerFactory.getLogger(ResearchController.class);
    private final ResearchService researchService;

    @Autowired
    public ResearchController(ResearchService researchService) {
        this.researchService = researchService;
    }

    @PostMapping("/glove/{researchName}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> loadGlove(@RequestParam("file") MultipartFile file, @PathVariable String researchName) throws IOException {
        log.debug("adding glove file for {}", researchName);
        researchService.loadGlove(file, researchName);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/kinect/{researchName}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> loadKinect(@RequestParam("file") MultipartFile file, @PathVariable String researchName) throws IOException {
        log.debug("adding kinect file for {}", researchName);
        researchService.loadKinect(file, researchName);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/pulsometer/{researchName}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> loadPulsometer(@RequestParam("file") MultipartFile file, @PathVariable String researchName) throws IOException {
        log.debug("adding pulsometer file for {}", researchName);
        researchService.loadPulsometer(file, researchName);
        return ResponseEntity.ok().build();
    }
}
