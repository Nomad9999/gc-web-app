package org.politechnika.web.rest;


import org.politechnika.service.ReportService;
import org.politechnika.service.ResearchService;
import org.politechnika.service.components.ReportBytesResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/report")
public class ReportController {

    private final Logger log = LoggerFactory.getLogger(ReportController.class);

    private ReportService reportService;

    private ResearchService researchService;

    @Autowired
    public ReportController(ReportService reportService, final ResearchService researchService) {
        this.reportService = reportService;
        this.researchService = researchService;
    }

    @GetMapping("/statistics/{researchName}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Resource> getStaticticsReport(@PathVariable String researchName) {
        log.debug("get statistics for {}", researchName);
        ReportBytesResource reportBytesResource = reportService.createStatisticsReport(researchName);
        ByteArrayResource report = reportBytesResource.toByteArrayResource();

        return ResponseEntity.ok()
            .contentLength(reportBytesResource.getContentLength())
            .contentType(MediaType.parseMediaType("application/octet-stream"))
            .body(report);
    }

    @GetMapping("/hand-csv/{hand}/{researchName}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Resource> handCsv(
        @PathVariable String researchName,
        @PathVariable String hand) {
        log.debug("get {} hand csv for {}", hand, researchName);
        ReportBytesResource handCsv = reportService.createHandCsv(researchName, hand);
        return ResponseEntity.ok()
            .contentLength(handCsv.getContentLength())
            .contentType(MediaType.parseMediaType("text/csv"))
            .body(handCsv.toByteArrayResource());
    }

    @GetMapping("/point-distance/{researchName}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Resource> pointsDistance(@PathVariable String researchName) {
        log.debug("get pointDistance for {}", researchName);
        ReportBytesResource pointDistCsv = reportService.createPointDistanceCsv(researchName);
        return ResponseEntity.ok()
            .contentLength(pointDistCsv.getContentLength())
            .contentType(MediaType.parseMediaType("text/csv"))
            .body(pointDistCsv.toByteArrayResource());
    }

    @GetMapping("/correlation/{researchName}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Resource> correlation(@PathVariable String researchName) {
        log.debug("get correlation for {}", researchName);
        ReportBytesResource correlationReport = reportService.createCorrelationReport(researchName);
        return ResponseEntity.ok()
            .contentLength(correlationReport.getContentLength())
            .contentType(MediaType.parseMediaType("application/octet-stream"))
            .body(correlationReport.toByteArrayResource());
    }

    @GetMapping("/random")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> getRandom() {
        log.debug("get random report ");
        String randomName = reportService.getRandomName();
        ReportBytesResource randomReport = reportService.getRandomReport(randomName);
        return ResponseEntity.ok()
            .contentLength(randomReport.getContentLength())
            .contentType(MediaType.parseMediaType("application/octet-stream"))
            .body(randomReport.toByteArrayResource());
    }

    @GetMapping("/clr")
    @ResponseStatus(HttpStatus.OK)
    public void clr() {
        reportService.clear();
    }
}
