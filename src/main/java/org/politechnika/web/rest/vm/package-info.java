/**
 * View Models used by Spring MVC REST controllers.
 */
package org.politechnika.web.rest.vm;
