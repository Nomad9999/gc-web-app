package org.politechnika.repository;

import org.politechnika.domain.GloveDataEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GloveDataRepository extends JpaRepository<GloveDataEntity, Long> {

}
