package org.politechnika.repository;

import org.politechnika.domain.KinectDataEntity;
import org.politechnika.domain.ResearchEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ResearchDataRepository extends JpaRepository<ResearchEntity, Long> {

    Optional<ResearchEntity> findByResearchName(String researchName);

    @Query("SELECT DISTINCT r.researchName FROM ResearchEntity r")
    List<String> getAllResearchNames();
}
