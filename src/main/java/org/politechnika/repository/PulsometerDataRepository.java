package org.politechnika.repository;

import org.politechnika.domain.KinectDataEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PulsometerDataRepository extends JpaRepository<KinectDataEntity, Long> {

}
