package org.politechnika.domain;

import org.apache.commons.compress.utils.Lists;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.List;

@Entity
@Table(name = "research")
public class ResearchEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    private String researchName;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "research", fetch = FetchType.EAGER)
    @OrderColumn(name = "order_column")
    private List<GloveDataEntity> gloveData = Lists.newArrayList();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "research", fetch = FetchType.EAGER)
    @OrderColumn(name = "order_column")
    private List<KinectDataEntity> kinectData = Lists.newArrayList();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "research", fetch = FetchType.EAGER)
    @OrderColumn(name = "order_column")
    private List<PulsometerDataEntity> pulsometerData = Lists.newArrayList();

    public String getResearchName() {
        return researchName;
    }

    public void setResearchName(String researchName) {
        this.researchName = researchName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<GloveDataEntity> getGloveData() {
        return gloveData;
    }

    public void setGloveData(List<GloveDataEntity> gloveData) {
        this.gloveData = gloveData;
    }

    public List<KinectDataEntity> getKinectData() {
        return kinectData;
    }

    public void setKinectData(List<KinectDataEntity> kinectData) {
        this.kinectData = kinectData;
    }

    public List<PulsometerDataEntity> getPulsometerData() {
        return pulsometerData;
    }

    public void setPulsometerData(List<PulsometerDataEntity> pulsometerData) {
        this.pulsometerData = pulsometerData;
    }
}
