package org.politechnika.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

@Entity
@Table(name = "kinect_data")
public class KinectDataEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "research_id")
    private ResearchEntity research;

    @Column(name = "spineBase_x")
    private double spineBase_x;

    @Column(name = "spineBase_y")
    private double spineBase_y;

    @Column(name = "spineBase_z")
    private double spineBase_z;

    @Column(name = "spineMid_x")
    private double spineMid_x;

    @Column(name = "spineMid_y")
    private double spineMid_y;

    @Column(name = "spineMid_z")
    private double spineMid_z;

    @Column(name = "neck_x")
    private double neck_x;

    @Column(name = "neck_y")
    private double neck_y;

    @Column(name = "neck_z")
    private double neck_z;

    @Column(name = "head_x")
    private double head_x;

    @Column(name = "head_y")
    private double head_y;

    @Column(name = "head_z")
    private double head_z;

    @Column(name = "shoulderLeft_x")
    private double shoulderLeft_x;

    @Column(name = "shoulderLeft_y")
    private double shoulderLeft_y;

    @Column(name = "shoulderLeft_z")
    private double shoulderLeft_z;

    @Column(name = "elbowLeft_x")
    private double elbowLeft_x;

    @Column(name = "elbowLeft_y")
    private double elbowLeft_y;

    @Column(name = "elbowLeft_z")
    private double elbowLeft_z;

    @Column(name = "wristLeft_x")
    private double wristLeft_x;

    @Column(name = "wristLeft_y")
    private double wristLeft_y;

    @Column(name = "wristLeft_z")
    private double wristLeft_z;

    @Column(name = "handLeft_x")
    private double handLeft_x;

    @Column(name = "handLeft_y")
    private double handLeft_y;

    @Column(name = "handLeft_z")
    private double handLeft_z;

    @Column(name = "shoulderRight_x")
    private double shoulderRight_x;

    @Column(name = "shoulderRight_y")
    private double shoulderRight_y;

    @Column(name = "shoulderRight_z")
    private double shoulderRight_z;

    @Column(name = "elbowRight_x")
    private double elbowRight_x;

    @Column(name = "elbowRight_y")
    private double elbowRight_y;

    @Column(name = "elbowRight_z")
    private double elbowRight_z;

    @Column(name = "wristRight_x")
    private double wristRight_x;

    @Column(name = "wristRight_y")
    private double wristRight_y;

    @Column(name = "wristRight_z")
    private double wristRight_z;

    @Column(name = "handRight_x")
    private double handRight_x;

    @Column(name = "handRight_y")
    private double handRight_y;

    @Column(name = "handRight_z")
    private double handRight_z;

    @Column(name = "hipLeft_x")
    private double hipLeft_x;

    @Column(name = "hipLeft_y")
    private double hipLeft_y;

    @Column(name = "hipLeft_z")
    private double hipLeft_z;

    @Column(name = "kneeLeft_x")
    private double kneeLeft_x;

    @Column(name = "kneeLeft_y")
    private double kneeLeft_y;

    @Column(name = "kneeLeft_z")
    private double kneeLeft_z;

    @Column(name = "ankleLeft_x")
    private double ankleLeft_x;

    @Column(name = "ankleLeft_y")
    private double ankleLeft_y;

    @Column(name = "ankleLeft_z")
    private double ankleLeft_z;

    @Column(name = "footLeft_x")
    private double footLeft_x;

    @Column(name = "footLeft_y")
    private double footLeft_y;

    @Column(name = "footLeft_z")
    private double footLeft_z;

    @Column(name = "hipRight_x")
    private double hipRight_x;

    @Column(name = "hipRight_y")
    private double hipRight_y;

    @Column(name = "hipRight_z")
    private double hipRight_z;

    @Column(name = "kneeRight_x")
    private double kneeRight_x;

    @Column(name = "kneeRight_y")
    private double kneeRight_y;

    @Column(name = "kneeRight_z")
    private double kneeRight_z;

    @Column(name = "ankleRight_x")
    private double ankleRight_x;

    @Column(name = "ankleRight_y")
    private double ankleRight_y;

    @Column(name = "ankleRight_z")
    private double ankleRight_z;

    @Column(name = "footRight_x")
    private double footRight_x;

    @Column(name = "footRight_y")
    private double footRight_y;

    @Column(name = "footRight_z")
    private double footRight_z;

    @Column(name = "spineShoulder_x")
    private double spineShoulder_x;

    @Column(name = "spineShoulder_y")
    private double spineShoulder_y;

    @Column(name = "spineShoulder_z")
    private double spineShoulder_z;

    @Column(name = "handTipLeft_x")
    private double handTipLeft_x;

    @Column(name = "handTipLeft_y")
    private double handTipLeft_y;

    @Column(name = "handTipLeft_z")
    private double handTipLeft_z;

    @Column(name = "thumbLeft_x")
    private double thumbLeft_x;

    @Column(name = "thumbLeft_y")
    private double thumbLeft_y;

    @Column(name = "thumbLeft_z")
    private double thumbLeft_z;

    @Column(name = "handTipRight_x")
    private double handTipRight_x;

    @Column(name = "handTipRight_y")
    private double handTipRight_y;

    @Column(name = "handTipRight_z")
    private double handTipRight_z;

    @Column(name = "thumbRight_x")
    private double thumbRight_x;

    @Column(name = "thumbRight_y")
    private double thumbRight_y;

    @Column(name = "thumbRight_z")
    private double thumbRight_z;

    private Instant timestamp;

    public Long getId() {
        return id;
    }

    public ResearchEntity getResearch() {
        return research;
    }

    public void setResearch(ResearchEntity research) {
        this.research = research;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof KinectDataEntity)) return false;

        KinectDataEntity that = (KinectDataEntity) o;

        if (Double.compare(that.spineBase_x, spineBase_x) != 0) return false;
        if (Double.compare(that.spineBase_y, spineBase_y) != 0) return false;
        if (Double.compare(that.spineBase_z, spineBase_z) != 0) return false;
        if (Double.compare(that.spineMid_x, spineMid_x) != 0) return false;
        if (Double.compare(that.spineMid_y, spineMid_y) != 0) return false;
        if (Double.compare(that.spineMid_z, spineMid_z) != 0) return false;
        if (Double.compare(that.neck_x, neck_x) != 0) return false;
        if (Double.compare(that.neck_y, neck_y) != 0) return false;
        if (Double.compare(that.neck_z, neck_z) != 0) return false;
        if (Double.compare(that.head_x, head_x) != 0) return false;
        if (Double.compare(that.head_y, head_y) != 0) return false;
        if (Double.compare(that.head_z, head_z) != 0) return false;
        if (Double.compare(that.shoulderLeft_x, shoulderLeft_x) != 0) return false;
        if (Double.compare(that.shoulderLeft_y, shoulderLeft_y) != 0) return false;
        if (Double.compare(that.shoulderLeft_z, shoulderLeft_z) != 0) return false;
        if (Double.compare(that.elbowLeft_x, elbowLeft_x) != 0) return false;
        if (Double.compare(that.elbowLeft_y, elbowLeft_y) != 0) return false;
        if (Double.compare(that.elbowLeft_z, elbowLeft_z) != 0) return false;
        if (Double.compare(that.wristLeft_x, wristLeft_x) != 0) return false;
        if (Double.compare(that.wristLeft_y, wristLeft_y) != 0) return false;
        if (Double.compare(that.wristLeft_z, wristLeft_z) != 0) return false;
        if (Double.compare(that.handLeft_x, handLeft_x) != 0) return false;
        if (Double.compare(that.handLeft_y, handLeft_y) != 0) return false;
        if (Double.compare(that.handLeft_z, handLeft_z) != 0) return false;
        if (Double.compare(that.shoulderRight_x, shoulderRight_x) != 0) return false;
        if (Double.compare(that.shoulderRight_y, shoulderRight_y) != 0) return false;
        if (Double.compare(that.shoulderRight_z, shoulderRight_z) != 0) return false;
        if (Double.compare(that.elbowRight_x, elbowRight_x) != 0) return false;
        if (Double.compare(that.elbowRight_y, elbowRight_y) != 0) return false;
        if (Double.compare(that.elbowRight_z, elbowRight_z) != 0) return false;
        if (Double.compare(that.wristRight_x, wristRight_x) != 0) return false;
        if (Double.compare(that.wristRight_y, wristRight_y) != 0) return false;
        if (Double.compare(that.wristRight_z, wristRight_z) != 0) return false;
        if (Double.compare(that.handRight_x, handRight_x) != 0) return false;
        if (Double.compare(that.handRight_y, handRight_y) != 0) return false;
        if (Double.compare(that.handRight_z, handRight_z) != 0) return false;
        if (Double.compare(that.hipLeft_x, hipLeft_x) != 0) return false;
        if (Double.compare(that.hipLeft_y, hipLeft_y) != 0) return false;
        if (Double.compare(that.hipLeft_z, hipLeft_z) != 0) return false;
        if (Double.compare(that.kneeLeft_x, kneeLeft_x) != 0) return false;
        if (Double.compare(that.kneeLeft_y, kneeLeft_y) != 0) return false;
        if (Double.compare(that.kneeLeft_z, kneeLeft_z) != 0) return false;
        if (Double.compare(that.ankleLeft_x, ankleLeft_x) != 0) return false;
        if (Double.compare(that.ankleLeft_y, ankleLeft_y) != 0) return false;
        if (Double.compare(that.ankleLeft_z, ankleLeft_z) != 0) return false;
        if (Double.compare(that.footLeft_x, footLeft_x) != 0) return false;
        if (Double.compare(that.footLeft_y, footLeft_y) != 0) return false;
        if (Double.compare(that.footLeft_z, footLeft_z) != 0) return false;
        if (Double.compare(that.hipRight_x, hipRight_x) != 0) return false;
        if (Double.compare(that.hipRight_y, hipRight_y) != 0) return false;
        if (Double.compare(that.hipRight_z, hipRight_z) != 0) return false;
        if (Double.compare(that.kneeRight_x, kneeRight_x) != 0) return false;
        if (Double.compare(that.kneeRight_y, kneeRight_y) != 0) return false;
        if (Double.compare(that.kneeRight_z, kneeRight_z) != 0) return false;
        if (Double.compare(that.ankleRight_x, ankleRight_x) != 0) return false;
        if (Double.compare(that.ankleRight_y, ankleRight_y) != 0) return false;
        if (Double.compare(that.ankleRight_z, ankleRight_z) != 0) return false;
        if (Double.compare(that.footRight_x, footRight_x) != 0) return false;
        if (Double.compare(that.footRight_y, footRight_y) != 0) return false;
        if (Double.compare(that.footRight_z, footRight_z) != 0) return false;
        if (Double.compare(that.spineShoulder_x, spineShoulder_x) != 0) return false;
        if (Double.compare(that.spineShoulder_y, spineShoulder_y) != 0) return false;
        if (Double.compare(that.spineShoulder_z, spineShoulder_z) != 0) return false;
        if (Double.compare(that.handTipLeft_x, handTipLeft_x) != 0) return false;
        if (Double.compare(that.handTipLeft_y, handTipLeft_y) != 0) return false;
        if (Double.compare(that.handTipLeft_z, handTipLeft_z) != 0) return false;
        if (Double.compare(that.thumbLeft_x, thumbLeft_x) != 0) return false;
        if (Double.compare(that.thumbLeft_y, thumbLeft_y) != 0) return false;
        if (Double.compare(that.thumbLeft_z, thumbLeft_z) != 0) return false;
        if (Double.compare(that.handTipRight_x, handTipRight_x) != 0) return false;
        if (Double.compare(that.handTipRight_y, handTipRight_y) != 0) return false;
        if (Double.compare(that.handTipRight_z, handTipRight_z) != 0) return false;
        if (Double.compare(that.thumbRight_x, thumbRight_x) != 0) return false;
        if (Double.compare(that.thumbRight_y, thumbRight_y) != 0) return false;
        if (Double.compare(that.thumbRight_z, thumbRight_z) != 0) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        return timestamp != null ? timestamp.equals(that.timestamp) : that.timestamp == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id != null ? id.hashCode() : 0;
        temp = Double.doubleToLongBits(spineBase_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(spineBase_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(spineBase_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(spineMid_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(spineMid_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(spineMid_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(neck_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(neck_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(neck_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(head_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(head_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(head_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(shoulderLeft_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(shoulderLeft_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(shoulderLeft_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(elbowLeft_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(elbowLeft_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(elbowLeft_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(wristLeft_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(wristLeft_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(wristLeft_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(handLeft_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(handLeft_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(handLeft_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(shoulderRight_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(shoulderRight_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(shoulderRight_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(elbowRight_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(elbowRight_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(elbowRight_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(wristRight_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(wristRight_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(wristRight_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(handRight_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(handRight_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(handRight_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(hipLeft_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(hipLeft_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(hipLeft_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(kneeLeft_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(kneeLeft_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(kneeLeft_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(ankleLeft_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(ankleLeft_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(ankleLeft_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(footLeft_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(footLeft_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(footLeft_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(hipRight_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(hipRight_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(hipRight_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(kneeRight_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(kneeRight_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(kneeRight_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(ankleRight_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(ankleRight_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(ankleRight_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(footRight_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(footRight_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(footRight_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(spineShoulder_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(spineShoulder_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(spineShoulder_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(handTipLeft_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(handTipLeft_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(handTipLeft_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(thumbLeft_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(thumbLeft_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(thumbLeft_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(handTipRight_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(handTipRight_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(handTipRight_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(thumbRight_x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(thumbRight_y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(thumbRight_z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (timestamp != null ? timestamp.hashCode() : 0);
        return result;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getSpineBase_x() {
        return spineBase_x;
    }

    public void setSpineBase_x(double spineBase_x) {
        this.spineBase_x = spineBase_x;
    }

    public double getSpineBase_y() {
        return spineBase_y;
    }

    public void setSpineBase_y(double spineBase_y) {
        this.spineBase_y = spineBase_y;
    }

    public double getSpineBase_z() {
        return spineBase_z;
    }

    public void setSpineBase_z(double spineBase_z) {
        this.spineBase_z = spineBase_z;
    }

    public double getSpineMid_x() {
        return spineMid_x;
    }

    public void setSpineMid_x(double spineMid_x) {
        this.spineMid_x = spineMid_x;
    }

    public double getSpineMid_y() {
        return spineMid_y;
    }

    public void setSpineMid_y(double spineMid_y) {
        this.spineMid_y = spineMid_y;
    }

    public double getSpineMid_z() {
        return spineMid_z;
    }

    public void setSpineMid_z(double spineMid_z) {
        this.spineMid_z = spineMid_z;
    }

    public double getNeck_x() {
        return neck_x;
    }

    public void setNeck_x(double neck_x) {
        this.neck_x = neck_x;
    }

    public double getNeck_y() {
        return neck_y;
    }

    public void setNeck_y(double neck_y) {
        this.neck_y = neck_y;
    }

    public double getNeck_z() {
        return neck_z;
    }

    public void setNeck_z(double neck_z) {
        this.neck_z = neck_z;
    }

    public double getHead_x() {
        return head_x;
    }

    public void setHead_x(double head_x) {
        this.head_x = head_x;
    }

    public double getHead_y() {
        return head_y;
    }

    public void setHead_y(double head_y) {
        this.head_y = head_y;
    }

    public double getHead_z() {
        return head_z;
    }

    public void setHead_z(double head_z) {
        this.head_z = head_z;
    }

    public double getShoulderLeft_x() {
        return shoulderLeft_x;
    }

    public void setShoulderLeft_x(double shoulderLeft_x) {
        this.shoulderLeft_x = shoulderLeft_x;
    }

    public double getShoulderLeft_y() {
        return shoulderLeft_y;
    }

    public void setShoulderLeft_y(double shoulderLeft_y) {
        this.shoulderLeft_y = shoulderLeft_y;
    }

    public double getShoulderLeft_z() {
        return shoulderLeft_z;
    }

    public void setShoulderLeft_z(double shoulderLeft_z) {
        this.shoulderLeft_z = shoulderLeft_z;
    }

    public double getElbowLeft_x() {
        return elbowLeft_x;
    }

    public void setElbowLeft_x(double elbowLeft_x) {
        this.elbowLeft_x = elbowLeft_x;
    }

    public double getElbowLeft_y() {
        return elbowLeft_y;
    }

    public void setElbowLeft_y(double elbowLeft_y) {
        this.elbowLeft_y = elbowLeft_y;
    }

    public double getElbowLeft_z() {
        return elbowLeft_z;
    }

    public void setElbowLeft_z(double elbowLeft_z) {
        this.elbowLeft_z = elbowLeft_z;
    }

    public double getWristLeft_x() {
        return wristLeft_x;
    }

    public void setWristLeft_x(double wristLeft_x) {
        this.wristLeft_x = wristLeft_x;
    }

    public double getWristLeft_y() {
        return wristLeft_y;
    }

    public void setWristLeft_y(double wristLeft_y) {
        this.wristLeft_y = wristLeft_y;
    }

    public double getWristLeft_z() {
        return wristLeft_z;
    }

    public void setWristLeft_z(double wristLeft_z) {
        this.wristLeft_z = wristLeft_z;
    }

    public double getHandLeft_x() {
        return handLeft_x;
    }

    public void setHandLeft_x(double handLeft_x) {
        this.handLeft_x = handLeft_x;
    }

    public double getHandLeft_y() {
        return handLeft_y;
    }

    public void setHandLeft_y(double handLeft_y) {
        this.handLeft_y = handLeft_y;
    }

    public double getHandLeft_z() {
        return handLeft_z;
    }

    public void setHandLeft_z(double handLeft_z) {
        this.handLeft_z = handLeft_z;
    }

    public double getShoulderRight_x() {
        return shoulderRight_x;
    }

    public void setShoulderRight_x(double shoulderRight_x) {
        this.shoulderRight_x = shoulderRight_x;
    }

    public double getShoulderRight_y() {
        return shoulderRight_y;
    }

    public void setShoulderRight_y(double shoulderRight_y) {
        this.shoulderRight_y = shoulderRight_y;
    }

    public double getShoulderRight_z() {
        return shoulderRight_z;
    }

    public void setShoulderRight_z(double shoulderRight_z) {
        this.shoulderRight_z = shoulderRight_z;
    }

    public double getElbowRight_x() {
        return elbowRight_x;
    }

    public void setElbowRight_x(double elbowRight_x) {
        this.elbowRight_x = elbowRight_x;
    }

    public double getElbowRight_y() {
        return elbowRight_y;
    }

    public void setElbowRight_y(double elbowRight_y) {
        this.elbowRight_y = elbowRight_y;
    }

    public double getElbowRight_z() {
        return elbowRight_z;
    }

    public void setElbowRight_z(double elbowRight_z) {
        this.elbowRight_z = elbowRight_z;
    }

    public double getWristRight_x() {
        return wristRight_x;
    }

    public void setWristRight_x(double wristRight_x) {
        this.wristRight_x = wristRight_x;
    }

    public double getWristRight_y() {
        return wristRight_y;
    }

    public void setWristRight_y(double wristRight_y) {
        this.wristRight_y = wristRight_y;
    }

    public double getWristRight_z() {
        return wristRight_z;
    }

    public void setWristRight_z(double wristRight_z) {
        this.wristRight_z = wristRight_z;
    }

    public double getHandRight_x() {
        return handRight_x;
    }

    public void setHandRight_x(double handRight_x) {
        this.handRight_x = handRight_x;
    }

    public double getHandRight_y() {
        return handRight_y;
    }

    public void setHandRight_y(double handRight_y) {
        this.handRight_y = handRight_y;
    }

    public double getHandRight_z() {
        return handRight_z;
    }

    public void setHandRight_z(double handRight_z) {
        this.handRight_z = handRight_z;
    }

    public double getHipLeft_x() {
        return hipLeft_x;
    }

    public void setHipLeft_x(double hipLeft_x) {
        this.hipLeft_x = hipLeft_x;
    }

    public double getHipLeft_y() {
        return hipLeft_y;
    }

    public void setHipLeft_y(double hipLeft_y) {
        this.hipLeft_y = hipLeft_y;
    }

    public double getHipLeft_z() {
        return hipLeft_z;
    }

    public void setHipLeft_z(double hipLeft_z) {
        this.hipLeft_z = hipLeft_z;
    }

    public double getKneeLeft_x() {
        return kneeLeft_x;
    }

    public void setKneeLeft_x(double kneeLeft_x) {
        this.kneeLeft_x = kneeLeft_x;
    }

    public double getKneeLeft_y() {
        return kneeLeft_y;
    }

    public void setKneeLeft_y(double kneeLeft_y) {
        this.kneeLeft_y = kneeLeft_y;
    }

    public double getKneeLeft_z() {
        return kneeLeft_z;
    }

    public void setKneeLeft_z(double kneeLeft_z) {
        this.kneeLeft_z = kneeLeft_z;
    }

    public double getAnkleLeft_x() {
        return ankleLeft_x;
    }

    public void setAnkleLeft_x(double ankleLeft_x) {
        this.ankleLeft_x = ankleLeft_x;
    }

    public double getAnkleLeft_y() {
        return ankleLeft_y;
    }

    public void setAnkleLeft_y(double ankleLeft_y) {
        this.ankleLeft_y = ankleLeft_y;
    }

    public double getAnkleLeft_z() {
        return ankleLeft_z;
    }

    public void setAnkleLeft_z(double ankleLeft_z) {
        this.ankleLeft_z = ankleLeft_z;
    }

    public double getFootLeft_x() {
        return footLeft_x;
    }

    public void setFootLeft_x(double footLeft_x) {
        this.footLeft_x = footLeft_x;
    }

    public double getFootLeft_y() {
        return footLeft_y;
    }

    public void setFootLeft_y(double footLeft_y) {
        this.footLeft_y = footLeft_y;
    }

    public double getFootLeft_z() {
        return footLeft_z;
    }

    public void setFootLeft_z(double footLeft_z) {
        this.footLeft_z = footLeft_z;
    }

    public double getHipRight_x() {
        return hipRight_x;
    }

    public void setHipRight_x(double hipRight_x) {
        this.hipRight_x = hipRight_x;
    }

    public double getHipRight_y() {
        return hipRight_y;
    }

    public void setHipRight_y(double hipRight_y) {
        this.hipRight_y = hipRight_y;
    }

    public double getHipRight_z() {
        return hipRight_z;
    }

    public void setHipRight_z(double hipRight_z) {
        this.hipRight_z = hipRight_z;
    }

    public double getKneeRight_x() {
        return kneeRight_x;
    }

    public void setKneeRight_x(double kneeRight_x) {
        this.kneeRight_x = kneeRight_x;
    }

    public double getKneeRight_y() {
        return kneeRight_y;
    }

    public void setKneeRight_y(double kneeRight_y) {
        this.kneeRight_y = kneeRight_y;
    }

    public double getKneeRight_z() {
        return kneeRight_z;
    }

    public void setKneeRight_z(double kneeRight_z) {
        this.kneeRight_z = kneeRight_z;
    }

    public double getAnkleRight_x() {
        return ankleRight_x;
    }

    public void setAnkleRight_x(double ankleRight_x) {
        this.ankleRight_x = ankleRight_x;
    }

    public double getAnkleRight_y() {
        return ankleRight_y;
    }

    public void setAnkleRight_y(double ankleRight_y) {
        this.ankleRight_y = ankleRight_y;
    }

    public double getAnkleRight_z() {
        return ankleRight_z;
    }

    public void setAnkleRight_z(double ankleRight_z) {
        this.ankleRight_z = ankleRight_z;
    }

    public double getFootRight_x() {
        return footRight_x;
    }

    public void setFootRight_x(double footRight_x) {
        this.footRight_x = footRight_x;
    }

    public double getFootRight_y() {
        return footRight_y;
    }

    public void setFootRight_y(double footRight_y) {
        this.footRight_y = footRight_y;
    }

    public double getFootRight_z() {
        return footRight_z;
    }

    public void setFootRight_z(double footRight_z) {
        this.footRight_z = footRight_z;
    }

    public double getSpineShoulder_x() {
        return spineShoulder_x;
    }

    public void setSpineShoulder_x(double spineShoulder_x) {
        this.spineShoulder_x = spineShoulder_x;
    }

    public double getSpineShoulder_y() {
        return spineShoulder_y;
    }

    public void setSpineShoulder_y(double spineShoulder_y) {
        this.spineShoulder_y = spineShoulder_y;
    }

    public double getSpineShoulder_z() {
        return spineShoulder_z;
    }

    public void setSpineShoulder_z(double spineShoulder_z) {
        this.spineShoulder_z = spineShoulder_z;
    }

    public double getHandTipLeft_x() {
        return handTipLeft_x;
    }

    public void setHandTipLeft_x(double handTipLeft_x) {
        this.handTipLeft_x = handTipLeft_x;
    }

    public double getHandTipLeft_y() {
        return handTipLeft_y;
    }

    public void setHandTipLeft_y(double handTipLeft_y) {
        this.handTipLeft_y = handTipLeft_y;
    }

    public double getHandTipLeft_z() {
        return handTipLeft_z;
    }

    public void setHandTipLeft_z(double handTipLeft_z) {
        this.handTipLeft_z = handTipLeft_z;
    }

    public double getThumbLeft_x() {
        return thumbLeft_x;
    }

    public void setThumbLeft_x(double thumbLeft_x) {
        this.thumbLeft_x = thumbLeft_x;
    }

    public double getThumbLeft_y() {
        return thumbLeft_y;
    }

    public void setThumbLeft_y(double thumbLeft_y) {
        this.thumbLeft_y = thumbLeft_y;
    }

    public double getThumbLeft_z() {
        return thumbLeft_z;
    }

    public void setThumbLeft_z(double thumbLeft_z) {
        this.thumbLeft_z = thumbLeft_z;
    }

    public double getHandTipRight_x() {
        return handTipRight_x;
    }

    public void setHandTipRight_x(double handTipRight_x) {
        this.handTipRight_x = handTipRight_x;
    }

    public double getHandTipRight_y() {
        return handTipRight_y;
    }

    public void setHandTipRight_y(double handTipRight_y) {
        this.handTipRight_y = handTipRight_y;
    }

    public double getHandTipRight_z() {
        return handTipRight_z;
    }

    public void setHandTipRight_z(double handTipRight_z) {
        this.handTipRight_z = handTipRight_z;
    }

    public double getThumbRight_x() {
        return thumbRight_x;
    }

    public void setThumbRight_x(double thumbRight_x) {
        this.thumbRight_x = thumbRight_x;
    }

    public double getThumbRight_y() {
        return thumbRight_y;
    }

    public void setThumbRight_y(double thumbRight_y) {
        this.thumbRight_y = thumbRight_y;
    }

    public double getThumbRight_z() {
        return thumbRight_z;
    }

    public void setThumbRight_z(double thumbRight_z) {
        this.thumbRight_z = thumbRight_z;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }
}
