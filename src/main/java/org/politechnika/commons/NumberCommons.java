package org.politechnika.commons;

import org.slf4j.Logger;

public final class NumberCommons {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(NumberCommons.class);

    public static int tryGetIntValueFromString(String string) {
        try {
            return Integer.parseInt(string);
        } catch (NumberFormatException e) {
            log.error("Something went wrong ;p, recovering with default value of 1000 {}", e);
        }

        return 1000;
    }
}
