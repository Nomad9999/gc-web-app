import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { GcWebAppSharedLibsModule, GcWebAppSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [GcWebAppSharedLibsModule, GcWebAppSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [GcWebAppSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GcWebAppSharedModule {
  static forRoot() {
    return {
      ngModule: GcWebAppSharedModule
    };
  }
}
